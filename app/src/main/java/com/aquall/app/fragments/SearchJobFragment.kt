package com.aquall.app.fragments

import android.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.MultiAutoCompleteTextView
import androidx.activity.result.ActivityResult
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.MainActivity
import com.aquall.app.adapters.CategoryAutoCompleteAdapter
import com.aquall.app.adapters.SubCategoryAutoCompleteAdapter
import com.aquall.app.databinding.FragmentSearchJobBinding
import com.aquall.app.replaceFragment
import com.aquall.app.utils.ProgressDialog
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SearchKeys
import com.aquall.app.utils.model.SubCategory
import com.aquall.app.viewmodel.CategoriesViewModel
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import androidx.activity.result.ActivityResultCallback

import androidx.activity.result.contract.ActivityResultContracts.GetContent

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import com.aquall.app.Localizer
import com.aquall.app.utils.Logger
import com.aquall.app.utils.SearchPreferenceUtil
import com.aquall.app.utils.model.Location
import com.google.android.libraries.places.widget.AutocompleteActivity


class  SearchJobFragment : Fragment(),SubCategoryAutoCompleteAdapter.onChangeListener, AdapterView.OnItemSelectedListener,CategoriesViewModel.SubCategoryObserver,CategoriesViewModel.CategoryObserver{
    var selectedCategory:Category? =null
    var selectedLocation:Location?=null
    lateinit var categoryList:ArrayList<Category>
    var selectedSubCategories:ArrayList<SubCategory>?=null
    var locationResult:ActivityResultLauncher<Intent>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationResult  = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.let {
                    val place = Autocomplete.getPlaceFromIntent(it)
                    Log.e("LOCATION", "Place: ${place.name}, ${place.id}, ${place.latLng?.latitude}:${place.latLng?.longitude}")
                    inflate.editTextJobLocation.setText(place.name)
                    if (place.name!=null&&place.latLng!=null) {
                        val location = Location()
                        location.name = place.name!!
                        location.type = "Point"
                        location.coordinates = arrayListOf(place.latLng!!.longitude,place.latLng!!.latitude)
                        selectedLocation = location
                    }
                }
            }

            if (result.resultCode == AutocompleteActivity.RESULT_ERROR) {
                result.data?.let {
                    val status = Autocomplete.getStatusFromIntent(it)
                    status.statusMessage?.let { it1 -> Log.e("LOCATION", it1) }
                }
            }
        }

    }
    lateinit var viewModel: CategoriesViewModel
    lateinit var  inflate:FragmentSearchJobBinding;
    var recentSearchKeys:List<SearchKeys>?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(activity is MainActivity)
            (activity as MainActivity).setScreenType(NavScreen.Search())

        inflate = FragmentSearchJobBinding.inflate(inflater, container, false)

        inflate.label = Localizer.labels
        inflate.imageBackArrow.setOnClickListener {
            activity?.replaceFragment(ScreenName.DASHBOARD)
        }
        inflate.autoCompleteCategory.isFocusableInTouchMode = false
        inflate.autoCompleteCategory.onItemSelectedListener = this
        viewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            CategoriesViewModel::class.java
        )
        val categories = viewModel.getCategories()
        if (categories.value!=null)
        {
            onCategoriesChanged(categories.value!!)
        }
        categories.observe(viewLifecycleOwner,this)
        val subCategories = viewModel.getSubCategories()
        subCategories.observe(viewLifecycleOwner,this)


        inflate.layoutRangeSeek.automCompleteRange.isEnabled = false
        inflate.editTextJobLocation.isFocusableInTouchMode = false
        inflate.editTextJobLocation.setOnClickListener {
            val AUTOCOMPLETE_REQUEST_CODE = 100

            // Set the fields to specify which types of place data to
            // return after the user has made a selection.
            val fields = listOf(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG)

            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(activity)
            locationResult?.launch(intent)
        }
        inflate.textToolbarClear.setOnClickListener {
            clearAllData()
        }
        inflate.editTextJobLocation.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0?.length!! >0) {
                    inflate.layoutRangeSeek.automCompleteRange.isEnabled = true
                }
            }
        })

        inflate.autoCompleteSubCategory.isEnabled = false
        inflate.autoCompleteSubCategory.isFocusableInTouchMode = false
        inflate.autoCompleteSubCategory.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())
        inflate.autoCompleteSubCategory.setOnClickListener {
            (it as AutoCompleteTextView).showDropDown()
        }
        inflate.buttonSearchJob.setOnClickListener{
            val searchkeys= SearchKeys()
            if(!TextUtils.isEmpty(inflate.editTextJobTitle.getText()))
                searchkeys.name=inflate.editTextJobTitle.getText().toString()
            if(selectedCategory!=null) {
                  searchkeys.category.add(selectedCategory!!)
            }
            if (selectedSubCategories!=null && selectedSubCategories?.size!! >0)
            {
            searchkeys.subCategory.addAll(selectedSubCategories!!)
            }
            if(selectedLocation!=null)
            {
                searchkeys.location =selectedLocation
                searchkeys.range = inflate.layoutRangeSeek.automCompleteRange.progress.toString()
            }
            SearchPreferenceUtil.addNewSearchKey(searchkeys)
            activity?.replaceFragment(ScreenName.JOBSEARCHRESULT,searchkeys.toString())
        }
        getRecentSearch()
        val get = arguments?.getString("data")
        Logger.e("JobSearchResult: ${get}")
        if (get!=null&&get.length>0) {
            try {
                var searchKeys = SearchKeys.fromString(get)
//                activity?.replaceFragment(ScreenName.JOBSEARCHRESULT,searchKeys.toString(), prevDat = "")
                populatePreviousData(searchKeys)

            } catch (e: Exception) {
            }

        }
        return inflate.root
    }

    private fun getRecentSearch() {
        viewModel.getRecentSearches().observe(viewLifecycleOwner){searchKeys->
        recentSearchKeys = searchKeys
        var allKeyNames = searchKeys.map{ it.name }
        allKeyNames?.let {
            val arrayAdapter = ArrayAdapter<String>(
                requireContext(),
                R.layout.simple_list_item_1,
                it
            )
            inflate.editTextJobTitle.setAdapter(arrayAdapter)
            arrayAdapter.notifyDataSetChanged()
        }
        inflate.editTextJobTitle.setOnFocusChangeListener { view, b -> if (b) inflate.editTextJobTitle.showDropDown() }
        inflate.editTextJobTitle.setOnItemClickListener { _, view, i, _ ->
            val keyname = inflate.editTextJobTitle.adapter.getItem(i)
            keyname?.let {
                val searchKey = recentSearchKeys?.find {
                    it.name.equals(keyname)
                }
                if (searchKey != null) {
                    populatePreviousData(searchKey)
                }
            }
        }

        }
    }

    override fun onAutocompleteSelect(value: String, positions:ArrayList<SubCategory>) {
        inflate.autoCompleteSubCategory.setText(value)
        selectedSubCategories = positions
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        inflate.autoCompleteSubCategory.isEnabled = true
        selectedCategory = categoryList[p2]
        selectCategory()
    }

    private fun selectCategory() {
        ProgressDialog.show(viewLifecycleOwner, requireContext())
        if (selectedCategory != null) {
            viewModel.getSubCategories(selectedCategory!!._id)
        }
        selectedSubCategories = null
        inflate.autoCompleteSubCategory.setText("")
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        inflate.autoCompleteSubCategory.isEnabled = false
    }

    override fun onCategoriesChanged(t: List<Category>) {
        categoryList =ArrayList<Category>(t)
        val categoryStringList = categoryList.map {it.name }
        inflate.autoCompleteCategory.setAdapter(activity?.let { CategoryAutoCompleteAdapter(it,categoryStringList) })
        inflate.autoCompleteCategory.setOnItemClickListener { adapterView, view, i, l ->  onItemSelected(adapterView,view,i,l)}
        inflate.autoCompleteCategory.setOnClickListener{
            (it as AutoCompleteTextView).showDropDown()
        }
    }
    var subCategoryAutoCompleteAdapter:SubCategoryAutoCompleteAdapter?=null;
    override fun onSubCategoriesChanged(value: List<SubCategory>) {
        Logger.e("OnSubCategories called")
        ProgressDialog.hide()
        subCategoryAutoCompleteAdapter = SubCategoryAutoCompleteAdapter(requireActivity(),value)
        subCategoryAutoCompleteAdapter?.changeListener = this
        inflate.autoCompleteSubCategory.setAdapter(subCategoryAutoCompleteAdapter)
    }



    fun populatePreviousData(searchkey:SearchKeys){
        inflate.editTextJobTitle.setText(searchkey.name)
        searchkey.location?.let {
            inflate.editTextJobLocation.setText(it.name)
            selectedLocation = it
        }
        searchkey.range?.let {
        inflate.layoutRangeSeek.automCompleteRange.progress = it.toInt()
        }
        searchkey.category?.let {
            for (category in it){
                inflate.autoCompleteCategory.setText(category.name)
                selectedCategory = category
                if (selectedCategory != null) {
                    Logger.e("SEARCHJOBFRAGMENT category selected")
                    viewModel.getSubCategories(selectedCategory!!._id)
                }
                inflate.autoCompleteSubCategory.isEnabled = true
            }
        }
        searchkey.subCategory?.let {subCategoryList->
            inflate.autoCompleteSubCategory.isEnabled = true
         inflate
             .autoCompleteSubCategory.setText(subCategoryList.map { it.name }.joinToString(","))
            selectedSubCategories = subCategoryList
            subCategoryAutoCompleteAdapter?.selectedPositions =subCategoryList
        }
    }

    fun clearAllData()
    {
        inflate.editTextJobTitle.setText("")
        inflate.editTextJobLocation.setText("")
        selectedLocation = null
        inflate.layoutRangeSeek.automCompleteRange.progress = 0
        inflate.autoCompleteCategory.setText("")
        selectedCategory = null
        inflate.autoCompleteSubCategory.isEnabled = false
        inflate
            .autoCompleteSubCategory.setText("")
        selectedSubCategories = null
    }

}

