package com.aquall.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.aquall.app.Localizer
import com.aquall.app.databinding.FragmentLoginBinding
import com.aquall.app.login
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.utils.*


class LoginFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var inflate:FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        inflate = FragmentLoginBinding.inflate(layoutInflater, container, false)
        inflate.label = Localizer.labels
        inflate.buttonRegisterNow.setOnClickListener{
            activity?.replaceFragment(ScreenName.REGISTER)

        }
        inflate.textForgotPassword.setOnClickListener {
           activity?.replaceFragment(ScreenName.FORGOTPASSWORD)

        }

        inflate.buttonLogin.setOnClickListener {
           validate()
        }

        inflate.editEmail.setSingleLine()
        return inflate.root
    }

    fun validate():Boolean
    {

        if (!TextFieldValidator.isValidEmail(inflate.editEmail.getText()))
        {
            inflate.editEmail.setError(Localizer.labels.email_validation)
            return false
        }

        if (!TextFieldValidator.isValidPassword(inflate.editPassword.getText()))
        {
            inflate.editPassword.setError(Localizer.labels.password_validation)
            return false
        }

       login()

        return true

    }

    private  fun login()
    {
        ProgressDialog.show(viewLifecycleOwner,requireContext())
        val login = AuthRepositary().login(
                inflate.editEmail.getText().toString(),
                inflate.editPassword.getText().toString()
            )
            login.observe(viewLifecycleOwner,{
                ProgressDialog.hide()
                if (it)
                {
                    activity?.login(ScreenName.DASHBOARD)
                }else{
                    Localizer.labels.login_network_failure?.let { it1 ->
                        AlertDialog.show(activity,
                            it1,false,null,viewLifecycleOwner)
                    }
                }
            })


    }

}