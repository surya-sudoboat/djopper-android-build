package com.aquall.app.fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.adapters.JobListAdapter
import com.aquall.app.adapters.JobSearchKeyListAdapter
import com.aquall.app.databinding.FragmentJobSearchResultBinding
import com.aquall.app.newScreen
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.utils.Logger
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.JobsList
import com.aquall.app.utils.model.SearchKeys
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewmodel.JobViewModel

class JobSearchResultFragment : Fragment(),JobListViewHolder.IteractionListener,Observer<JobsList>{
    lateinit var viewModel:JobViewModel
    lateinit var adapter:JobListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var inflate:FragmentJobSearchResultBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        inflate = FragmentJobSearchResultBinding.inflate(inflater, container, false)
        inflate.ivBackArrow.setOnClickListener { activity?.onBackPressed() }
        inflate.ivFilter.setOnClickListener { activity?.onBackPressed() }
        val get = arguments?.getString("data")
        Logger.e("JobSearchResult: ${get}")
        var searchKeys = SearchKeys.fromString(get!!)
        var keyList = ArrayList<String>()

        if (searchKeys.category!=null&&searchKeys.category.isNotEmpty())
        {
            for (cat in searchKeys.category!!)
            {
                if (cat!=null&&!TextUtils.isEmpty(cat.name))
                {
                    keyList.add(cat.name)
                }
            }
        }
        if (searchKeys.subCategory!=null&& searchKeys.subCategory.isNotEmpty())
        {
            for (subCat in searchKeys.subCategory)
            {
                if (subCat!=null&&!TextUtils.isEmpty(subCat.name))
                {
                    keyList.add(subCat.name!!)
                }
            }
        }
        if (keyList.isNotEmpty())
        {
        JobSearchKeyListAdapter.setAdapter(inflate.rvSearchKeys,keyList)
        }
        else{
            inflate.rvSearchKeys.visibility = GONE
        }
        viewModel = ViewModelProvider({viewModelStore},defaultViewModelProviderFactory).get(JobViewModel::class.java)
        viewModel.getNetworkData().observe(viewLifecycleOwner, Observer {
            if (it!=null)
            {

                inflate.networkStatus = it
            }
        })
        viewModel.getJobList().observe(viewLifecycleOwner, this)
        viewModel.search(searchKeys)
        if (!TextUtils.isEmpty(searchKeys.name)) {
            inflate.textToolbarTitle.setText(searchKeys.name)
        }
        return inflate.root
    }

    override fun onFavoriteChanged(position: Int, value: Boolean) {
        if (!value) {
            val job_id = adapter.jobs.get(position)._id
            viewModel.removeFavorite(job_id!!)
        }else{
            val job_id = adapter.jobs.get(position)._id
            viewModel.markAsFavorite(job_id!!)
        }
    }

    var chatRooms: HashMap<String,List<ChatRoom>>? = null
    override fun onItemClicked(position: Int) {


        val intentBundle = IntentBundle()
        val get = adapter.jobs.get(position)
        intentBundle.job = get
        if (chatRooms!=null&&get._id!=null&&chatRooms?.get(get._id)!=null&& chatRooms?.size!! >0) {
            intentBundle.chatRoom = chatRooms?.get(get._id)!!.get(0)
        }
        activity?.newScreen(ScreenName.JOBDESCRIPTION,intentBundle.toString())
    }

    override fun isFavoriteList(): Boolean {
      return false
    }

    override fun showFavoriteAndKnowMore(): Boolean {
        return true
    }

    override fun deleteAlertText(): String {
        return ""
    }

    override fun onDeleteClicked(position: Int) {}
    override fun onEditClicked(position: Int) {

    }

    override fun getMessagesCount(position: Int): Int {
        return 0
    }

    override fun onChanged(t: JobsList?) {
        if (t!=null) {
            chatRooms = t.chatRoom
           adapter = JobListAdapter.setAdapter(inflate.rvJobResult,t.jobs,this)
        }
    }


}