package com.aquall.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.databinding.FragmentCategorySelectionBinding
import com.aquall.app.replaceFragment
import com.aquall.app.utils.ScreenName

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategorySelectionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategorySelectionFragment : Fragment() {
    lateinit var categorySelectionBinding :FragmentCategorySelectionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        categorySelectionBinding = FragmentCategorySelectionBinding.inflate(inflater, container, false)
        categorySelectionBinding.layoutCategoryOne.setOnClickListener {
              activity?.replaceFragment(ScreenName.LOGIN)
        }
        return categorySelectionBinding.root
    }

}