package com.aquall.app.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.*
import com.aquall.app.databinding.FragmentProfileBinding
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.LabelRepositary
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.*
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.Profile
import com.aquall.app.viewholders.ClickListener
import com.aquall.app.viewmodel.ProfileAuthViewModel

class ProfileFragment : Fragment(),Observer<Profile> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var binding:FragmentProfileBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        if (activity is MainActivity) {
            (activity as MainActivity).setScreenType(NavScreen.Profile())
        }
        val viewModel = ViewModelProvider(
            { viewModelStore },
            defaultViewModelProviderFactory
        ).get(ProfileAuthViewModel::class.java)
        viewModel.getProfile().observe(viewLifecycleOwner,this)

        binding.tvLabelAppliedJobs.setOnClickListener {
            activity?.replaceFragment(ScreenName.APPLIEDJOBS)
        }

        binding.tvLabelPostedJobs.setOnClickListener {
            activity?.replaceFragment(ScreenName.POSTEDJOBS)
        }
        binding.tvLabelExpiredJobs.setOnClickListener {
            activity?.replaceFragment(ScreenName.EXPIREDJOBS)

        }
        binding.tvLabelPaymentHistory.setOnClickListener {
            activity?.replaceFragment(ScreenName.PAYMENTHISTORY)

        }
        binding.tvLabelInvoice.setOnClickListener {
            activity?.replaceFragment(ScreenName.INVOICELIST)

        }
        binding.buttonLanguageChange.setText(Localizer.getLabel())
        binding.buttonLanguageChange.setOnClickListener {
            LanguagePopup(requireContext(), object : ClickListener {
                override fun onClick(language: Localizer.Locale) {
                    language.code?.let { it1 ->
                        val labelRepositary = LabelRepositary()
                        labelRepositary.getLabels(language = it1,false)
                        ProgressDialog.show(viewLifecycleOwner,requireContext())
                        labelRepositary.networkStatus.observe(viewLifecycleOwner) {
                            if (it == DataStatus.DATA_AVAILABLE) {
                                ProgressDialog.hide()
                                val intent = activity?.intent
                                intent?.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                intent?.putExtra("screenName",ScreenName.PROFILE.getValue())
                                activity?.startActivity(intent)
                                activity?.finish()
                            } else if (it == DataStatus.FAILED) {
                                ProgressDialog.hide()
                            }
                        }
                    }
                }
            }).show()
        }
        binding.layoutTopBar.setOnClickListener { activity?.replaceFragment(ScreenName.EDITPROFILE) }
        binding.tvLabelLogout.setOnClickListener {
            AlertDialog.show(requireContext(),Localizer.labels.are_you_sure_you_want_to_logout,true,
                object : AlertDialog.Listener {
                    override fun success() {
                        ProgressDialog.show(viewLifecycleOwner,requireContext())
                        val authRepositary = AuthRepositary()
                        authRepositary.logout(false)
                        authRepositary.networkStatus.observe(viewLifecycleOwner) {
                            if (it.equals(DataStatus.DATA_AVAILABLE)) {
                                ProgressDialog.hide()
                                activity?.logout()
                            }
                            else if(it.equals(DataStatus.FAILED))
                            {
                                ProgressDialog.hide()
                                Toast.makeText(
                                    requireContext(),
                                    Localizer.labels.logout_failed,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }

                    override fun cancel() {}

                },viewLifecycleOwner)
        }
        return binding.root
    }

    override fun onChanged(t: Profile?) {
        if (t!=null) {
            binding.profile =t

            if (t.image!=null)
            {
                ImageProcessor.into(binding.ivProfilePicture).setCircleCrop().loadResource(
                        ServiceUrl.BASE_URL+t.image,R.drawable.ic_abstract_user)
            }

        }
    }


}