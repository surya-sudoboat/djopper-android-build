package com.aquall.app.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.DocumentsContract
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aquall.app.Localizer
import com.aquall.app.repositary.model.InvoiceDetail
import com.aquall.app.ui.AquallTopBar
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.ProgressDialog
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.viewmodel.JobViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class InvoiceFragment:Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                InvoiceFragmentScreen()
            }
        }
    }
    var startForResult:ActivityResultLauncher<Intent>?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startForResult = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult())
        { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                //  you will get result here in result.data
                result?.data?.also { uri ->
                    // Perform operations on the document using its URI.
                    sendFileToOtherApp(uri.data)
                }
            }else{
                isFileBeingWritten = false
            }
        }
    }

    fun sendFileToOtherApp(uri:Uri?){
        try {
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            alterDocument(uri!!,fileContentForDownload!!)
            CoroutineScope(Dispatchers.IO).launch {
                delay(2000)
                viewLifecycleOwner.lifecycle.coroutineScope.launch {
                    ProgressDialog.hide()
                    isFileBeingWritten = false
                    openPdfFile(uri)
                }
            }

        } catch (e: Exception) {
            ProgressDialog.hide()
            isFileBeingWritten = false
            e.message?.let {
                Log.e("INVOICE_FRAGMENT",it)
            }
        }
    }

    private fun openPdfFile(uri: Uri?) {
        val target = Intent(Intent.ACTION_VIEW)
        target.setDataAndType(uri, "application/pdf")
        target.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
        target.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        val intent = Intent.createChooser(target, "Open File")
        startActivity(intent)
    }


    @Composable
    fun InvoiceFragmentScreen(viewmodel: JobViewModel = viewModel()) {
        val invoiceListState = viewmodel.getInvocieList().observeAsState()
        Column(Modifier.fillMaxSize()) {
            AquallTopBar(titleText = Localizer.labels.invoice_list) {
                requireActivity().onBackPressed()
            }
            InvoiceList(invoiceList = invoiceListState)
            {filename,endDate->
                viewmodel.getInvoiceList(filename,endDate).observe(viewLifecycleOwner)
                {fileContent->
                    when(fileContent.status){
                        DataStatus.LOADING->{
                            ProgressDialog.show(viewLifecycleOwner,requireContext())
                        }
                        DataStatus.DATA_AVAILABLE->{
                            fileContent.data?.let { fileContentData->
                                ProgressDialog.hide()
                                if(!isFileBeingWritten)
                                    savePdfFile("$filename-$endDate.pdf", fileContentData)
                            }
                        }
                        DataStatus.FAILED->{
                            ProgressDialog.hide()
                            AlertDialog.show(requireContext(), text = "Download Failed",false,null,viewLifecycleOwner)
                        }
                    }

                }
            }
        }
    }
    var isFileBeingWritten = false

    @Composable
    fun InvoiceList(
        invoiceList: State<List<InvoiceDetail>?>,
        onItemClick: (startDate: String,endDate:String) -> Unit = {startDate,endDate->}
    ) {
        val value = invoiceList.value
        value?.let {
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                for (invoice in it) {
                    item {
                        InvoiceItem(invoiceDetail = invoice, onItemClick)
                    }
                }
            }
        }


    }


    @Composable
    fun InvoiceItem(invoiceDetail: InvoiceDetail, onItemClick: (startDate: String,endDate:String) -> Unit = {startDate,endDate->}) {
        Box(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Card(
                Modifier
                    .fillMaxWidth()
            ) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(5.dp)) {
                    Text(
                        text = "${invoiceDetail.month} - ${invoiceDetail.year}",
                        modifier = Modifier.align(Alignment.CenterVertically)
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Button(
                        onClick = {
                            try {
                                Log.e("INVOICE_FRAG","clicked called")
                                val dateFormat = SimpleDateFormat("LLLL yyyy", Locale.getDefault())
                                val monthYearText =
                                    dateFormat.parse("${invoiceDetail.month} ${invoiceDetail.year}")
                                monthYearText?.let {
                                    Log.e("Prasanth parsed date: ", it.toString())
                                    val endDate = Calendar.getInstance()
                                    endDate.time = it
                                    endDate[Calendar.DAY_OF_MONTH] = endDate.getActualMaximum(Calendar.DAY_OF_MONTH)
                                    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
                                    onItemClick(formatter.format(it),formatter.format(endDate.time))
                                }
                            } catch (e: Exception) {
                                    e.message?.let { Log.e("Aquall", it) }
                            }
                        },
                        modifier = Modifier.align(Alignment.CenterVertically)
                    ) {
                        Text(text = Localizer.labels.download)
                    }
                }
            }
        }
    }

    var fileContentForDownload:String? = null
    var fileName:String? = null
    fun savePdfFile(filename: String,fileContents:String) {
        fileContentForDownload = fileContents
        fileName = filename
        isFileBeingWritten =true
        try {
            createFile(filename=filename)
        } catch (e: Exception) {
            ProgressDialog.hide()
            isFileBeingWritten= false
            e.message?.let {
                Log.e("INVOICE_FRAGMENT",it)
            }
        }

    }

    private fun alterDocument(uri: Uri,content:String) {
        try {
            val contentResolver = requireContext().contentResolver
            contentResolver.openFileDescriptor(uri, "w")?.use { parcelFileDesc ->
                FileOutputStream(parcelFileDesc.fileDescriptor).use {
                    it.write(
                        Base64.decode(content,Base64.DEFAULT)
                    )
                }
            }
        } catch (e: FileNotFoundException) {
            isFileBeingWritten = false
            e.printStackTrace()
        } catch (e: IOException) {
            isFileBeingWritten = false
            e.printStackTrace()
        }
    }
    val CREATE_FILE = 1

    private fun createFile(pickerInitialUri: Uri?=null,filename: String) {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "application/pdf"
            putExtra(Intent.EXTRA_TITLE, filename)

            // Optionally, specify a URI for the directory that should be opened in
            // the system file picker before your app creates the document.
            pickerInitialUri?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri)
                }
            }

        }
        startForResult?.launch(intent)
    }



}