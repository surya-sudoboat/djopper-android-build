package com.aquall.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aquall.app.R
import com.aquall.app.databinding.FragmentCandidatesBinding
import com.aquall.app.Localizer


class CandidatesFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var fragmentCandidatesBinding:FragmentCandidatesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentCandidatesBinding =  FragmentCandidatesBinding.inflate(inflater, container, false)
        fragmentCandidatesBinding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }

        return fragmentCandidatesBinding.root
    }

}