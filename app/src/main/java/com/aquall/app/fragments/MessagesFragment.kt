package com.aquall.app.fragments

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.adapters.JobListAdapter
import com.aquall.app.adapters.JobsViewPagerAdapter
import com.aquall.app.databinding.FragmentMessagesBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.ChatRepositary
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.utils.*
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewmodel.JobViewModel
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.launch
import org.json.JSONObject


class MessagesFragment : Fragment(),TabLayout.OnTabSelectedListener {
    lateinit var binding:FragmentMessagesBinding
    var appliedList:List<Job>? = null
    var postedList:List<Job>? = null
    var appliedListAdapter:JobListAdapter?=null
    var postedListAdapter:JobListAdapter?=null
    var postChatbadge:Int =0;
    var appliedJobBadge:Int=0
    var appliedChatRooms:HashMap<String,List<ChatRoom>>? = null
    var postedChatRooms:HashMap<String,List<ChatRoom>>? = null
    var currentItem = 0
    lateinit var jobsViewPagerAdapter:JobsViewPagerAdapter
    lateinit var customItemTouchHelper:CustomItemTouchHelper
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
         binding = FragmentMessagesBinding.inflate(inflater, container, false)
        customItemTouchHelper =
            CustomItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                private var mIcon: Drawable? = null
                private var mBackground: ColorDrawable? = null
                private var isEnabled = true
                init {
                    mIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_trash_delete);
                    mBackground = ColorDrawable(Color.RED);
                }

                fun enable(){
                    isEnabled = true
                }

                fun disable(){
                    isEnabled = false
                }

                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    if (appliedListAdapter!=null && appliedListAdapter?.jobs!=null && viewHolder.layoutPosition<=appliedListAdapter!!.jobs!!.size-1) {
                        onAppliedJobDeleteClicked(viewHolder.layoutPosition)
                    }
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    if (recyclerView.adapter!=null&&viewHolder.layoutPosition<recyclerView.adapter!!.itemCount-1) {
                        mIcon!!.setTint(Color.WHITE)
                        val itemView = viewHolder.itemView
                        val backgroundCornerOffset =
                            25 //so mBackground is behind the rounded corners of itemView
                        val iconMargin = (itemView.height - mIcon!!.intrinsicHeight) / 2
                        val iconTop = itemView.top + (itemView.height - mIcon!!.intrinsicHeight) / 2
                        val iconBottom = iconTop + mIcon!!.intrinsicHeight
                        if (dX > 0) { // Swiping to the right
                            val iconLeft = itemView.left + iconMargin
                            val iconRight = iconLeft + mIcon!!.intrinsicWidth
                            mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                            mBackground!!.setBounds(
                                itemView.left, itemView.top,
                                itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom
                            )
                        } else if (dX < 0) { // Swiping to the left
                            val iconLeft = itemView.right - iconMargin - mIcon!!.intrinsicWidth
                            val iconRight = itemView.right - iconMargin
                            mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                            mBackground!!.setBounds(
                                itemView.right + dX.toInt() - backgroundCornerOffset,
                                itemView.top, itemView.right, itemView.bottom
                            )
                        } else { // view is unSwiped
                            mIcon!!.setBounds(0, 0, 0, 0)
                            mBackground!!.setBounds(0, 0, 0, 0)
                        }

                        mBackground!!.draw(c)
                        mIcon!!.draw(c)

                        super.onChildDraw(
                            c,
                            recyclerView,
                            viewHolder,
                            dX,
                            dY,
                            actionState,
                            isCurrentlyActive
                        )
                    }
                }
            })
        binding.label = Localizer.labels
        binding.tabLayoutMessages.getTabAt(0)?.text = Localizer.labels.applied_jobs
        binding.tabLayoutMessages.getTabAt(1)?.text = Localizer.labels.posted_jobs
        binding.svJobs.queryHint = Localizer.labels.search_applied_jobs
        binding.textToolbarTitle.setText(Localizer.labels.messages)
        binding.svJobs.queryHint = Localizer.labels.search_applied_jobs
        val bundle = arguments?.getString("data")
        if (bundle!=null){
            try {
                val jsonObject = JSONObject(bundle)
                currentItem = jsonObject.getInt("tabSelection")
            } catch (e: Exception) {
            }
        }
        binding.svJobs.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.vpJobs.currentItem==0)
                {
                    appliedListAdapter?.search(newText?:"")
                }else{
                    postedListAdapter?.search(newText?:"")
                }
              return false
            }

        })

         binding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }
         if(activity is MainActivity)
            (activity as MainActivity).setScreenType(NavScreen.Messages())
         var viewModel = JobViewModel()
        var viewModel1 = JobViewModel()
        ProgressDialog.show(viewLifecycleOwner,requireContext())
       viewModel.getJobList().observe(viewLifecycleOwner, Observer {
           appliedList = it.jobs
           appliedChatRooms = it.chatRoom
           appliedJobBadge = it.unreadCount
           if (postedList!=null)
               loadScreen()
       })
        viewModel.getAppliedJobsChat()
        viewModel1.getJobList().observe(viewLifecycleOwner,Observer {
            postedList = it.jobs
            postedChatRooms = it.chatRoom
            postChatbadge = it.unreadCount
            if (appliedList!=null)
                loadScreen()
        })
        viewModel1.getPostedJobsChat()
        SocketClient.getSocketClient().unReadLiveData.observe(viewLifecycleOwner) {
            viewModel.getAppliedJobsChat()
            viewModel1.getPostedJobsChat()

        }

        binding.vpJobs.isUserInputEnabled = false
        return binding.root
    }
    fun onAppliedJobDeleteClicked(position:Int){
        appliedListAdapter?.notifyItemChanged(position)
        val get = appliedListAdapter?.getItem(position)
        AlertDialog.show(requireContext(),Localizer.labels.do_you_want_to_delete_this_chat,true,object:AlertDialog.Listener{
            override fun success() {
                val chat_room_id = appliedChatRooms?.get(get?._id)?.get(0)?._id
                if (chat_room_id!=null) {
                    ChatRepositary().deleteChat(chat_room_id)
                }
                get?.let {
//                    appliedListAdapter?.remove(it)
                }

            }

            override fun cancel() {

            }
        },viewLifecycleOwner)

    }
    fun loadScreen()
    {
        ProgressDialog.hide()
        if (appliedListAdapter==null&&postedListAdapter==null) {
            appliedListAdapter = JobListAdapter.getAdapter(appliedList!!,object : JobListViewHolder.IteractionListener{
                override fun onFavoriteChanged(position: Int, value: Boolean) {

                }

                override fun onItemClicked(position: Int) {
                    val get = appliedList?.get(position)

                    val messageBundle = IntentBundle()
                    messageBundle.screenType =0
                    messageBundle.job = get
                    if (appliedChatRooms!=null&& appliedChatRooms?.get(get?._id)!=null&&appliedChatRooms!!.size>0) {
                        messageBundle.chatRoom = appliedChatRooms?.get(get?._id)!!.get(0)
                    }
                    val jsonObject = JSONObject()
                    jsonObject.put("tabSelection",0)
                    activity?.replaceFragment(ScreenName.MESSAGEDETAIL,JsonUtil.toJson(messageBundle,IntentBundle::class.java),true,jsonObject.toString())

                }

                override fun isFavoriteList(): Boolean {
                    return false
                }

                override fun showFavoriteAndKnowMore(): Boolean {
                    return true
                }

                override fun deleteAlertText(): String {
                    return  ""
                }

                override fun onDeleteClicked(position: Int) {

                }

                override fun onEditClicked(position: Int) {

                }

                override fun getMessagesCount(position:Int): Int {
                    return 0
                }
            },
                needLastEmptyCell = true,
                isMessageAdapter = true
            )
            postedListAdapter = JobListAdapter.getAdapter(postedList!!,object :JobListViewHolder.IteractionListener{
                override fun onFavoriteChanged(position: Int, value: Boolean) {

                }

                override fun onItemClicked(position: Int) {
                    val get = postedList?.get(position)
                    val messageBundle = IntentBundle()
                    messageBundle.screenType =1
                    messageBundle.job = get
                    messageBundle.isCandidateClickable  = true
                    if (postedChatRooms!=null&& postedChatRooms?.get(get?._id)!=null&&postedChatRooms!!.size>0&&postedChatRooms!!.get(get?._id!!)!!.size>0) {
                        messageBundle.chatRoom = postedChatRooms?.get(get?._id)!!.get(0)
                    }
                    val jsonObject = JSONObject()
                    jsonObject.put("tabSelection",1)
                    activity?.replaceFragment(ScreenName.CANDIDATELIST,JsonUtil.toJson(messageBundle,IntentBundle::class.java),true,jsonObject.toString())

                }

                override fun getMessagesCount(position:Int): Int {
                    return  postedChatRooms?.get(postedList?.get(position)?._id)?.size?:0
                }

                override fun isFavoriteList(): Boolean {
                    return false
                }

                override fun showFavoriteAndKnowMore(): Boolean {
                    return true
                }

                override fun deleteAlertText(): String {
                    return ""
                }

                override fun onDeleteClicked(position: Int) {

                }

                override fun onEditClicked(position: Int) {

                }
            },
                needLastEmptyCell = true,
                isMessageAdapter = true
            )
            jobsViewPagerAdapter =
                JobsViewPagerAdapter(this, appliedListAdapter!!, postedListAdapter!!,customItemTouchHelper)
            binding.vpJobs.adapter = jobsViewPagerAdapter
            TabLayoutMediator(binding.tabLayoutMessages,binding.vpJobs){tab,position->
                tab.text = if (position==0)Localizer.labels.applied_jobs else Localizer.labels.posted_jobs
                tab.setCustomView(R.layout.tab_custom_view);
                if (position==0) {
                    val b = tab.customView!!.findViewById(android.R.id.text1) as TextView
                    b.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                }
                if (position==0&&appliedJobBadge>0)
               {
                   setBadge(binding.tabLayoutMessages.getTabAt(0),appliedJobBadge)

               }else if (position!=0 &&postChatbadge>0)
               {
                   setBadge(binding.tabLayoutMessages.getTabAt(1),postChatbadge)

               }

            }.attach()

            binding.tabLayoutMessages.addOnTabSelectedListener(this)

        }else{
            if (appliedList!=null) {
                appliedListAdapter?.updateAdapter(appliedList!!)
                if (appliedJobBadge>0) {
                    setBadge(binding.tabLayoutMessages.getTabAt(0),appliedJobBadge)
                }
            }
            if (postedList!=null)
            {
                postedListAdapter?.updateAdapter(postedList!!)
                if (postChatbadge>0) {
                    setBadge(binding.tabLayoutMessages.getTabAt(1),postChatbadge)
                }

            }
        }

        if (currentItem==1)
        {
            viewLifecycleOwner.lifecycleScope.launch {
                binding.tabLayoutMessages.selectTab(binding.tabLayoutMessages.getTabAt(1))
            }
        }


    }



    override fun onTabSelected(tab: TabLayout.Tab?) {

        val b = tab?.customView!!.findViewById(android.R.id.text1) as TextView
        b.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))

        if (tab?.position==0) {
                binding.svJobs.queryHint = Localizer.labels.search_applied_jobs
            }
           else{
                binding.svJobs.queryHint = Localizer.labels.search_posted_jobs

            }
        }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
        val b = tab?.customView!!.findViewById(android.R.id.text1) as TextView
        b.setTextColor(ContextCompat.getColor(requireContext(),R.color.black))
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

fun setBadge(tab: TabLayout.Tab?,badgeCount:Int){
    if (tab != null && tab.customView != null) {
        val b = tab.customView!!.findViewById(R.id.badge) as TextView
        val v: View = tab.customView!!.findViewById(R.id.badgeCotainer)

        b.setText(badgeCount.toString())
        if (v != null&&badgeCount>0) {
            v.visibility = View.VISIBLE
        }else{
            v.visibility = View.GONE
        }
    }
}
}