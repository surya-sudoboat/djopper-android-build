package com.aquall.app.fragments

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView.OnQueryTextListener
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.adapters.CandidateAdapter
import com.aquall.app.databinding.FragmentCandidatesBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.ChatRepositary
import com.aquall.app.repositary.model.CandidateModel
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.CustomItemTouchHelper
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.aquall.app.viewholders.CandidateListViewHolder
import com.aquall.app.viewmodel.JobViewModel

class CandidatesListFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
lateinit var binding:FragmentCandidatesBinding
var mUserList:List<CandidateModel>? = null
    lateinit var candidateAdapter:CandidateAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCandidatesBinding.inflate(inflater, container, false)

        binding.svJobs.queryHint = Localizer.labels.search_candidates_list
        val string = arguments?.getString("data", null)
        binding.svJobs.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
              return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0!=null&&binding.rvCandidates.adapter!=null) {
                    (binding.rvCandidates.adapter as CandidateAdapter).filter(p0)
                }
                return true
            }
        })
        if (string!=null)
        {
            val fromJson = JsonUtil.fromJson(string, IntentBundle::class.java)
            fromJson.job?._id.let {
            binding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }
                binding.textToolbarTitle.text = fromJson.job?.name
                val viewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
                    JobViewModel::class.java
                )

                fromJson.job?._id?.let { it1 -> viewModel.getCandidates(it1).observe(viewLifecycleOwner,object : Observer<List<CandidateModel>>{
                    override fun onChanged(t: List<CandidateModel>?) {
                        if (t!=null) {
                            mUserList = t
                            binding.textCandidatesCount.text = "${t.size} ${Localizer.labels.candidates}"
                            binding.rvCandidates.layoutManager =  LinearLayoutManager(requireContext())
                             candidateAdapter =
                                CandidateAdapter(t, object : CandidateListViewHolder.Iteractor {
                                    override fun onItemClick(position: Int) {
                                        if (fromJson.isCandidateClickable) {
                                            fromJson.screenType = 1
                                            fromJson.candidate = mUserList?.get(position)?.applicant
                                            fromJson.chatRoom = mUserList?.get(position)?.chat_room
                                            activity?.replaceFragment(
                                                ScreenName.MESSAGEDETAIL,
                                                JsonUtil.toJson(fromJson, IntentBundle::class.java),
                                                false
                                            )
                                        }
                                    }

                                    override fun getJob(): Job? {
                                        return fromJson.job
                                    }
                                })
                            binding.rvCandidates.adapter = candidateAdapter
                        }
                    }

                }) }

                if (fromJson.isCandidateClickable) {
                   var customItemTouchHelper =
                        CustomItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                            private var mIcon: Drawable? = null
                            private var mBackground: ColorDrawable? = null
                            private var isEnabled = true
                            init {
                                mIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_trash_delete);
                                mBackground = ColorDrawable(Color.RED);
                            }

                            fun enable(){
                                isEnabled = true
                            }

                            fun disable(){
                                isEnabled = false
                            }

                            override fun onMove(
                                recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder
                            ): Boolean {
                                return false
                            }

                            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                                deleteChat(viewHolder.layoutPosition)
                            }

                            override fun onChildDraw(
                                c: Canvas,
                                recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                dX: Float,
                                dY: Float,
                                actionState: Int,
                                isCurrentlyActive: Boolean
                            ) {
                                mIcon!!.setTint(Color.WHITE)
                                val itemView = viewHolder.itemView
                                val backgroundCornerOffset =
                                    25 //so mBackground is behind the rounded corners of itemView
                                val iconMargin = (itemView.height - mIcon!!.intrinsicHeight) / 2
                                val iconTop = itemView.top + (itemView.height - mIcon!!.intrinsicHeight) / 2
                                val iconBottom = iconTop + mIcon!!.intrinsicHeight
                                if (dX > 0) { // Swiping to the right
                                    val iconLeft = itemView.left + iconMargin
                                    val iconRight = iconLeft + mIcon!!.intrinsicWidth
                                    mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                                    mBackground!!.setBounds(
                                        itemView.left, itemView.top,
                                        itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom
                                    )
                                } else if (dX < 0) { // Swiping to the left
                                    val iconLeft = itemView.right - iconMargin - mIcon!!.intrinsicWidth
                                    val iconRight = itemView.right - iconMargin
                                    mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                                    mBackground!!.setBounds(
                                        itemView.right + dX.toInt() - backgroundCornerOffset,
                                        itemView.top, itemView.right, itemView.bottom
                                    )
                                } else { // view is unSwiped
                                    mIcon!!.setBounds(0, 0, 0, 0)
                                    mBackground!!.setBounds(0, 0, 0, 0)
                                }

                                mBackground!!.draw(c)
                                mIcon!!.draw(c)

                                super.onChildDraw(
                                    c,
                                    recyclerView,
                                    viewHolder,
                                    dX,
                                    dY,
                                    actionState,
                                    isCurrentlyActive
                                )
                            }
                        })
                    customItemTouchHelper.attachToRecyclerView(binding.rvCandidates)
                }
                viewModel.getNetworkData().observe(viewLifecycleOwner
                ) { t -> binding.networkStatus = t }
            }
        }

        return binding.root
    }

    fun deleteChat(position:Int){
        candidateAdapter?.notifyItemChanged(position)
        val get = candidateAdapter?.getItem(position)
        AlertDialog.show(requireContext(),Localizer.labels.do_you_want_to_delete_this_chat,true,object:
            AlertDialog.Listener{
            override fun success() {
                val chat_room_id = get?.chat_room?._id
                if (chat_room_id!=null) {
                    ChatRepositary().deleteChat(chat_room_id)
                }
                get?.let {
                    candidateAdapter?.remove(it)
                }

            }

            override fun cancel() {

            }
        },viewLifecycleOwner)
    }


}