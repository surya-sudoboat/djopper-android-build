package com.aquall.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.MultiAutoCompleteTextView
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.R
import com.aquall.app.adapters.CategoryAutoCompleteAdapter
import com.aquall.app.adapters.SubCategoryAutoCompleteAdapter
import com.aquall.app.databinding.FragmentJobCreationBinding
import com.aquall.app.databinding.FragmentJobDescriptionBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.JobRepositary
import com.aquall.app.repositary.model.CreateJobRequest
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.*
import com.aquall.app.viewmodel.CategoriesViewModel
import com.aquall.app.viewmodel.JobViewModel
import com.aquall.app.viewmodel.ProfileAuthViewModel
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import android.provider.OpenableColumns
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.aquall.app.Localizer
import com.aquall.app.utils.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class JobCreationFragment : Fragment(), AdapterView.OnItemSelectedListener,
    CategoriesViewModel.SubCategoryObserver,CategoriesViewModel.CategoryObserver,PeriodDatePicker.OnDateSelected,Observer<Profile> {
    var locationResult: ActivityResultLauncher<Intent>? = null
    var selectedLocation:Location?=null
    var createdJob:Job? = null
    var selectedUri:Uri? = null
    var isEditJob = false
    var jobId:String?=null
    lateinit var getContent:ActivityResultLauncher<String>
    lateinit var requestPermissionLauncher:ActivityResultLauncher<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    getContent.launch("image/*")
                } else {

                }
            }
         getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            // Handle the returned Uri

             if (uri!=null) {
                 Logger.e(uri!!.path!!)
                 val fileName = getFileName(uri!!)
                 Logger.e(fileName!!)
                 binding.etImageName.setText(fileName)
                 selectedUri = uri
             }

         }

        locationResult  = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.let {
                    val place = Autocomplete.getPlaceFromIntent(it)
                    Log.e("LOCATION", "Place: ${place.name}, ${place.id}, ${place.latLng?.latitude}:${place.latLng?.longitude}")
                    binding.etLocationName.setText(place.name)
                    if (place.name!=null&&place.latLng!=null) {
                        val location = Location()
                        location.name = place.name!!
                        location.type = "Point"
                        location.coordinates = arrayListOf(place.latLng!!.longitude,place.latLng!!.latitude)
                        selectedLocation = location
                    }
                }
            }

            if (result.resultCode == AutocompleteActivity.RESULT_ERROR) {
                result.data?.let {
                    val status = Autocomplete.getStatusFromIntent(it)
                    status.statusMessage?.let { it1 -> Log.e("LOCATION", it1) }
                }
            }
        }
    }
    lateinit var binding:FragmentJobCreationBinding
    lateinit var viewModel: CategoriesViewModel
    lateinit var profileViewModel: ProfileAuthViewModel
    lateinit var jobViewModel: JobViewModel
    var selectedSubCategory:SubCategory? = null
    var selectedCategory:Category? =null
    var fromDate:String?=null
    var toDate:String?=null
    lateinit var categoryList:ArrayList<Category>
    lateinit var subCategoryList:ArrayList<SubCategory>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentJobCreationBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        binding.imageCrossBack.setOnClickListener {
            activity?.replaceFragment(ScreenName.DASHBOARD)
        }
        binding.autoCompleteCategory.isFocusableInTouchMode = false
        binding.autoCompleteCategory.onItemSelectedListener = this
        viewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            CategoriesViewModel::class.java
        )
        profileViewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            ProfileAuthViewModel::class.java
        )
        jobViewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            JobViewModel::class.java
        )
        profileViewModel.getProfile().observe(viewLifecycleOwner,this)
        val categories = viewModel.getCategories()
        if (categories.value!=null)
        {
            onCategoriesChanged(categories.value!!)
        }
        categories.observe(viewLifecycleOwner,this)
        val subCategories = viewModel.getSubCategories()
        subCategories.observe(viewLifecycleOwner,this)
        binding.etLocationName.isFocusableInTouchMode = false

        binding.etLocationName.setOnClickListener {
            val fields = listOf(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG)

            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(activity)



            locationResult?.launch(intent)
        }
        binding.autoCompleteSubCategory.isEnabled = false
        binding.autoCompleteSubCategory.isFocusableInTouchMode = false
        binding.autoCompleteSubCategory.setOnClickListener {
            (it as AutoCompleteTextView).showDropDown()
        }
        binding.etPeriodName.isFocusableInTouchMode =false
        binding.etPeriodName.setOnClickListener {
            val periodDatePicker = PeriodDatePicker(requireContext())
            periodDatePicker.mOnDateSelected = this
            periodDatePicker
                .show()
        }
        binding.etImageName.isFocusableInTouchMode = false
        binding.etImageName.setOnClickListener {
            when {
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED -> {
                    // You can use the API that requires the permission.
                    getContent.launch("image/*")
                }
                shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                AlertDialog.show(requireContext(),Localizer.labels.please_provide_the_read_storage_permission,false,
                    object : AlertDialog.Listener {
                        override fun success() {

                        }

                        override fun cancel() {

                        }

                    },viewLifecycleOwner)
            }
                else -> {
                    // You can directly ask for the permission.
                    // The registered ActivityResultCallback gets the result of this request.
                    requestPermissionLauncher.launch(
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                }
            }

        }
        binding.buttonPostJob.setOnClickListener {
            if (TextUtils.isEmpty(binding.etName.text)) {
                AlertDialog.show(requireContext(),Localizer.labels.please_provide_a_name,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(binding.autoCompleteCategory.text)) {
               AlertDialog.show(requireContext(),Localizer.labels.please_select_a_category,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(binding.autoCompleteSubCategory.text)) {
                AlertDialog.show(requireContext(),Localizer.labels.please_select_a_sub_category,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(binding.etDescriptionName.text)) {
                AlertDialog.show(requireContext(),Localizer.labels.please_provide_a_description,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(binding.etLocationName.text)||selectedLocation==null) {
                AlertDialog.show(requireContext(),Localizer.labels.please_provide_a_location,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(binding.etPeriodName.text)) {
                AlertDialog.show(requireContext(),Localizer.labels.please_select_a_period,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            if (!NetworkUtil.isNetworkConnected(requireContext()))
            {
                AlertDialog.show(requireContext(),Localizer.labels.no_internet_connection,false,null,viewLifecycleOwner)
                return@setOnClickListener
            }
            try {
                ProgressDialog.show(viewLifecycleOwner,requireContext())
                val createJobRequest = CreateJobRequest()
                createJobRequest.name = binding.etName.text.toString()
                createJobRequest.category_id = selectedCategory?._id
                createJobRequest.subcategory_id = selectedSubCategory?._id
                createJobRequest.description = binding.etDescriptionName.text.toString()
//                createJobRequest.setLocationName(binding.etLocationName.text.toString())
                createJobRequest.location = selectedLocation
                createJobRequest.mobile = myProfile.mobile!!
                createJobRequest.email = myProfile.email!!
                createJobRequest.start_date = fromDate!!
                createJobRequest.end_date = toDate!!
                createJobRequest.job_owner_id = myProfile._id
                var fileName:String? = null
                var fileData:ByteArray?= null
                if (selectedUri!=null)
                {
                    fileName = getFileName(selectedUri!!)
                    fileData = createImageData(selectedUri!!)
                }
                jobViewModel.createJob(createJobRequest,fileName,fileData,isEditJob,jobId).observe(viewLifecycleOwner) {
                    createdJob = it
                    it._id?.let { it1 ->
                        if (!isEditJob) {
                            jobViewModel.paymentDetail(it1).observe(viewLifecycleOwner) {
                                ProgressDialog.hide()
                                if (it != null) {
                                    val intentBundle = IntentBundle()
                                    intentBundle.job = createdJob
                                    intentBundle.paymentUrl = it
                                    intentBundle.takeToPayment = true
                                    Toast.makeText(requireContext(),Localizer.labels.job_updated_successfully,Toast.LENGTH_LONG).show()
                                    activity?.replaceFragment(ScreenName.JOBDESCRIPTION, intentBundle.toString(), false)
                                }
                            }
                        } else {
                            if (it != null) {
                                val intentBundle = IntentBundle()
                                intentBundle.job = createdJob
                                activity?.replaceFragment(ScreenName.JOBDESCRIPTION, intentBundle.toString(), false)
                            }
                        }
                    }
                }

                jobViewModel.getNetworkData().observe(viewLifecycleOwner, Observer {
                    if (it == DataStatus.DATA_AVAILABLE)
                    {


                    }else if(it == DataStatus.FAILED)
                    {
                        ProgressDialog.hide()
                        AlertDialog.show(requireContext(),Localizer.labels.job_offer_creation_failed,false,null,viewLifecycleOwner)
                    }
                })
            } catch (e: Exception) {
                ProgressDialog.hide()
            }

        }

        val data = arguments?.getString("data")
        if (data!=null&&!TextUtils.isEmpty(data))
        {

            val fromJson = JsonUtil.fromJson(data, Job::class.java)
            isEditJob = true
            jobId = fromJson._id
            if (!TextUtils.isEmpty(fromJson.image)) {
                binding.etImageName.setText(fromJson.image)
            }
            if (!TextUtils.isEmpty(fromJson.name)) {
                binding.etName.setText(fromJson.name)
            }
            if (!TextUtils.isEmpty(fromJson.description)) {
                binding.etDescriptionName.setText(fromJson.description)
            }
            if (fromJson.category!=null&&!TextUtils.isEmpty(fromJson.category?.name)) {
                binding.autoCompleteCategory.setText(fromJson.category?.name!!)
                selectedCategory = fromJson.category
            }
            if (fromJson.subcategory!=null&&!TextUtils.isEmpty(fromJson.subcategory?.name)) {
                binding.autoCompleteSubCategory.setText(fromJson.subcategory?.name!!)
                selectedSubCategory = fromJson.subcategory
                binding.autoCompleteSubCategory.isEnabled = true
            }

            if (fromJson.location!=null&&!TextUtils.isEmpty(fromJson.location?.name)) {
                binding.etLocationName.setText(fromJson.location?.name!!)
                selectedLocation = fromJson.location
            }


            if (fromJson.start_date!=null&&!TextUtils.isEmpty(fromJson.start_date)&&fromJson.end_date!=null&&!TextUtils.isEmpty(fromJson.end_date)) {
                onDate(getDate(fromJson.start_date!!), getDate(fromJson.end_date!!))
            }

            binding.textToolbarTitle.setText(Localizer.labels.update_job)
            binding.buttonPostJob.setText(Localizer.labels.update_job)

        }else{
            binding.textToolbarTitle.setText(Localizer.labels.post_a_job)
            binding.buttonPostJob.setText(Localizer.labels.post_a_job)
        }
        return binding.root
    }


    private fun createImageData(uri: Uri):ByteArray? {
        val inputStream = requireActivity().contentResolver.openInputStream(uri)
        inputStream?.buffered()?.use {
            return it.readBytes()
        }
        return null
    }
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        binding.autoCompleteSubCategory.isEnabled = false
    }

    override fun onSubCategoriesChanged(value: List<SubCategory>) {
        ProgressDialog.hide()
        subCategoryList = ArrayList(value);
        val categoryStringList = value.map { it.name!! }
        binding.autoCompleteSubCategory.setAdapter(activity?.let { CategoryAutoCompleteAdapter(it,categoryStringList) })
        binding.autoCompleteSubCategory.setOnItemClickListener { adapterView, view, i, l ->
            selectedSubCategory = subCategoryList[i]
        }
        binding.autoCompleteSubCategory.setOnClickListener{
            (it as AutoCompleteTextView).showDropDown()
        }

    }

    @SuppressLint("Range")
    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = requireActivity().getContentResolver().query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME!!))
                }
            }
            catch (e:Exception){

            }
            finally {
                cursor?.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    override fun onCategoriesChanged(value: List<Category>) {
        categoryList =ArrayList<Category>(value)
        val categoryStringList = categoryList.map {it.name }
        binding.autoCompleteCategory.setAdapter(activity?.let { CategoryAutoCompleteAdapter(it,categoryStringList) })
        binding.autoCompleteCategory.setOnItemClickListener { adapterView, view, i, l ->
            binding.autoCompleteSubCategory.isEnabled = true
            selectedCategory = categoryList[i]
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            if (selectedCategory!=null) {
                viewModel.getSubCategories(selectedCategory!!._id)
            }
            selectedSubCategory = null
            binding.autoCompleteSubCategory.setText("")
        }
        binding.autoCompleteCategory.setOnClickListener{
            (it as AutoCompleteTextView).showDropDown()
        }
    }

    override fun onDate(from: String,to:String) {
        fromDate = from
        toDate = to

        binding.etPeriodName.setText("${from} - ${to}")
    }

    lateinit var myProfile:Profile
    override fun onChanged(t: Profile?) {
        myProfile = t!!
    }
    fun getDate(dateStr: String):String {
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val mDate = formatter.parse(dateStr) // this never ends while debugging
            return formatter2.format(mDate)
        } catch (e: Exception){
        }
        return dateStr
    }


    override fun onDestroyView() {
        super.onDestroyView()
        arguments?.clear()
    }

}