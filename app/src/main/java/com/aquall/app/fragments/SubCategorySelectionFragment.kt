package com.aquall.app.fragments

import android.content.ClipData.Item
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.ListItem
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.newScreen
import com.aquall.app.ui.AquallTopBar
import com.aquall.app.utils.Logger
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.aquall.app.utils.model.JobsList
import com.aquall.app.utils.model.SearchKeys
import com.aquall.app.utils.model.SubCategory
import com.aquall.app.viewholders.AdSenseViewHolder
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewmodel.CategoriesViewModel
import com.aquall.app.viewmodel.JobViewModel
import java.util.stream.IntStream.range

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SubCategorySelectionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SubCategorySelectionFragment : Fragment() {
    private val TAG = "SubCategorySelectionFra"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val get = arguments?.getString("data")
        Logger.e(TAG+": ${get}")
        var searchKeys = SearchKeys.fromString(get!!)
        val category = searchKeys.category.get(0)
        return ComposeView(requireContext()).apply{
            setContent {
                Column(Modifier.fillMaxSize()) {
                    AquallTopBar(titleText = category.name, homeClick = {
                        requireActivity().onBackPressed()
                    })
                    Screen(category = category)
                }
            }
        }
    }


    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun Screen(category:Category,categoryViewModel:CategoriesViewModel = viewModel(),jobViewModel: JobViewModel = viewModel()){
        val subCategoryListState = categoryViewModel.getSubCategories().observeAsState()
        val networkStatus = categoryViewModel.getSubCategories(category._id).observeAsState()
        val jobList = jobViewModel.getJobList().observeAsState(null)
        jobViewModel.search(SearchKeys().apply {
            this.category = arrayListOf(category)})
        Column(Modifier.fillMaxSize()) {

        ScreenState(modifier = Modifier.padding(10.dp), networkStatus = networkStatus) {
            DropDownTextField(subCategoryListState = subCategoryListState){
                jobViewModel.search(SearchKeys().apply {
                    this.category = arrayListOf(category)
                    if (it.size>0)
                        this.subCategory = ArrayList(it)})

            }
        }
        Joblist(jobListState = jobList, favoriteChanged = {
                job, value ->
            if (!value) {
                jobViewModel.removeFavorite(job._id!!)
            }else{
                jobViewModel.markAsFavorite(job._id!!)
            }
        })
        }

    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun Joblist(jobListState:State<JobsList?>,favoriteChanged:(job: Job, value:Boolean)->Unit){
        val joblist = jobListState.value
        if(joblist!=null){
        val chatRooms = joblist?.chatRoom
        LazyColumn {
            for (i in range(0, joblist.jobs.size)) {
                item(i) {
                    var job = joblist.jobs.get(i)
                    Spacer(modifier = Modifier.height(10.dp))
                    if(job._id==null){
                    AndroidView(modifier = Modifier
                        .fillMaxWidth(), factory = {

                            val instance = AdSenseViewHolder.getInstance(null, context = it)
                            instance.bind()
                            instance.itemView


                    })}
                    else{
                        AndroidView(modifier = Modifier
                            .height(230.dp)
                            .fillMaxWidth(), factory = {
                        val instance = JobListViewHolder.getInstance(null, context = it)
                        instance.bind(job, object : JobListViewHolder.IteractionListener {
                            override fun onFavoriteChanged(position: Int, value: Boolean) {
                                favoriteChanged(job, value)
                            }
                            override fun onItemClicked(position: Int) {
                                val intentBundle = IntentBundle()
                                intentBundle.job = job
                                if (chatRooms != null && job._id != null && chatRooms?.get(job._id) != null && chatRooms?.size!! > 0) {
                                    intentBundle.chatRoom = chatRooms?.get(job._id)!!.get(0)
                                }
                                activity?.newScreen(
                                    ScreenName.JOBDESCRIPTION,
                                    intentBundle.toString()
                                )
                            }

                            override fun isFavoriteList(): Boolean {
                                return false
                            }

                            override fun showFavoriteAndKnowMore(): Boolean {
                                return true
                            }

                            override fun deleteAlertText(): String {
                                return ""
                            }

                            override fun onDeleteClicked(position: Int) {
                                TODO("Not yet implemented")
                            }

                            override fun onEditClicked(position: Int) {
                                TODO("Not yet implemented")
                            }

                            override fun getMessagesCount(position: Int): Int {
                                return 0
                            }
                        })
                        instance.itemView

                    })
                }
                }

            }

        }

        }else{
                Column(modifier = Modifier.fillMaxSize()) {

                    CircularProgressIndicator(modifier = Modifier.align(Alignment.CenterHorizontally))
                }
        }



    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun DropDownTextField(subCategoryListState:State<List<SubCategory>?>,getJobs:@Composable (subCategory:List<SubCategory>)->Unit){
        val options:List<SubCategory?>? = subCategoryListState.value
        var exp by remember { mutableStateOf(false) }
        var selectedOptions = remember { mutableStateListOf<SubCategory>() }
        var getJobs= remember {
            mutableStateOf(false)
        }
        ExposedDropdownMenuBox(modifier = Modifier.fillMaxWidth(), expanded = exp, onExpandedChange = {
            exp = !exp
            if (it)
                getJobs.value = false
        }) {
            OutlinedTextField(
                modifier= Modifier.fillMaxWidth(),
                value = selectedOptions.joinToString(separator = ",") { it.name as CharSequence },
                onValueChange = {},
                readOnly = true,
                label = { Text(text = Localizer.labels.sub_category!!)},
                trailingIcon = {
                    ExposedDropdownMenuDefaults.TrailingIcon(expanded = exp)
                },
                colors = ExposedDropdownMenuDefaults.textFieldColors(backgroundColor = Color.Transparent)
            )
            if (options?.isNotEmpty()==true) {
                DropdownMenu(
                    modifier = Modifier
                        .exposedDropdownSize(true)
                        .fillMaxWidth(),
                    expanded = exp, onDismissRequest = {
                    exp = false
                }) {
                    options.forEach { option ->
                        DropdownMenuItem(
                            modifier = Modifier.fillMaxWidth(),
                            onClick= {
                                if (selectedOptions.contains(option)){
                                    selectedOptions.remove(option)
                                }else{
                                    option?.let {
                                    selectedOptions.add(option)
                                    }
                                }

                            }
                        ) {
                            Checkbox(checked = selectedOptions.contains(option), enabled = true, onCheckedChange ={
                                if (selectedOptions.contains(option)){
                                    selectedOptions.remove(option)
                                }else{
                                    option?.let {
                                        selectedOptions.add(option)
                                    }
                                }
                            } ,colors = CheckboxDefaults.colors(colorResource(id = R.color.colorPrimary)))
                            Text(text = option?.name?:"")
                        }
                    }
                    Image(painter = painterResource(id = R.drawable.arrow_up), contentDescription = "up arrow", modifier = Modifier
                        .align(Alignment.End)
                        .clickable { exp = !exp })
                }
            }
        }
        val verticalGradientBrush = Brush.verticalGradient(
            colors = listOf(
                Color(0xFFD91830),
                Color(0xFFFF6679)
            )
        )
        Button(onClick = {
                  getJobs.value = true
        }, modifier = Modifier.fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.colorSecondaryVariant), contentColor = Color.White),
            contentPadding = PaddingValues(0.dp),
            shape = RoundedCornerShape(8.dp)) {
            Localizer.labels.search_button?.let {
                Box(Modifier.background(brush = verticalGradientBrush).fillMaxWidth()) {
                Text(text = it,modifier = Modifier
                    .height(ButtonDefaults.MinHeight)
                    .align(Alignment.Center)
                    .padding(8.dp)
                )
                }
            }
        }

        if (!exp  && getJobs.value){
            getJobs(selectedOptions)
        }


    }


    @Composable
    fun ScreenState(modifier:Modifier=Modifier,networkStatus:State<Int?>,dataAvailableState:@Composable (it:ColumnScope)->Unit){
        val value = networkStatus.value
        Column(modifier.fillMaxWidth()) {
        if (value==DataStatus.LOADING) {
        }
        else if(value==DataStatus.DATA_AVAILABLE){
            dataAvailableState(this)
        }
        else{
            Text(text = Localizer.labels.no_jobs_found!!)
        }    
        }
    }




}