package com.aquall.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.usage.NetworkStats
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.R
import com.aquall.app.databinding.FragmentEditProfileBinding
import com.aquall.app.databinding.FragmentProfileBinding
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.*
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Profile
import com.aquall.app.viewmodel.ProfileAuthViewModel
import android.content.DialogInterface
import android.content.Intent

import android.text.Editable
import android.text.InputType
import android.util.Log
import android.widget.AutoCompleteTextView

import android.widget.EditText
import androidx.activity.result.ActivityResult
import androidx.core.view.marginLeft
import com.android.volley.ClientError
import com.aquall.app.Localizer
import com.aquall.app.adapters.CategoryAutoCompleteAdapter
import com.aquall.app.adapters.CategoryMultiAutoCompleteAdapter
import com.aquall.app.adapters.SubCategoryAutoCompleteAdapter
import com.aquall.app.repositary.model.CompanyProfile
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.Location
import com.aquall.app.utils.model.SubCategory
import com.aquall.app.viewmodel.CategoriesViewModel
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
lateinit var getContent: ActivityResultLauncher<String>
lateinit var requestPermissionLauncher:ActivityResultLauncher<String>
var selectedSendNotification:Boolean = true

/**
 * A simple [Fragment] subclass.
 * Use the [EditProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EditProfileFragment : Fragment() ,Observer<Profile>,CategoriesViewModel.SubCategoryObserver,CategoriesViewModel.CategoryObserver{
    var locationResult:ActivityResultLauncher<Intent>? = null
    var selectedSendNotification:Boolean? = null
    var selectedSoundNotification:Boolean?= null
    var selectedLocation:Location?=null
    var selectedSubCategories:ArrayList<SubCategory>?=null
    var selectedCategories:ArrayList<Category>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    getContent.launch("image/*")
                } else {

                }
            }

        locationResult  = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                result.data?.let {
                    val place = Autocomplete.getPlaceFromIntent(it)
                    Log.e("LOCATION", "Place: ${place.name}, ${place.id}, ${place.latLng?.latitude}:${place.latLng?.longitude}")
                    binding.etlocation.setText(place.name)
                    if (place.name!=null&&place.latLng!=null) {
                        val location = Location()
                        location.name = place.name!!
                        location.type = "Point"
                        location.coordinates = arrayListOf(place.latLng!!.longitude,place.latLng!!.latitude)
                        selectedLocation = location
                    }
                }
            }

            if (result.resultCode == AutocompleteActivity.RESULT_ERROR) {
                result.data?.let {
                    val status = Autocomplete.getStatusFromIntent(it)
                    status.statusMessage?.let { it1 -> Log.e("LOCATION", it1) }
                }
            }
        }
        getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            // Handle the returned Uri
            if (uri!=null) {
                Logger.e(uri!!.path!!)
                val fileName = getFileName(uri!!)
                Logger.e(fileName!!)
               ImageProcessor.into(binding.ivProfilePic).setCircleCrop().loadResource(uri.toString(),R.drawable.ic_abstract_user)
                ProgressDialog.show(viewLifecycleOwner,requireContext())
                val authRepositary = AuthRepositary()
                val createImageData = createImageData(uri)
                if (createImageData!=null)
                {
                    authRepositary.updateProfilePicture(fileName,createImageData)
                }
                authRepositary.networkStatus.observe(viewLifecycleOwner) {
                    if (it == DataStatus.DATA_AVAILABLE) {
                        ProgressDialog.hide()
                        Toast.makeText(requireContext(), Localizer.labels.upload_successful, Toast.LENGTH_SHORT)
                            .show()
                    } else if (it == DataStatus.FAILED) {
                        ProgressDialog.hide()
                        if (authRepositary.latestError.cause is ClientError && (authRepositary.latestError.cause as ClientError).networkResponse.statusCode==413){
                            Toast.makeText(requireContext(), "File size is too large", Toast.LENGTH_SHORT).show()
                        }
                        Toast.makeText(requireContext(), Localizer.labels.upload_failed, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun createImageData(uri: Uri):ByteArray? {
        val inputStream = requireActivity().contentResolver.openInputStream(uri)
        inputStream?.buffered()?.use {
           return it.readBytes()
        }
        return null
    }

    @SuppressLint("Range")
    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = requireActivity().getContentResolver().query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME!!))
                }
            }
            catch (e:Exception){ }
            finally { cursor?.close() }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    lateinit var binding:FragmentEditProfileBinding
    lateinit var categoryViewModel: CategoriesViewModel
    lateinit var companyfields:List<EditText>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=  FragmentEditProfileBinding.inflate(inflater, container, false)
        companyfields = listOf(binding.etHouseNumber,binding.etWebsite,binding.etStreetName,binding.etCity,binding.etiban,binding.etkvkNumber,binding.etCountry,binding.etvatNumber,binding.etPostalCode)
        binding.label = Localizer.labels
        binding.imageBackArrow.setOnClickListener { activity?.onBackPressed()}
        val viewModel = ViewModelProvider(
            { viewModelStore },
            defaultViewModelProviderFactory
        ).get(ProfileAuthViewModel::class.java)
        categoryViewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            CategoriesViewModel::class.java
        )
        val categories = categoryViewModel.getCategories()
        if (categories.value!=null)
        {
            onCategoriesChanged(categories.value!!)
        }
        categories.observe(viewLifecycleOwner,this)
        viewModel.getProfile().observe(viewLifecycleOwner,this)
        val subCategories = categoryViewModel.getSubCategories()
        subCategories.observe(viewLifecycleOwner,this)

        binding.isEditable =false
        binding.etemail.isEnabled = false
        binding.autoCompleteCategory.isEnabled =false
        binding.autoCompleteSubCategory.isEnabled =false
        binding.etlocation.isEnabled = false
        binding.switchNotificationSound.isEnabled = false
        binding.switchNotification.isEnabled = false
        binding.etmobile.isEnabled=false
        companyfields.forEach { it.isEnabled = false }
        binding.countryCodePicker.visibility = View.GONE

        val arrayAdapter = ArrayAdapter<String>(
            requireContext(), android.R.layout.simple_list_item_1,
            arrayListOf(Localizer.labels.male, Localizer.labels.female,Localizer.labels.other)
        )
        binding.etgender.setAdapter(arrayAdapter)
        binding.etgender.setOnClickListener { if (binding.isEditable){
            if (binding.etgender.text.length>0) {
                (binding.etgender.adapter as ArrayAdapter<String>).filter.filter(null)
            }
            binding.etgender.showDropDown()
        }
        }
        binding.layoutRangeSeek.automCompleteRange.isEnabled = false
        binding.etBirthday.setOnClickListener {
            if (binding.isEditable) {
                val datePickerDialog = DatePickerDialog(requireContext())
                datePickerDialog.setOnDateSetListener { dp, y, m, d ->
                    binding.etBirthday.setText("${d}-${m+1}-${y}")
                }
                datePickerDialog.show()
            }
        }
        binding.buttonEditProfilePassword.setOnClickListener{
            passwordAlert.showPasswordAlert(requireContext())
        }
        binding.buttonEditProfile.setOnClickListener {
            if (!binding.isEditable) {
                binding.isEditable = true
                binding.etname.isEnabled = true
                binding.etlocation.isEnabled = true
                binding.etemail.isEnabled = true
                binding.etmobile.isEnabled = true
                binding.countryCodePicker.isEnabled = true
                binding.etLastname.isEnabled = true
                binding.etgender.isEnabled = true
                binding.autoCompleteCategory.isEnabled =true
                binding.etBirthday.isEnabled = true
                binding.countryCodePicker.visibility = View.VISIBLE
                companyfields.forEach { it.isEnabled = true }


                binding.layoutRangeSeek.automCompleteRange.isEnabled = true
                if (selectedCategories!=null&&!selectedCategories!!.isEmpty())
                {
                    binding.autoCompleteSubCategory.isEnabled = true
                }
                binding.etname.requestFocus()
                binding.switchNotification.isEnabled = true
                binding.switchNotificationSound.isEnabled = true
                binding.buttonEditProfile.visibility= View.GONE
                binding.buttonSave.visibility= View.VISIBLE
            }
        }

        binding.buttonSave.setOnClickListener {
            if (selectedSubCategories==null|| selectedSubCategories!!.size<=0)
            {
                Toast.makeText(requireContext(),Localizer.labels.you_need_to_choose_at_least_one_sub_category,Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            binding.switchNotification.isEnabled = false
            binding.switchNotificationSound.isEnabled = false
            binding.buttonEditProfile.visibility= View.VISIBLE
            binding.buttonSave.visibility= View.GONE
            clearFocus()
            binding.isEditable = false
            binding.etname.isEnabled =false
            binding.etBirthday.isEnabled =false
            binding.etgender.isEnabled =false
            binding.etlocation.isEnabled =false
            binding.etLastname.isEnabled =false
            binding.etlocation.isEnabled = false
            binding.etmobile.isEnabled =false
            binding.etemail.isEnabled = false
            binding.countryCodePicker.visibility = View.GONE
            companyfields.forEach { it.isEnabled = false }

            binding.autoCompleteCategory.isEnabled =false
            binding.autoCompleteSubCategory.isEnabled =false
            binding.layoutRangeSeek.automCompleteRange.isEnabled = false
            binding.countryCodePicker.isEnabled = false
            val profile = binding.profile
            if (!TextUtils.isEmpty(binding.etBirthday.text))
            {
                profile?.DOB = binding.etBirthday.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etname.text))
            {
                profile?.firstName  = binding.etname.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etLastname.text))
            {
                profile?.lastName  = binding.etLastname.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etgender.text))
            {
                profile?.gender = binding.etgender.text.toString().lowercase()
            }
            if (!TextUtils.isEmpty(binding.etemail.text))
            {
                profile?.email = binding.etemail.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etprofession.text))
            {
                profile?.profession = binding.etprofession.text.toString()
            }
            if(selectedLocation!=null)
            {
                profile?.location = selectedLocation
            }
            if (selectedCategories!=null)
            {
                profile?.preferred_category = selectedCategories
            }
            if (selectedSubCategories!=null)
            {
                profile?.preferred_subcategory  = selectedSubCategories
            }
            profile?.countryCode = binding.countryCodePicker.selectedCountryNameCode
            profile?.companyProfile = CompanyProfile()
            profile?.companyProfile?.email = binding.countryCodePicker.selectedCountryNameCode
            if (!TextUtils.isEmpty(binding.etkvkNumber.text))
            {
            profile?.companyProfile?.kvkNumber = binding.etkvkNumber.text.toString().toInt()
            }
            if (!TextUtils.isEmpty(binding.etLastname.text))
            {
                profile?.companyProfile?.companyName = binding.etLastname.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etCity.text))
            {
                profile?.companyProfile?.City = binding.etCity.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etCountry.text))
            {
                profile?.companyProfile?.country = binding.etCountry.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etiban.text))
            {
                profile?.companyProfile?.iban = binding.etiban.text.toString().toInt()
            }
            if (!TextUtils.isEmpty(binding.etname.text))
            {
                profile?.companyProfile?.nameOfContacperson = binding.etname.text.toString()
            }
            if (!TextUtils.isEmpty(binding.etPostalCode.text))
            {
                profile?.companyProfile?.postalCode = binding.etPostalCode.text.toString()
            }
            profile?.is_notification_enabled = binding.switchNotification.isChecked
            profile?.is_notification_sound_enabled = binding.switchNotificationSound.isChecked
            profile?.range = binding.layoutRangeSeek.automCompleteRange.progress.toString()
            profile?.companyProfile?.userId =profile?._id
            Logger.e(profile?.toString()!!)
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            val authRepositary = AuthRepositary()
            authRepositary.updateProfile(profile)
            authRepositary.networkStatus.observe(viewLifecycleOwner, Observer {
                if (it==DataStatus.DATA_AVAILABLE)
                {
                    ProgressDialog.hide()
                    Toast.makeText(requireContext(),Localizer.labels.profile_updated_successfuly,Toast.LENGTH_SHORT).show()
                }else if (it==DataStatus.FAILED){
                    ProgressDialog.hide()
                    Toast.makeText(requireContext(),Localizer.labels.profile_updation_failed,Toast.LENGTH_SHORT).show()
                }
            })
        }
        binding.etlocation.setOnClickListener {
            // Set the fields to specify which types of place data to
            // return after the user has made a selection.
            val fields = listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(activity)
            locationResult?.launch(intent)
        }
        binding.ivProfilePic.setOnClickListener {
            when {
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED -> {
                    // You can use the API that requires the permission.
                    getContent.launch("image/*")
                }
                shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) -> {
                    AlertDialog.show(requireContext(),Localizer.labels.please_provide_the_read_storage_permission,false,
                        object : AlertDialog.Listener {
                            override fun success() {}
                            override fun cancel() {}
                        },viewLifecycleOwner)
                }
                else -> {
                    // You can directly ask for the permission.
                    // The registered ActivityResultCallback gets the result of this request.
                    requestPermissionLauncher.launch(
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                }
            }
        }
        return binding.root
    }



    override fun onChanged(t: Profile?) {
        if (t!=null)
        {
            binding.profile = t
            try {
                binding.layoutRangeSeek.automCompleteRange.progress = t.range?.toInt()?:10
            } catch (e: Exception) {
            }
            if (t.image!=null)
            {
                ImageProcessor.into(binding.ivProfilePic).setCircleCrop().loadResource(
                    ServiceUrl.BASE_URL+t.image,R.drawable.ic_abstract_user)
            }
            if (t.preferred_category!=null) {
                selectedCategories = t.preferred_category
                binding.autoCompleteCategory.setText(getCategoryString(t.preferred_category!!.map { it.name }))
            }
            if (t.preferred_subcategory!=null) {
                selectedSubCategories = t.preferred_subcategory
                binding.autoCompleteSubCategory.setText(getCategoryString(t.preferred_subcategory!!.map { it.name!! }))
            }

            if(t.countryCode!=null){
                binding.countryCodePicker.setCountryForNameCode(t.countryCode)
            }


        }
    }

    fun getCategoryString(selected:List<String>):String
    {
        var concatenatedString:String = ""
        var i=0
        while ( i<selected.size){
            concatenatedString+=selected.get(i)
            if (i!=selected.size-1)
                concatenatedString+=","
            i++
        }
        return concatenatedString
    }

    fun clearFocus()
    {
        binding.etBirthday.clearFocus()
        binding.etPassword.clearFocus()
        binding.etemail.clearFocus()
        binding.etgender.clearFocus()
        binding.etname.clearFocus()
        binding.etprofession.clearFocus()
        binding.etLastname.clearFocus()
        binding.etmobile.clearFocus()
        hideSoftKeyboard()
    }

    private fun hideSoftKeyboard() {
        val windowToken = view?.rootView?.windowToken
        windowToken?.let{
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it, 0)
        }
    }

    lateinit var categoryList:ArrayList<Category>
    override fun onCategoriesChanged(value: List<Category>) {
        categoryList =ArrayList<Category>(value)
        val categoryAutoCompleteAdapter = CategoryMultiAutoCompleteAdapter(requireActivity(),value)
        categoryAutoCompleteAdapter.changeListener = object :  CategoryMultiAutoCompleteAdapter.onChangeListener{
            override fun onAutocompleteSelect(value: String, positions: ArrayList<Category>) {
                binding.autoCompleteCategory.setText(value)
                selectedCategories = positions
                binding.autoCompleteSubCategory.isEnabled = !positions.isEmpty()
                binding.autoCompleteSubCategory.setText("")
                if (positions.size>0) {
                    categoryViewModel.getSubCategories(selectedCategories!!.map { it._id })
                }
                selectedSubCategories = null
                binding.autoCompleteSubCategory.setText("")
            }

        }
        if (selectedCategories!=null&&selectedCategories!!.size>0)
        {
            val arrayList = ArrayList(selectedCategories!!.map { it.name })
            categoryAutoCompleteAdapter.selected = arrayList!!
            categoryAutoCompleteAdapter.selectedPositions = ArrayList(selectedCategories)
            categoryViewModel.getSubCategories(selectedCategories!!.map { it._id })
        }
        binding.autoCompleteCategory.setAdapter(categoryAutoCompleteAdapter)
        binding.autoCompleteCategory.setOnClickListener{
            (it as AutoCompleteTextView).showDropDown()
        }
    }

    lateinit var subCategoryList:ArrayList<SubCategory>
    override fun onSubCategoriesChanged(value: List<SubCategory>) {
        subCategoryList =ArrayList<SubCategory>(value)
        val subCategoryAutoCompleteAdapter = SubCategoryAutoCompleteAdapter(requireActivity(),value)
        subCategoryAutoCompleteAdapter.changeListener = object :  SubCategoryAutoCompleteAdapter.onChangeListener{
            override fun onAutocompleteSelect(value: String, positions: ArrayList<SubCategory>) {
                binding.autoCompleteSubCategory.setText(value)
                selectedSubCategories = positions
                if(positions.size<=0){
                    Toast.makeText(requireContext(),Localizer.labels.you_need_to_choose_at_least_one_sub_category,Toast.LENGTH_SHORT).show()
                }
            }

        }
        if (selectedSubCategories!=null)
        {
            subCategoryAutoCompleteAdapter.selected = ArrayList(selectedSubCategories!!.map { it.name!! })
            subCategoryAutoCompleteAdapter.selectedPositions = selectedSubCategories!!
        }
        binding.autoCompleteSubCategory.setAdapter(subCategoryAutoCompleteAdapter)
        binding.autoCompleteSubCategory.setOnClickListener{
            (it as AutoCompleteTextView).showDropDown()
        }
    }

    var passwordAlert:PasswordAlert = object : PasswordAlert() {
        override fun success(password: String) {
            val authRepositary = AuthRepositary()
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            authRepositary.changePassword(binding.profile?.password!!,password)
            authRepositary.networkStatus.observe(viewLifecycleOwner) {
                if (it == DataStatus.FAILED) {
                    ProgressDialog.hide()
                    ErrorCode.getMessage(
                        authRepositary.latestError,
                        requireContext(),
                        null
                    )
                }
                if (it == DataStatus.DATA_AVAILABLE) {
                    ProgressDialog.hide()
                    Toast.makeText(
                        requireContext(),
                        Localizer.labels.successfully_updated_password,
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.etPassword.setText(password)
                }
            }
        }

        override fun confirmPasswordWrong() {
            Toast.makeText(requireContext(),Localizer.labels.the_passwords_dont_match,Toast.LENGTH_SHORT).show()
            if (binding.profile?.password!=null) {
                binding.etPassword.setText(binding.profile?.password!!)
            }else{
                binding.etPassword.setText("")
            }
        }

        override fun userCancelled() {
        }
    }

    public  fun checkIfDataIsEditing():Boolean{
        if (binding.isEditable)
        {
            AlertDialog.show(requireContext(),Localizer.labels.do_you_want_to_discard_your_changes_to_profile,true,object:AlertDialog.Listener{
                override fun success() {
                    binding.isEditable = false
                    activity?.onBackPressed()
                }

                override fun cancel() {

                }
            },viewLifecycleOwner)
            return true
        }
        return false
    }




}