package com.aquall.app.fragments

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.text.format.DateUtils
import android.text.format.DateUtils.SECOND_IN_MILLIS
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.databinding.FragmentJobDescriptionBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.JobRepositary
import com.aquall.app.repositary.model.Payment
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.updateArguments
import com.aquall.app.utils.*
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.aquall.app.viewmodel.JobViewModel
import com.aquall.app.viewmodel.ProfileAuthViewModel
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [JobDescriptionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class JobDescriptionFragment : Fragment(),View.OnClickListener,AlertDialog.Listener {

lateinit var binding:FragmentJobDescriptionBinding
lateinit var job: Job
lateinit var jobViewModel: JobViewModel
lateinit var messageBundle :IntentBundle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentJobDescriptionBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        ImageProcessor.into(binding.ivJobDescriptionBanner).loadResource(R.drawable.ic_camera)
        binding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }
        var data = arguments?.getString("data",null)
        if (data!=null) {
            messageBundle = IntentBundle.fromString(data)
            if (messageBundle.job!=null) {
                job = messageBundle.job!!
                updateUi()
                if (messageBundle.takeToPayment) {
                    messageBundle.takeToPayment  = false
                    activity?.updateArguments(messageBundle.toString())
                    activity?.replaceFragment(ScreenName.PAYMENTDETAIL,messageBundle.paymentUrl,true)
                }

                jobViewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
                    JobViewModel::class.java
                )

                if (job._id!=null) {
                    getJobData(job._id!!)
                }
            }else{
                val jobOfferId = messageBundle.jobOfferId
                if (jobOfferId!=null) {
                    ProgressDialog.show(viewLifecycleOwner,requireContext())
                    getJobData(jobOfferId!!)
                }
            }
        }
        val viewModel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory).get(
            ProfileAuthViewModel::class.java
        )

        return binding.root
    }

    private fun getJobData(id:String) {
        jobViewModel.getJobDescription(id).observe(viewLifecycleOwner,
            object : Observer<Job> {
                override fun onChanged(t: Job?) {
                    if (t != null) {
                        job = t
                        updateUi()
                    }
                }

            })
        jobViewModel.getNetworkData().observe(viewLifecycleOwner, Observer {
            if (it == DataStatus.FAILED) {
                ProgressDialog.hide()
                Toast.makeText(
                    requireContext(),
                    Localizer.labels.couldnt_fetch_job_information_job_may_be_deleted_or_expired,
                    Toast.LENGTH_SHORT
                ).show()
                binding.buttonApplyJob.visibility = View.GONE
                binding.cbFavorite.visibility = View.GONE
            }else if (it== DataStatus.DATA_AVAILABLE){
                ProgressDialog.hide()
            }
        })
    }

    fun getPostedData(updatedAt:String):String
    {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        val parse = formatter.parse(updatedAt)
        return DateUtils.getRelativeTimeSpanString(parse.time,Date().time,SECOND_IN_MILLIS).toString()
    }

    private fun updateUi() {
        val userId = AuthRepositary.userId
        binding.tvLabelCategory.setText(Localizer.labels.category)
        binding.tvLabelDescription.setText(Localizer.labels.description)
        binding.buttonApplyJob.setText(Localizer.labels.apply_for_this_job)
        binding.cbFavorite.isSelected = job.is_favourite
        binding.textToolbarTitle.text = Localizer.labels.job_description
        binding.tvJobName.text = job.name
        binding.tvPostedBy.visibility = View.GONE
        job.job_owner?.firstName.let {firstName->
            var name = firstName
            binding.tvPostedBy.visibility = View.VISIBLE
            binding.tvPostedBy.text=name
        }
        binding.tvValueCategory.text = job.category?.name
        if (job.location != null) {
            binding.tvValueLocation.visibility = View.VISIBLE
            binding.tvValueLocation.text = job.location!!.name
        } else {
            binding.tvValueLocation.visibility = View.GONE
        }
        binding.cbFavorite.setOnClickListener(this)
        if (job.start_date != null && job.end_date != null) {
            binding.tvValuePeriod.text = "${getDate(job.start_date!!)} - ${getDate(job.end_date!!)}"
        }
        binding.tvValueDescription.text = job.description
        binding.buttonApplyJob.setOnClickListener(this)
        binding.buttonChat.visibility = View.GONE
        if (job.updatedAt!=null) {
            if (job.job_owner?._id.equals(userId)) {
                binding.tvPostedOn.setText(Localizer.labels.posted+" " + getDate(job.updatedAt!!))
            } else {
                binding.tvPostedOn.setText(Localizer.labels.posted+" " + getPostedData(job.updatedAt!!))
            }
        }
        if (job.job_application?._id!=null)
        {
            binding.buttonApplyJob.visibility = View.VISIBLE
            binding.buttonApplyJob.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.feijoa_600))
            binding.buttonApplyJob.text = Localizer.labels.job_applied
            binding.buttonApplyJob.isEnabled = false
            binding.buttonApplyJob.setOnClickListener(null)
        }
        if (!TextUtils.isEmpty(job.image)) {
            ImageProcessor.into(binding.ivJobDescriptionBanner).loadResource(
                ServiceUrl.BASE_URL+job.image,R.drawable.ic_camera)

        }
        if(job.job_application!=null)
        {
            binding.buttonChat.visibility = View.VISIBLE
        }else{
            binding.buttonChat.visibility = View.GONE
        }
        binding.buttonChat.setOnClickListener {
            if (messageBundle.chatRoom!=null) {
                activity?.replaceFragment(
                    ScreenName.MESSAGEDETAIL,
                    JsonUtil.toJson(messageBundle, IntentBundle::class.java)
                )
            }else{
                ProgressDialog.show(viewLifecycleOwner,requireContext())
                jobViewModel.createChat(job.job_application!!._id).observe(viewLifecycleOwner
                ) {
                    messageBundle.chatRoom = it
                    activity?.replaceFragment(
                        ScreenName.MESSAGEDETAIL,
                        JsonUtil.toJson(messageBundle, IntentBundle::class.java)
                    )
                }
                jobViewModel.getNetworkData().observe(viewLifecycleOwner,networkObserver)
            }
        }
        binding.buttonPay.visibility = View.GONE
        if (job.payment!=null&&job.payment!!.status!=null)
        {
            val status = Payment.STATUS.getSTATUS(job.payment!!.status!!)
            if (status!=Payment.PAID)
            {
                binding.buttonPay.visibility = View.VISIBLE
                binding.buttonPay.setText(status.getButtonText())
                binding.buttonPay.setOnClickListener {
                    ProgressDialog.show(viewLifecycleOwner,requireContext())
                    job._id?.let { it1 -> jobViewModel.paymentDetail(it1).observe(viewLifecycleOwner) {
                        ProgressDialog.hide()
                        activity?.replaceFragment(ScreenName.PAYMENTDETAIL, it, true)
                    }
                    }
                }
            }else{
                binding.buttonPay.visibility = View.GONE

            }
        }else{
            binding.buttonPay.visibility = View.GONE
        }

        if (job.job_owner?._id.equals(userId))
        {
            binding.imageEditJob.visibility = View.VISIBLE
            binding.imageEditJob.setOnClickListener {

                activity?.replaceFragment(
                    ScreenName.JOBCREATION,
                    JsonUtil.toJson(job, Job::class.java),false
                )
            }
            binding.buttonApplyJob.visibility = View.GONE
        }else{
            binding.imageEditJob.visibility = View.GONE
            binding.buttonApplyJob.visibility = View.VISIBLE

        }

        if (job._id!=null&&job.job_owner_id?.equals(userId)==true){
            binding.txtHoldStatus.visibility = View.VISIBLE
            val isOnHold = job.is_on_hold
            binding.txtHoldStatus.text = if(isOnHold) Localizer.labels.unhold else Localizer.labels.hold
             if(isOnHold)
                binding.textHoldDesc.visibility = View.VISIBLE
            else
                binding.textHoldDesc.visibility = View.GONE
            binding.txtHoldStatus.setOnClickListener {
                        job._id?.let{
                        jobViewModel.holjobApplication(it,!job.is_on_hold).observe(viewLifecycleOwner,
                            Observer {
                                if(it==DataStatus.LOADING)
                                {
                                    ProgressDialog.show(viewLifecycleOwner,requireContext())
                                }
                                else if (it==DataStatus.DATA_AVAILABLE) {
                                    ProgressDialog.hide()
                                    job.is_on_hold = !job.is_on_hold
                                    binding.txtHoldStatus.text = if(job.is_on_hold)Localizer.labels.unhold else Localizer.labels.hold
                                }else{
                                    ProgressDialog.hide()
                                }
                            })
                        }
                    }
            }
        else
        {
            binding.txtHoldStatus.visibility = View.GONE
        }
    }

    fun getDate(dateStr: String):String {
        try {
            /** DEBUG dateStr = '2006-04-16T04:00:00Z' **/
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val mDate = formatter.parse(dateStr) // this never ends while debugging
            return formatter2.format(mDate)
        } catch (e: Exception){
        }
        return dateStr
    }

    override fun onClick(p0: View?) {
        if (p0?.id == binding.cbFavorite.id)
        {
            val selected = binding.cbFavorite.isSelected
            if (!selected) {
                AlertDialog.show(p0.context,Localizer.labels.do_you_want_to_make_this_job_offer_as_favorite,true,this)
            }else{
                AlertDialog.show(p0.context,Localizer.labels.do_you_want_to_remove_this_job_offer_from_favorites,true,this)
            }
        }else if(job._id!=null&&p0?.id == binding.buttonApplyJob.id)
        {

            ProgressDialog.show(viewLifecycleOwner,requireContext())
            jobViewModel.applyJob(jobId = job._id!!).observe(viewLifecycleOwner) {
                messageBundle.chatRoom = it.chatRoom
                messageBundle.job?.job_application = it.jobApplication
                Toast.makeText(
                    requireContext(),
                    Localizer.labels.job_applied_successfully,
                    Toast.LENGTH_SHORT
                ).show()
                binding.buttonApplyJob.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.feijoa_600))
                binding.buttonApplyJob.text = Localizer.labels.job_applied
                binding.buttonApplyJob.isEnabled = false
                binding.buttonApplyJob.setOnClickListener(null)
                binding.buttonChat.visibility = View.VISIBLE
            }
            jobViewModel.getNetworkData().observe(viewLifecycleOwner, networkObserver)
        }
    }

    var networkObserver = Observer<Int> {
        if (it == DataStatus.FAILED)
        {
            ProgressDialog.hide()
            ErrorCode.getMessage(jobViewModel.getLatestError(),requireContext())
        }else if (it == DataStatus.DATA_AVAILABLE)
        {
            ProgressDialog.hide()
        }
    }

    override fun success() {
        if (binding.cbFavorite.isSelected)
        {
            binding.cbFavorite.isSelected = false
            val jobId = job._id
            JobRepositary().removeFavorite(jobId!!)
        }
        else
        {
            binding.cbFavorite.isSelected = true
            val jobId = job._id
            JobRepositary().addAsFavorite(jobId!!)
        }
    }

    override fun cancel() {

    }
}