package com.aquall.app.fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.ClientError
import com.aquall.app.*
import com.aquall.app.databinding.FragmentRegisterBinding
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.RegisterBaseRequest
import com.aquall.app.repositary.model.RegisterCompanyrequest
import com.aquall.app.repositary.model.Registerrequest
import com.aquall.app.utils.*
import com.aquall.app.utils.model.RegistrationType
import java.util.*


class RegisterFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }
    lateinit var binding:FragmentRegisterBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentRegisterBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        binding.businessUtil = BusinessUtil()
        binding.buttonLogin.setOnClickListener{
            activity?.replaceFragment(ScreenName.LOGIN)

        }
        binding.buttonRegisterNow.setOnClickListener { validate() }

        binding.registrationType = RegistrationType.INDIVIDUAL
        binding.radioGroupRegister.setOnCheckedChangeListener{radioGroup,position->
            if(radioGroup.checkedRadioButtonId==R.id.radioCompany)
                binding.registrationType = RegistrationType.COMPANY
            else
                binding.registrationType = RegistrationType.INDIVIDUAL

            binding.executePendingBindings()
        }
        setLayout()
        return binding.root
    }

    fun setLayout(){
        binding.editTextName.setSingleLine()
        binding.ediTextMobileNumber.setSingleLine()
//        binding.editTextPassword.setSingleLine()
        binding.editTextEmail.setSingleLine()
    }

    fun validate():Boolean
    {
        if (binding.registrationType == RegistrationType.COMPANY)
        {
            if (TextUtils.isEmpty(binding.editTextCompanyName.getText()))
            {binding.editTextCompanyName.setError(Localizer.labels.company_name_validation_text)
                return false}
        }

        if (!TextFieldValidator.isValidEmail(binding.editTextEmail.getText()))
        {binding.editTextEmail.setError(Localizer.labels.email_validation)
            return false}

        if (!TextFieldValidator.isValidPassword(binding.editTextPassword.getText()))
        {binding.editTextPassword.setError(Localizer.labels.password_validation)
            return false}


        if (!TextFieldValidator.isValidMobile(binding.ediTextMobileNumber.getText()))
        {
            if (binding.ediTextMobileNumber.text.isNotEmpty()) {
                binding.ediTextMobileNumber.setError(Localizer.labels.phone_number_validation_correct)
            }else{
                binding.ediTextMobileNumber.setError(Localizer.labels.phone_number_validation)
            }
            return false
        }


        if (TextUtils.isEmpty(binding.editTextName.getText()))
        {binding.editTextName.setError(Localizer.labels.name_validation_text)
            return false}

        register()

        return true

    }

    fun register()
    {
        ProgressDialog.show(viewLifecycleOwner,requireContext())
        lateinit var registerrequest:Registerrequest
        registerrequest= Registerrequest()
        registerrequest.email = binding.editTextEmail.getText().toString()
        registerrequest.mobile = binding.ediTextMobileNumber.getText().toString()
        registerrequest.countryCode = binding.countryCodePicker.selectedCountryNameCode
        registerrequest.dialCode = binding.countryCodePicker.selectedCountryCodeWithPlus
        registerrequest.password = binding.editTextPassword.getText().toString()
        registerrequest.password_confirmation = binding.editTextPassword.getText().toString()
        val authRepositary = AuthRepositary()

        if (binding.registrationType ==RegistrationType.INDIVIDUAL) {
            val split = binding.editTextName.getText().toString().split(" ")
            registerrequest.firstName = split.get(0)
            if (split.size>1) {
                registerrequest.lastName = split.get(1)
            }else{
                registerrequest.lastName = ""
            }
        }else{
            registerrequest.firstName = binding.editTextCompanyName.text.toString()
            registerrequest.lastName = binding.editTextName.text.toString()
            registerrequest.isCompany = true
        }
        authRepositary.register(registerrequest).observe(viewLifecycleOwner, androidx.lifecycle.Observer { registerResponse ->
            ProgressDialog.hide()
            if (registerResponse)
            {
                if(binding.registrationType==RegistrationType.COMPANY)
                {
                    val registerrequestCompany= RegisterCompanyrequest()
                    registerrequestCompany.email = binding.editTextEmail.getText().toString()
                    registerrequestCompany.companyName = binding.editTextCompanyName.text.toString()
                    registerrequestCompany.nameOfContacperson = binding.editTextName.text.toString()
                    registerrequestCompany.userId = AuthRepositary.userId
                    authRepositary.registerCompany(registerrequestCompany).observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                        ProgressDialog.hide()
                        if (it)
                        {
                            activity?.login(ScreenName.DASHBOARD)
                        }else{
                            if (authRepositary.latestError.cause is ClientError) {
                                val errorString = authRepositary.getErrorString(requireContext())
                                AlertDialog.show(requireContext(),errorString,false,null,viewLifecycleOwner)
                            }
                        }
                    })

                }else{
                    activity?.login(ScreenName.DASHBOARD)
                }
            }else{
                if (authRepositary.latestError.cause is ClientError) {
                    val errorString = authRepositary.getErrorString(requireContext())
                    AlertDialog.show(requireContext(),errorString,false,null,viewLifecycleOwner)
                }
            }
        })


    }

}