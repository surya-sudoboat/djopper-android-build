package com.aquall.app.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.aquall.app.R
import com.aquall.app.adapters.MessageListAdapter
import com.aquall.app.databinding.FragmentMessageDetailBinding
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.MessageList
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.ImageProcessor
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.viewmodel.ChatViewModel
import com.aquall.app.Localizer

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MessageDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MessageDetailFragment : Fragment() {

    lateinit var binding:FragmentMessageDetailBinding

    var adapter:MessageListAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    var messageBundle:IntentBundle?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMessageDetailBinding.inflate(inflater, container, false)
        
        binding.etMessageNew.setHint(Localizer.labels.type_your_message_here)
        val string = arguments?.getString("data")

        if (string!=null) {
            messageBundle = JsonUtil.fromJson(string!!,IntentBundle::class.java)
            if (messageBundle?.screenType==0)
            {
                binding.layoutJobToolbar.visibility = View.VISIBLE
                binding.layoutCandidateToolBar.visibility = View.GONE
                binding.jobTitleBigJob.text = messageBundle?.job?.name
                binding.tvValueLocationJob.text = messageBundle?.job?.location?.name
                if (messageBundle?.job?.category!=null) {
                    binding.textValueCategoryJob.visibility = View.VISIBLE
                    binding.textValueCategoryJob.text = messageBundle?.job?.category?.name
                }else{
                    binding.textValueCategoryJob.visibility = View.GONE
                }
                binding.imageBackArrowJob.setOnClickListener { activity?.onBackPressed() }

                if (!TextUtils.isEmpty(messageBundle?.job?.image)) {
                    ImageProcessor.into(binding.imageViewSquareProfilePicJob).setRoundedCorners(10f,0f).loadResource(
                        ServiceUrl.BASE_URL+messageBundle?.job?.image,
                        R.drawable.ic_camera)

                }
            }
            else if (messageBundle?.screenType==1)
            {
                binding.layoutJobToolbar.visibility = View.GONE
                binding.layoutCandidateToolBar.visibility = View.VISIBLE
                binding.imageBackArrowCandidate.setOnClickListener { activity?.onBackPressed() }
                binding.jobTitleBigCandidate.text = messageBundle?.candidate?.firstName + " " +messageBundle?.candidate?.lastName
                if (!TextUtils.isEmpty(messageBundle?.candidate?.image)) {
                    ImageProcessor.into(binding.iVProfilePicCandidate).setCircleCrop().loadResource(
                        ServiceUrl.BASE_URL+messageBundle?.candidate?.image,
                        R.drawable.ic_abstract_user)

                }
            }
            val viewModel = ViewModelProvider({viewModelStore},defaultViewModelProviderFactory)
                .get(ChatViewModel::class.java)


            messageBundle?.chatRoom?._id?.let { viewModel.getChat(it).observe(viewLifecycleOwner, Observer {
                setChatRoom(it)

            }) }

            binding.ivSendMessage.isEnabled = false

            binding.etMessageNew.addTextChangedListener(object:TextWatcher{
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun afterTextChanged(p0: Editable?) {
                    binding.ivSendMessage.isEnabled = p0?.length!! >0
                }
            })

            binding.ivSendMessage.setOnClickListener {
                messageBundle?.chatRoom?._id?.let {
                    viewModel.sendChat(it,binding.etMessageNew.text.toString())
                    binding.etMessageNew.setText("")
                }
                }



            SocketClient.getSocketClient().messageLiveData.observe(viewLifecycleOwner,{
                if (it._id.equals(messageBundle?.chatRoom?._id))
                {
                    setChatRoom(it)
                }
            })

        }
        return binding.root
    }

    private fun setChatRoom(
        it: MessageList
    ) {
        var messagelist = it.messages!!
        val linearLayoutManager = LinearLayoutManager(requireContext())
        linearLayoutManager.reverseLayout = true
        binding.rvValueMessages.layoutManager = linearLayoutManager

        if (adapter == null) {
            adapter =
                AuthRepositary.userId?.let { it1 ->
                    var temp = messagelist
                    temp = temp.sortedByDescending { it.timestamp }
                    MessageListAdapter(temp, it1, messageBundle?.screenType!!)
                }
            binding.rvValueMessages.adapter = adapter
        } else {
            messagelist = messagelist.sortedByDescending { it.timestamp }
            adapter?.mMessageList = messagelist
            adapter?.notifyDataSetChanged()
            binding.rvValueMessages.scrollToPosition(0)
        }
    }


}