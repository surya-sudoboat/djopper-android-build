package com.aquall.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.aquall.app.Localizer
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.databinding.FragmentForgotPasswordBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.utils.ProgressDialog
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.TextFieldValidator
import com.aquall.app.utils.enums.DataStatus

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ForgotPasswordFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ForgotPasswordFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
lateinit var binding:FragmentForgotPasswordBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       binding = FragmentForgotPasswordBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        binding.buttonLogin.setOnClickListener {
           activity?.replaceFragment(ScreenName.LOGIN)
        }
        binding.buttonSend.setOnClickListener {
            if(!TextFieldValidator.isValidEmail(binding.editTextEmail.getText())){
                binding.editTextEmail.setError(Localizer.labels.email_validation)
                return@setOnClickListener
            }

            val authRepositary = AuthRepositary()
            authRepositary.forgotPassword(binding.editTextEmail.getText().toString())
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            authRepositary.networkStatus.observe(viewLifecycleOwner, Observer {
                if (it ==DataStatus.DATA_AVAILABLE)
                {
                    ProgressDialog.hide()
                    Toast.makeText(requireContext(),Localizer.labels.reset_password_note,Toast.LENGTH_SHORT).show()
                }else if (it ==DataStatus.FAILED)
                {
                    ProgressDialog.hide()
                    Toast.makeText(requireContext(),Localizer.labels.forgot_password_failed,Toast.LENGTH_SHORT).show()

                }
            })
        }


        return binding.root
    }


}