package com.aquall.app.fragments

import android.content.ClipData.Item
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.repositary.model.Payments
import com.aquall.app.ui.AquallTopBar
import com.aquall.app.ui.OpenSansFont
import com.aquall.app.ui.Themes
import com.aquall.app.ui.openSans
import com.aquall.app.utils.StartEndFilterFragment
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.viewmodel.JobViewModel
import com.google.accompanist.appcompattheme.AppCompatTheme
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PaymentListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PaymentListFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return ComposeView(requireContext()).apply {
            setContent {

                PaymentList()

            }
        }
    }

    @Preview
    @Composable
    private fun PaymentList() {
        val viewModel: JobViewModel = viewModel()
        AppCompatTheme {
            Scaffold(
                topBar = {
                    AquallTopBar(
                        titleText = Localizer.labels.payment_history,
                        menuIcons = {
                            IconButton(
                                {
                                    StartEndFilterFragment { startDate, endDate ->
                                        viewModel.getPaymentHistory(startDate, endDate)
                                    }.show(requireActivity().supportFragmentManager, "")
                                }
                            )
                            {
                                Icon(
                                    painterResource(id = R.drawable.ic_filter_funel),
                                    contentDescription = null
                                )
                            }
                        }
                    ) {
                        requireActivity().onBackPressed()
                    }
                }) { paddingValue ->
                Column(
                    modifier =
                    Modifier
                        .padding(paddingValue)
                        .background(color = Color.LightGray)
                        .fillMaxSize()
                ) {
                    val networkData = viewModel.getNetworkData().observeAsState()
                    viewModel.getPaymentHistory()
                    CheckProgressData(Modifier.align(Alignment.CenterHorizontally),viewModel,networkData)
                }
            }
        }
    }

    @Composable
    fun CheckProgressData(modifier: Modifier= Modifier,viewModel: JobViewModel, networkData:State<Int?>){
        val value = networkData.value
        if (value == DataStatus.LOADING){
            Spacer(modifier = Modifier.height(20.dp))
            CircularProgressIndicator(modifier)
        }else if (value == DataStatus.DATA_AVAILABLE){
            PaymentList(jobList = viewModel.paymentLiveData.value)
        }else{
            Text(text = "Payment List Loading Failed")
        }
    }

    @Composable
    private fun PaymentList(jobList: List<Payments>?) {
        jobList?.let { paymentList ->
            LazyColumn {
                for (payment in paymentList) {
                    item {
                        PaymentItem(payments = payment)
                    }

                }
            }
        }
    }


}

@Preview
@Composable
fun PaymentItem(modifier: Modifier = Modifier, payments: Payments = Payments().getSample()) {
    val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
    val toformat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    Box(
        modifier = modifier
            .fillMaxWidth()
            .padding(10.dp)
    ) {
        Card(
            modifier = Modifier.fillMaxWidth()
        ) {
            Column(modifier = Modifier.fillMaxWidth()) {
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = payments.description,
                    modifier = Modifier.padding(5.dp),
                    fontFamily = openSans,
                    fontSize = 16.sp
                )
                Spacer(modifier = Modifier.height(5.dp))
                Spacer(
                    modifier = Modifier
                        .height(1.dp)
                        .background(Color.LightGray)
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "Amount: ${payments.amount.value} ${payments.amount.currency}",
                    modifier = Modifier.padding(5.dp),
                    fontFamily = openSans,
                    fontSize = 16.sp
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "paid_on: ${toformat.format(fromFormat.parse(payments.payment_created_at)!!)}",
                    modifier = Modifier.padding(5.dp),
                    fontFamily = openSans,
                    fontSize = 16.sp
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "exp_on: ${toformat.format(fromFormat.parse(payments.payment_expired_at?:payments.url_expired_at)!!)}",
                    modifier = Modifier.padding(5.dp),
                    fontFamily = openSans,
                    fontSize = 16.sp
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "Status: ${payments.status}",
                    modifier = Modifier.padding(5.dp),
                    fontFamily = openSans,
                    fontSize = 16.sp
                )

            }

        }
    }
}