package com.aquall.app.fragments

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.adapters.JobListAdapter
import com.aquall.app.databinding.FragmentFavoriteJobListBinding
import com.aquall.app.newScreen
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.CustomItemTouchHelper
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewmodel.JobViewModel


/**
 * A simple [Fragment] subclass.
 * Use the [FavoriteJobListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavoriteJobListFragment : Fragment(),JobListViewHolder.IteractionListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var binding:FragmentFavoriteJobListBinding
    lateinit var adapter: JobListAdapter
    lateinit var viewModel: JobViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteJobListBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        viewModel = ViewModelProvider({viewModelStore},defaultViewModelProviderFactory).get(JobViewModel::class.java)
        showEmptyStatus()
        viewModel.getJobList().observe(viewLifecycleOwner, Observer {
            if (it!=null&&it.jobs.size>0) {
                chatRooms = it.chatRoom
                adapter =  JobListAdapter.setAdapter(binding.rvFavoriteList,it.jobs,this@FavoriteJobListFragment,true)
                binding.textErrorStatus.visibility = View.GONE
                binding.rvFavoriteList.visibility = View.VISIBLE
                binding.pbLoader.visibility =View.GONE
            }else{
                showEmptyStatus()
            }
        })
        viewModel.getFavorite()
//        viewModel.getNetworkData().observe(viewLifecycleOwner, Observer { binding.networkStatus =it })

        binding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }
        if (activity is MainActivity) {
            (activity as MainActivity).setScreenType(NavScreen.Favorite())
        }
        CustomItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            private var mIcon: Drawable? = null
            private var mBackground: ColorDrawable? = null
            init {
                mIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_trash_delete);
                mBackground =  ColorDrawable(Color.RED);
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (binding.rvFavoriteList.adapter!=null&&viewHolder.layoutPosition<binding.rvFavoriteList.adapter!!.itemCount-1) {
                    adapter.notifyItemChanged(viewHolder.layoutPosition)
                    functionAlert(viewHolder.layoutPosition,false)
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {

                if (recyclerView.adapter!=null&&viewHolder.layoutPosition<recyclerView.adapter!!.itemCount-1) {
                    mIcon!!.setTint(Color.WHITE)
                    val itemView = viewHolder.itemView
                    val backgroundCornerOffset =
                        25 //so mBackground is behind the rounded corners of itemView
                    val iconMargin = (itemView.height - mIcon!!.intrinsicHeight) / 2
                    val iconTop = itemView.top + (itemView.height - mIcon!!.intrinsicHeight) / 2
                    val iconBottom = iconTop + mIcon!!.intrinsicHeight
                    if (dX > 0) { // Swiping to the right
                        val iconLeft = itemView.left + iconMargin
                        val iconRight = iconLeft + mIcon!!.intrinsicWidth
                        mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                        mBackground!!.setBounds(
                            itemView.left, itemView.top,
                            itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom
                        )
                    } else if (dX < 0) { // Swiping to the left
                        val iconLeft = itemView.right - iconMargin - mIcon!!.intrinsicWidth
                        val iconRight = itemView.right - iconMargin
                        mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                        mBackground!!.setBounds(
                            itemView.right + dX.toInt() - backgroundCornerOffset,
                            itemView.top, itemView.right, itemView.bottom
                        )
                    } else { // view is unSwiped
                        mIcon!!.setBounds(0, 0, 0, 0)
                        mBackground!!.setBounds(0, 0, 0, 0)
                    }

                    mBackground!!.draw(c)
                    mIcon!!.draw(c)

                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )
                }
            }
        }).attachToRecyclerView(binding.rvFavoriteList)
        return binding.root
    }

    private fun showEmptyStatus() {
        binding.textErrorStatus.visibility = View.VISIBLE
        binding.pbLoader.visibility = View.GONE
        binding.rvFavoriteList.visibility = View.GONE
    }
    fun functionAlert(position: Int,selected:Boolean){
            val listener = object : AlertDialog.Listener {
                override fun success() {
                    onFavoriteChanged(position, selected)
                }

                override fun cancel() {
                }
            }
        if (!selected) {
            com.aquall.app.utils.AlertDialog.show(requireContext(),Localizer.labels.do_you_want_to_make_this_job_offer_as_favorite,true,
                listener
            )
        }else{
            com.aquall.app.utils.AlertDialog.show(requireContext(),Localizer.labels.do_you_want_to_remove_this_job_offer_from_favorites,true,listener)
        }
    }
    override fun onFavoriteChanged(position:Int,value: Boolean) {
        if (!value) {
            val job_id = adapter.jobs.get(position)._id
            val removeFavorites = adapter.removeFavorites(position)
            binding.rvFavoriteList.adapter = removeFavorites
            if (job_id!=null) {
                viewModel.removeFavorite(job_id)
            }
        }else{
            val job_id = adapter.jobs.get(position)._id
            if (job_id!=null) {
                viewModel.markAsFavorite(job_id)
            }
        }

        if (adapter.jobs.size<=0)
            showEmptyStatus()
    }
    var chatRooms: HashMap<String,List<ChatRoom>>? = null
    override fun onItemClicked(position: Int) {
        val intentBundle = IntentBundle()
        val get = adapter.jobs.get(position)
        get.is_favourite =true
        intentBundle.job = get
        if (chatRooms!=null&&get._id!=null&&chatRooms?.get(get._id)!=null&& chatRooms?.size!! >0) {
            intentBundle.chatRoom = chatRooms?.get(get._id)!!.get(0)
        }
        activity?.newScreen(ScreenName.JOBDESCRIPTION,intentBundle.toString())
    }


    override fun isFavoriteList(): Boolean {
       return true
    }

    override fun showFavoriteAndKnowMore(): Boolean {
        return true
    }

    override fun deleteAlertText(): String {
        return ""
    }

    override fun onDeleteClicked(position: Int) {}
    override fun onEditClicked(position: Int) {

    }


    override fun getMessagesCount(position: Int): Int {
        return 0
    }


}