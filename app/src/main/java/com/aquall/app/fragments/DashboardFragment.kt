package com.aquall.app.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aquall.app.Localizer
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.adapters.JobListAdapter
import com.aquall.app.databinding.FragmentDashboardBinding
import com.aquall.app.newScreen
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.LabelRepositary
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.utils.AdSense
import com.aquall.app.utils.LanguagePopup
import com.aquall.app.utils.ProgressDialog
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.SearchKeys
import com.aquall.app.viewholders.ClickListener
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewmodel.CategoriesViewModel
import com.aquall.app.viewmodel.JobViewModel

class DashboardFragment : Fragment(), Observer<List<Category>>,
    JobListViewHolder.IteractionListener {
    lateinit var binding: FragmentDashboardBinding
    lateinit var adapter: JobListAdapter
    lateinit var viewModelJob: JobViewModel
    var categorryList: List<Category>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        binding.label = Localizer.labels
        AdSense.init(requireContext(), viewLifecycleOwner)
        val viewmodel = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory)
            .get(CategoriesViewModel::class.java)
        viewModelJob = ViewModelProvider({ viewModelStore }, defaultViewModelProviderFactory)
            .get(JobViewModel::class.java)
        val categories = viewmodel.getCategories()
        if (categories.value != null) {
            onChanged(categories.value)
        }
        categories.observe(viewLifecycleOwner, this)
        viewModelJob.getJobList().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                chatRooms = it.chatRoom
                adapter = JobListAdapter.setAdapter(
                    binding.recycleJobsAndAds, it.jobs,
                    this@DashboardFragment, true
                )
            }
        })
        viewModelJob.search(SearchKeys())
        viewModelJob.getNetworkData().observe(viewLifecycleOwner, Observer {
            binding.networkStatus = it
        })
        if (activity is MainActivity) {
            (activity as MainActivity).setScreenType(NavScreen.Dashboard())
        }
        binding.buttonPostJob.setOnClickListener {
            activity?.replaceFragment(ScreenName.JOBCREATION, "")
        }
        SocketClient.getSocketClient().connect()
        binding.buttonLanguageChange.setText(Localizer.getLabel())
        binding.buttonLanguageChange.setOnClickListener {
            LanguagePopup(requireContext(), object : ClickListener {
                override fun onClick(language: Localizer.Locale) {
                    language.code?.let { it1 ->
                        val labelRepositary = LabelRepositary()
                        labelRepositary.getLabels(language = it1, false)
                        ProgressDialog.show(viewLifecycleOwner, requireContext())
                        labelRepositary.networkStatus.observe(viewLifecycleOwner) {
                            if (it == DataStatus.DATA_AVAILABLE) {
                                ProgressDialog.hide()
                                val intent = activity?.intent
                                intent?.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                activity?.startActivity(intent)
                                activity?.finish()
                            } else if (it == DataStatus.FAILED) {
                                ProgressDialog.hide()
                            }
                        }
                    }
                }
            }).show()
        }
        return binding.root
    }

    var chatRooms: HashMap<String, List<ChatRoom>>? = null
    override fun onFavoriteChanged(position: Int, value: Boolean) {
        if (!value) {
            val job_id = adapter.jobs.get(position)._id
            if (job_id != null) {
                viewModelJob.removeFavorite(job_id)
            }
        } else {
            val job_id = adapter.jobs.get(position)._id
            if (job_id != null) {
                viewModelJob.markAsFavorite(job_id)
            }
        }
    }
    override fun onItemClicked(position: Int) {
        val intentBundle = IntentBundle()
        val get = adapter.jobs.get(position)
        intentBundle.job = get
        if (chatRooms != null && get._id != null && chatRooms?.get(get._id) != null && chatRooms?.size!! > 0) {
            intentBundle.chatRoom = chatRooms?.get(get._id)!!.get(0)
        }
        activity?.newScreen(ScreenName.JOBDESCRIPTION, intentBundle.toString())
    }

    override fun isFavoriteList(): Boolean {
        return false
    }

    override fun showFavoriteAndKnowMore(): Boolean {
        return true
    }

    override fun deleteAlertText(): String {
        return ""
    }

    override fun onDeleteClicked(position: Int) {

    }

    override fun onEditClicked(position: Int) {

    }

    override fun getMessagesCount(position: Int): Int {
        return 0
    }

    override fun onChanged(t: List<Category>?) {
        if (t != null) {

            binding.recyclerCategory.setContent {
                CategoryList(t = t) {
                    val searchKeys = SearchKeys()
                    searchKeys.category = arrayListOf<Category>(it)
                    activity?.replaceFragment(
                        ScreenName.SUBCATEGORYFRAGMENTLIST,
                        searchKeys.toString()
                    )
                }
            }
        }
    }


    @Composable
    fun CategoryList(t: List<Category>, onItemClick: (category: Category) -> Unit = {}) {
        val color = colorResource(id = com.aquall.app.R.color.colorPrimary)
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(color)
        ) {
            for (i in 0 until t.size step (2)) {
                Row(Modifier.fillMaxWidth()) {
                    category(category = t.get(i), Modifier.weight(.5f), onItemClick)
                    if (i + 1 < t.size) {
                        category(category = t.get(i + 1), Modifier.weight(.5f), onItemClick)
                    }
                }
            }
        }
    }


    @Composable
    fun category(
        category: Category,
        modifier: Modifier = Modifier,
        onItemClick: (category: Category) -> Unit = {}
    ) {

        val verticalGradientBrush = Brush.verticalGradient(
            colors = listOf(
                Color(0xFFD91830),
                Color(0xFFFF6679)
            )
        )
        Box(modifier.padding(10.dp)) {
            Card(
                Modifier
                    .fillMaxWidth()
            ) {
                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(0.dp, 10.dp)
                        .align(Alignment.Center)
                ) {
                    Text(
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        text = category.name
                    )
                    Text(
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        text = "${category.sub_category_count} sub categories"
                    )
                    Spacer(modifier = Modifier.height(2.dp))
                    Button(modifier = Modifier.align(Alignment.CenterHorizontally),
                        contentPadding = PaddingValues(0.dp),
                        shape = RoundedCornerShape(8.dp),
                        onClick = { onItemClick(category)}) {
                        Text(
                            text = Localizer.labels.view_vacancies,
                            modifier = Modifier
                                .height(ButtonDefaults.MinHeight)
                                .align(Alignment.CenterVertically)
                                .background(brush = verticalGradientBrush)
                                .padding(8.dp)
                        )
                    }
                }
            }
        }
    }

}