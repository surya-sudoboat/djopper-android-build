package com.aquall.app.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.aquall.app.R
import android.webkit.JavascriptInterface
import com.aquall.app.utils.Logger
import android.graphics.Bitmap
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.aquall.app.repositary.service.ServiceUrl

import android.graphics.BitmapFactory





// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PaymentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PaymentFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
lateinit var webView: WebView
lateinit var progressBar: ProgressBar
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_payment, container, false)

        webView = inflate.findViewById(R.id.webView);
        progressBar = inflate.findViewById(R.id.progressBar);

        webView.webViewClient  = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                val bm = BitmapFactory.decodeResource(resources, R.drawable.ic_splash_logo)
                super.onPageStarted(view, url, favicon?:bm)
                progressBar.visibility = View.VISIBLE
            }
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = View.GONE
                if (url!=null)
                {
                    Logger.e("url:  ${url}")
                    if (url.contains(ServiceUrl.BASE_URL))
                    {
                        activity?.onBackPressed()
                    }
                }
            }
        }
        webView.settings.javaScriptEnabled =true
        val string = arguments?.getString("data", "")
        if (string!=null) {
            webView.loadUrl(string)

        }
        return inflate
    }







}