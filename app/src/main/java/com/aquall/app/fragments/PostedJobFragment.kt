package com.aquall.app.fragments

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.adapters.JobListAdapter
import com.aquall.app.databinding.FragmentPostedJobBinding
import com.aquall.app.replaceFragment
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.CustomItemTouchHelper
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.ProgressDialog
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.enums.NavScreen
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.aquall.app.viewholders.JobListViewHolder
import com.aquall.app.viewholders.JobListViewHolderBig
import com.aquall.app.viewmodel.JobViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PostedJobFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PostedJobFragment : Fragment(),JobListViewHolder.IteractionListener,SearchView.OnQueryTextListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    lateinit var  binding: FragmentPostedJobBinding
     var adapter: JobListAdapter? = null
    lateinit var viewModel: JobViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentPostedJobBinding.inflate(inflater, container, false)
        binding.textToolbarTitle.setText(Localizer.labels.posted_jobs)
        binding.buttonSearchAJob.setText("+ "+Localizer.labels.post_a_job)
        binding.svJobs.queryHint= Localizer.labels.search_posted_jobs
        viewModel = ViewModelProvider({viewModelStore},defaultViewModelProviderFactory)
            .get(JobViewModel::class.java)
        viewModel.getJobList().observe(viewLifecycleOwner, Observer {
            if (it!=null) {
                postedChatRooms = it.chatRoom
                adapter =  JobListAdapter.setAdapter(binding.rvAppliedJobs,it.jobs,
                    this@PostedJobFragment)
            }
        })
        viewModel.getPostedJobs()
        viewModel.getNetworkData().observe(viewLifecycleOwner,
            Observer { binding.networkStatus =it })
        binding.imageBackArrow.setOnClickListener { activity?.onBackPressed() }
        if (activity is MainActivity) {
            (activity as MainActivity).setScreenType(NavScreen.Favorite())
        }

        binding.buttonSearchAJob.setOnClickListener {
            activity?.replaceFragment(ScreenName.JOBCREATION,"",false)
        }

        binding.svJobs.setOnQueryTextListener(this)
        CustomItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            private var mIcon: Drawable? = null
            private var mBackground: ColorDrawable? = null
            init {
                mIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_trash_delete);
                mBackground =  ColorDrawable(Color.RED);
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val layoutPosition = viewHolder.layoutPosition
                adapter?.notifyItemChanged(layoutPosition)
                AlertDialog.show(requireContext(),deleteAlertText(),true,
                    object : AlertDialog.Listener {
                        override fun success() {
                            onDeleteClicked(layoutPosition)

                        }

                        override fun cancel() {

                        }

                    })
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                mIcon!!.setTint(Color.WHITE)
                val itemView = viewHolder.itemView
                val backgroundCornerOffset =
                    25 //so mBackground is behind the rounded corners of itemView
                val iconMargin = (itemView.height - mIcon!!.intrinsicHeight) / 2
                val iconTop = itemView.top + (itemView.height - mIcon!!.intrinsicHeight) / 2
                val iconBottom = iconTop + mIcon!!.intrinsicHeight
                if (dX > 0) { // Swiping to the right
                    val iconLeft = itemView.left + iconMargin
                    val iconRight = iconLeft + mIcon!!.intrinsicWidth
                    mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                    mBackground!!.setBounds(
                        itemView.left, itemView.top,
                        itemView.left + dX.toInt() + backgroundCornerOffset, itemView.bottom
                    )
                } else if (dX < 0) { // Swiping to the left
                    val iconLeft = itemView.right - iconMargin - mIcon!!.intrinsicWidth
                    val iconRight = itemView.right - iconMargin
                    mIcon!!.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                    mBackground!!.setBounds(
                        itemView.right + dX.toInt() - backgroundCornerOffset,
                        itemView.top, itemView.right, itemView.bottom
                    )
                } else { // view is unSwiped
                    mIcon!!.setBounds(0, 0, 0, 0)
                    mBackground!!.setBounds(0, 0, 0, 0)
                }

                mBackground!!.draw(c)
                mIcon!!.draw(c)

                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }).attachToRecyclerView(binding.rvAppliedJobs)

        return binding.root
    }

    var postedChatRooms:HashMap<String,List<ChatRoom>>? = null
    override fun onFavoriteChanged(position: Int, value: Boolean) {

    }


    override fun onItemClicked(position: Int) {
        if (adapter!=null) {
            val get = adapter!!.tempJobs.get(position)
            val messageBundle = IntentBundle()
            messageBundle.screenType =1
            messageBundle.job = get
            messageBundle.isCandidateClickable  = false
            if (postedChatRooms!=null&& postedChatRooms?.get(get?._id)!=null&&postedChatRooms!!.size>0&&postedChatRooms!!.get(get?._id!!)!!.size>0) {
                messageBundle.chatRoom = postedChatRooms?.get(get?._id)!!.get(0)
            }

            activity?.replaceFragment(ScreenName.JOBDESCRIPTION,
                JsonUtil.toJson(messageBundle, IntentBundle::class.java))
        }
    }

    override fun isFavoriteList(): Boolean {
        return false
    }

    override fun showFavoriteAndKnowMore(): Boolean {
        return false
    }

    override fun getMessagesCount(position: Int): Int {
        return 0
    }

     override fun deleteAlertText(): String {
        return Localizer.labels.are_you_sure_you_want_to_delete_the_posted_job
    }

     fun getIsPostedJob(): Boolean {
        return true
    }

     override fun onDeleteClicked(position: Int) {
        if (adapter!=null) {
            val job_id = adapter!!.jobs.get(position)._id
            val removeFavorites = adapter!!.removeFavorites(position)
            binding.rvAppliedJobs.adapter = removeFavorites
            ProgressDialog.show(viewLifecycleOwner,requireContext())
            viewModel.deleteJob(job_id!!).observe(viewLifecycleOwner) {
                if (it==DataStatus.DATA_AVAILABLE)
                {
                    ProgressDialog.hide()

                    Toast.makeText(requireContext(),Localizer.labels.deleted_job_text,Toast.LENGTH_SHORT).show()
                }else if (it==DataStatus.FAILED)
                {
                    ProgressDialog.hide()
                    Toast.makeText(requireContext(),Localizer.labels.job_deletion_failed,Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

     fun onCandidateClicked(position: Int) {
        if (adapter!=null) {
            val get = adapter!!.tempJobs.get(position)
            val messageBundle = IntentBundle()
            messageBundle.screenType =1
            messageBundle.job = get
            messageBundle.isCandidateClickable  = false
            if (postedChatRooms!=null&& postedChatRooms?.get(get?._id)!=null&&postedChatRooms!!.size>0&&postedChatRooms!!.get(get?._id!!)!!.size>0) {
                messageBundle.chatRoom = postedChatRooms?.get(get?._id)!!.get(0)
            }

            activity?.replaceFragment(ScreenName.CANDIDATELIST,
                JsonUtil.toJson(messageBundle, IntentBundle::class.java))
        }
    }

     override fun onEditClicked(position: Int) {
        if (adapter!=null) {
            val get = adapter!!.tempJobs.get(position)
            activity?.replaceFragment(ScreenName.JOBCREATION,
                JsonUtil.toJson(get, Job::class.java))
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (adapter!=null) {
            val filter = adapter!!.filter(newText)
            binding.rvAppliedJobs.adapter = filter
            if (adapter!!.tempJobs.size<=0)
            {
                binding.textNoData.visibility = View.VISIBLE
            }else{
                binding.textNoData.visibility = View.GONE

            }
        }
        return true
    }


}