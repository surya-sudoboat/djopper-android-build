package com.aquall.app.repositary.model

import com.aquall.app.utils.model.Location
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class LocationDB() :RealmModel{
     var type:String?=null
     var name:String?=null
     var coordinates: RealmList<Double>? = null

    constructor(location: Location) : this() {
        val realmList = RealmList<Double>()
        location.coordinates?.let { realmList.addAll(it) }
        this.coordinates = realmList
        this.type = location.type
        this.name = location.name
    }
}






