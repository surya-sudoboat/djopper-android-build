package com.aquall.app.repositary.service

import android.R.attr
import com.android.volley.NetworkResponse
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.RequestFuture
import com.aquall.app.repositary.model.JobDescriptionResponse
import android.R.attr.bitmap
import android.content.Context
import com.aquall.app.repositary.service.VolleyMultiPartRequest.DataPart
import android.graphics.Bitmap
import android.net.Uri
import java.io.ByteArrayOutputStream
fun getData(fileName: String,uri:ByteArray,key:String):HashMap<String,DataPart>{

    val hashMap = HashMap<String, DataPart>()
    hashMap.put(key,DataPart(fileName,uri))
    return hashMap
}

class MultiPartRequest(url:String,fileName:String,key:String,uri:ByteArray,future: RequestFuture<String>):VolleyMultiPartRequest(Request.Method.POST,ServiceUrl.BASE_URL+url,
    getData(fileName,uri,key),future,future) {



}