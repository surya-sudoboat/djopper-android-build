package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Category
import io.realm.RealmModel
import io.realm.annotations.RealmClass

@RealmClass
open class CategoryDB():RealmModel {
    lateinit var _id:String
    lateinit var name:String
    var sub_category_count:Int=0

    constructor(category:Category):this(){
        this._id = category._id
        this.name = category.name
        this.sub_category_count = category.sub_category_count
    }

    override fun toString(): String {
        return JsonUtil.toJson(this,CategoryDB::class.java)
    }
}