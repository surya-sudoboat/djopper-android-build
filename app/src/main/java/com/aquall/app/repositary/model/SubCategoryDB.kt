package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.SubCategory
import com.google.gson.annotations.SerializedName
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class SubCategoryDB():RealmModel {
    lateinit var _id:String
    @SerializedName("value",alternate = ["name"])
    lateinit var value:String

    constructor(subCategory: SubCategory):this(){
        this._id = subCategory._id
        this.value = subCategory.name?:""
    }

    override fun toString(): String {
        return JsonUtil.toJson(this,SubCategoryDB::class.java)
    }
}