package com.aquall.app.repositary

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.android.volley.ClientError
import com.aquall.app.utils.enums.DataStatus
import io.realm.Realm
import io.realm.RealmObject
import org.json.JSONObject

open class BaseRepositary {
    val networkStatus = MutableLiveData<Int>()
    lateinit var latestError:Exception
fun getErrorString(context: Context):String{
    try {
        val localizedMessage =
            String((latestError.cause as ClientError).networkResponse.data)
        val jsonObject = JSONObject(localizedMessage)
        val errorJson = jsonObject.getJSONObject("error")
        val string = errorJson.getString("message")
        return string
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return "Network error"
}
}