package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.Location
import com.aquall.app.utils.model.Profile
import com.aquall.app.utils.model.SubCategory
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class LoginDB():RealmModel{
     @PrimaryKey
     var _id:String? = null
     var firstName:String? = null
     var lastName:String? = null
     var email:String? = null
     var mobile:String? = null
     var token:String? = null
     var DOB:String? = null
     var location: LocationDB? = null
     var gender: String? = null
     var password:String? = null
    var countryCode:String?=null
     var image:String? =null
     var unread_message_count:Int = 0
     var unread_fav_job_count:Int = 0
    var dialCode:String? = null
     var locale:String? = null
     var profession:String? = null
    var is_notification_enabled:Boolean = true
    var is_notification_sound_enabled:Boolean = true
    var preferred_category:RealmList<CategoryDB>?=null
    var preferred_subcategory:RealmList<SubCategoryDB>?=null
    var range:String? = null
    var appliedJobsCount:Int=0
    var postedJobsCount:Int=0
    var favouriteCount:Int=0
    var isCompany:Boolean = false
    var companyProfile:CompanyProfile?=null


    fun setProfile(profile: Profile):LoginDB
    {
        this._id=profile._id
        this.firstName=profile.firstName
        this.lastName=profile.lastName
        this.DOB = profile.DOB
        this.email = profile.email
        this.mobile = profile.mobile
        this.range = profile.range
        this.countryCode = profile.countryCode
        this.location= profile.location?.let { LocationDB(it) }
        this.gender = profile.gender
//        this.password = profile.password
        this.image = profile.image
        this.dialCode = profile.dialCode
        this.is_notification_enabled = profile.is_notification_enabled
        this.is_notification_sound_enabled = profile.is_notification_sound_enabled
        val categoryRealmList = RealmList<CategoryDB>()
        if (profile.preferred_category!=null) {
            categoryRealmList.addAll(profile.preferred_category!!.map { CategoryDB(it) }!!)
        }
        this.preferred_category = categoryRealmList
        val subCategoryRealmList = RealmList<SubCategoryDB>()
        if (profile.preferred_subcategory!=null) {
            subCategoryRealmList.addAll(profile.preferred_subcategory!!.map { SubCategoryDB(it)!! })
        }
        this.preferred_subcategory = subCategoryRealmList
        this.profession = profile.profession
        this.is_notification_enabled = profile.is_notification_enabled
        this.is_notification_sound_enabled = profile.is_notification_sound_enabled
        this.appliedJobsCount = profile.appliedJobsCount
        this.postedJobsCount = profile.postedJobsCount
        this.favouriteCount = profile.favouriteCount
        this.isCompany = profile.isCompany
        this.companyProfile = profile.companyProfile
        return this
    }


    companion object {
        fun fromString(value: String): LoginDB {
            return JsonUtil.fromJson(value,LoginDB::class.java)
        }
    }
}