package com.aquall.app.repositary

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration


public class AppDatabase  {



    companion object {
        fun getDB(): Realm {
            return Realm.getDefaultInstance()
        }

       fun init (context:Context){
           Realm.init(context)
            val config = RealmConfiguration.Builder().name("Aquall").deleteRealmIfMigrationNeeded().build()
            Realm.setDefaultConfiguration(config)

        }
    }


}