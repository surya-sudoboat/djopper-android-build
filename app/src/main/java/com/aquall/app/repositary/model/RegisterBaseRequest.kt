package com.aquall.app.repositary.model

open class RegisterBaseRequest {
    var email:String?=null
    var mobile:String?=null
    var password:String?=null
    var password_confirmation:String?=null
    var countryCode:String?= null
    var dialCode:String?= null
}