package com.aquall.app.repositary.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class CompanyProfile(
    @SerializedName("companyName"        ) var companyName        : String? = null,
    @SerializedName("nameOfContacperson" ) var nameOfContacperson : String? = null,
    @SerializedName("streetName"         ) var streetName         : String? = null,
    @SerializedName("houseNumber"        ) var houseNumber        : Int?    = null,
    @SerializedName("postalCode"         ) var postalCode         : String? = null,
    @SerializedName("City"               ) var City               : String? = null,
    @SerializedName("country"            ) var country            : String? = null,
    @SerializedName("kvkNumber"          ) var kvkNumber          : Int?    = null,
    @SerializedName("iban"               ) var iban               : Int?    = null,
    @SerializedName("vatNumber"          ) var vatNumber          : Int?    = null,
    @SerializedName("email"              ) var email              : String? = null,
    @SerializedName("website"            ) var website            : String? = null,
    @SerializedName("logo"               ) var logo               : String? = null,
    @PrimaryKey
    @SerializedName("userId"             ) var userId             : String? = null

) :RealmModel{


}