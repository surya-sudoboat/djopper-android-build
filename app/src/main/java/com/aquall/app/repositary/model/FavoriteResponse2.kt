package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Favorite
import com.aquall.app.utils.model.Favorite2
import com.google.gson.annotations.SerializedName

class FavoriteResponse2 {
    @SerializedName("data", alternate = ["jobs"])
    lateinit var jobs:List<Favorite2>
    var unread_fav_job_count:Int = 0

    companion object{
        fun fromString(value:String):FavoriteResponse2
        {
            return JsonUtil.fromJson(value,FavoriteResponse2::class.java)
        }
    }
}