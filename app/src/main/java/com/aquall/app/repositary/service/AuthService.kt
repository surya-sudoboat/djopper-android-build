package com.aquall.app.repositary.service

import com.android.volley.toolbox.RequestFuture
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.*
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.Settings
import com.aquall.app.utils.SharedPrefUtil
import com.example.networkmanager.NetworkCallback
import com.example.networkmanager.ServiceManager
import com.google.android.gms.common.util.SharedPreferencesUtils
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class AuthService:BaseService<String>(
ServiceUrl.BASE_URL), NetworkCallback<String> {

    fun login(email:String,password:String): LoginDB
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            val json = JSONObject()
            json.put("email",email)
            json.put("password",password)
            val get = post("/api/auth/login",json.toString(), newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            val fromString = LoginResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }

    fun getProfile(): LoginDB
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = get("/api/users/get", newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            val fromString = LoginResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }

    fun getCompanyProfile(): CompanyProfile
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = get("/api/company/get_user", newFuture)
            ServiceManager.processor.process(get)
            var get1 = newFuture.get(30,TimeUnit.SECONDS)
            get1 = JSONArray(get1).getJSONObject(0).toString()
            val fromString = Gson().fromJson<CompanyProfile>(get1,CompanyProfile::class.java)
            return fromString
        } catch (e: Exception) {
            throw e
        }
    }
    fun getCount(): String
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = get("/api/job_count", newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)

            return get1
        } catch (e: Exception) {
            throw e
        }
    }

    fun settings():Settings.SettingsData{
        try {
            val newFuture = RequestFuture.newFuture<String>()
            val get = get("/api/settings",newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            val fromString = JsonUtil.fromJson(get1,Settings.SettingsData::class.java)
            return fromString
        } catch (e: Exception) {
            throw e
        }
    }

    fun register(registerrequest: Registerrequest): LoginDB
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            val get = post("/api/auth/signup",registerrequest.toString(), newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            val fromString = LoginResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }


    fun registerCompany(registerrequest: RegisterCompanyrequest): LoginDB
    {
        try {
            val newFuture = RequestFuture.newFuture<String>()
            val get = post("/api/company/signup",registerrequest.toString(), newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            val fromString = LoginResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }

    fun updateProfile(profilestring:String)
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = put("/api/users/update",profilestring, newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }

    fun updateCompanyProfile(profilestring:String)
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = put("/api/company/update_company",profilestring, newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }
    fun logout()
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val get = get("/api/auth/logout", newFuture)
            ServiceManager.processor.process(get)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }
    fun updateFCMToken(fcmToken:String)
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val jsonObject = JSONObject()
            jsonObject.put("token",fcmToken)
            val get = post("/api/fcm_token",jsonObject.toString(), newFuture)
            ServiceManager.processor.process(get)
            newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }

    fun changePassword(oldPassword:String,newPassword:String)
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            mToken  = AuthRepositary.token
            val jsonObject = JSONObject()
            jsonObject.put("old_password",oldPassword)
            jsonObject.put("password",newPassword)
            jsonObject.put("password_confirmation",newPassword)
            val get = put("/api/users/change-password",jsonObject.toString(), newFuture)
            ServiceManager.processor.process(get)
            newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }

    fun updateProfilePicture(fileName:String,file:ByteArray):String
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            val multiPartRequest = MultiPartRequest("/api/users/profile-picture",fileName, "file", file, newFuture)
            ServiceManager.processor.process(multiPartRequest)
            val get1 = newFuture.get(120,TimeUnit.SECONDS)
            return get1
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }
    fun forgotPassword(email:String)
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            val jsonObject = JSONObject()
            jsonObject.put("email",email)
            val get = post("/api/users/send-password-reset-link",jsonObject.toString(), newFuture)
            ServiceManager.processor.process(get)
            newFuture.get(30,TimeUnit.SECONDS)
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }

    override fun onSuccess(response: String) {

    }

    override fun onFailure(error: Throwable) {

    }
}