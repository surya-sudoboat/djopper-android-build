package com.aquall.app.repositary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aquall.app.repositary.dao.CategoryDAO
import com.aquall.app.repositary.dao.LabelsDAO
import com.aquall.app.repositary.dao.LoginDAO
import com.aquall.app.repositary.model.LoginDB
import com.aquall.app.repositary.model.RegisterCompanyrequest
import com.aquall.app.repositary.model.Registerrequest
import com.aquall.app.repositary.model.UpdateProfileRequest
import com.aquall.app.repositary.service.AuthService
import com.aquall.app.repositary.service.CategoryService
import com.aquall.app.utils.MessageException
import com.aquall.app.utils.Settings
import com.aquall.app.utils.SharedPrefUtil
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Profile
import com.example.networkmanager.NetworkCallback
import com.google.android.libraries.places.api.Places
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.util.concurrent.Flow
import java.util.logging.Logger

class AuthRepositary :BaseRepositary(){

     fun login(email:String,password:String):LiveData<Boolean>
    {
        val networkResponse = MutableLiveData<Boolean>()
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val loginDB = AuthService().login(email,password)
                token = loginDB.token
                userId = loginDB._id
                userData = loginDB
                CategoryRepositary().getCategoryNetwork()
                loginDB.password = password
                LoginDAO().save(loginDB)

                networkResponse.postValue(true)
                val firebaseToken = SharedPrefUtil.getInstance().firebaseToken
                if (firebaseToken!=null) {
                    AuthService().updateFCMToken(firebaseToken)
                }
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
                val unreadMessageCount = loginDB.unread_message_count
                val unread_fav_job_count = loginDB.unread_fav_job_count
                SocketClient.getSocketClient().unReadLiveData.postValue(unreadMessageCount)
                SocketClient.getSocketClient().unReadFavLiveData.postValue(unread_fav_job_count)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkResponse.postValue(false)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }
        }

        return networkResponse
    }

    fun updateFCMToken(token:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                if (token!=null) {
                    AuthService().updateFCMToken(token)
                }
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }
        }
    }

    fun register(registerrequest: Registerrequest):LiveData<Boolean>
    {
        val networkResponse = MutableLiveData<Boolean>()
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val register = AuthService().register(registerrequest)
                CategoryRepositary().getCategoryNetwork()
                register.password = registerrequest.password
                LoginDAO().save(register)
                token = register.token
                userId = register._id
                userData =register
                networkResponse.postValue(true)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkResponse.postValue(false)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }
        }

        return networkResponse
    }


    fun registerCompany(registerrequest: RegisterCompanyrequest):LiveData<Boolean>
    {
        val networkResponse = MutableLiveData<Boolean>()
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val register = AuthService().registerCompany(registerrequest)
                networkResponse.postValue(true)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkResponse.postValue(false)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }
        }

        return networkResponse
    }

    //get the livedata from viewmodel and post values to it from background job
    lateinit var mProfileLiveData: MutableLiveData<Profile>
    fun getProfile(liveData: MutableLiveData<Profile>){
        mProfileLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            if (userData!=null){
                mProfileLiveData.postValue(Profile(userData!!))
            }else{
            val first = LoginDAO().getByPrimaryKey(userId!!)
            if (first!=null) {
                userData = first
                mProfileLiveData.postValue(Profile(first))
            }
            }

            val profile = AuthService().getProfile()
            if (profile.isCompany){
                try {
                    val companyProfile = AuthService().getCompanyProfile()
                    profile.companyProfile = companyProfile
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            val countResponse = AuthService().getCount()
            profile.password = userData?.password
            if (profile!=null)
            {
                if (countResponse!=null)
                {
                    val jsonObject = JSONObject(countResponse)
                    profile.appliedJobsCount =
                        if (jsonObject.has("appliedJobs"))
                            jsonObject.getInt("appliedJobs")
                        else
                            0
                    profile.postedJobsCount =
                        if (jsonObject.has("postedJobs"))
                            jsonObject.getInt("postedJobs")
                        else
                            0
                    profile.favouriteCount =
                        if (jsonObject.has("favouriteJobs"))
                            jsonObject.getInt("favouriteJobs")
                        else
                            0
                    SocketClient.getSocketClient().unReadFavLiveData.postValue(profile.favouriteCount)
                }
                userData = profile
                mProfileLiveData.postValue(Profile(profile))
                LoginDAO().save(profile)
            }
        }
    }



    fun isLoggedIn():Boolean{
        val count = LoginDAO().getCount()
        if (count>0)
        {
            CoroutineScope(Dispatchers.IO).launch {
                val first = LoginDAO().getFirst()
                token = first?.token
                userId = first?._id
                userData = first
            }
            return true
        }
        return false
    }


    fun updateProfile(profile:Profile)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                AuthService().updateProfile(UpdateProfileRequest(profile).toString())
                if (profile.isCompany&&profile.companyProfile!=null)
                    AuthService().updateCompanyProfile(Gson().toJson(profile.companyProfile))
                val first = LoginDAO().getFirst()
                val profile1 = first?.setProfile(profile)
                userData = profile1
                LoginDAO().save(profile1!!)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }

        }
    }
    fun changePassword(oldPassword:String,newPassword:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                AuthService().changePassword(oldPassword,newPassword)
                val first = LoginDAO().getFirst()
                if (first!=null) {
                    first.password = newPassword
                    LoginDAO().save(first)
                }
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }

        }
    }

    fun logout(isSoftLogout:Boolean)
    {
        CoroutineScope(Dispatchers.IO).launch{
            networkStatus.postValue(DataStatus.LOADING)
            LoginDAO().clearAll()
            AppDatabase.getDB().executeTransaction(Realm.Transaction {
                it.deleteAll()
            })
            if (!isSoftLogout)
            {
                AuthService().logout()
            }
            val firebaseToken = SharedPrefUtil.getInstance().firebaseToken
            SharedPrefUtil.getInstance().deleteAll()
            SharedPrefUtil.getInstance().firebaseToken = firebaseToken
            delay(1000)
            networkStatus.postValue(DataStatus.DATA_AVAILABLE)
        }
    }

    fun updateProfilePicture(fileName:String,file:ByteArray)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val updateProfilePicture = AuthService().updateProfilePicture(fileName, file)
                com.aquall.app.utils.Logger.e(updateProfilePicture)
                val jsonObject = JSONObject(updateProfilePicture)
                val data = jsonObject.getJSONObject("data")
                val string = data.getString("url")
                val first = LoginDAO().getFirst()
                first?.image = string
                LoginDAO().save(first!!)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }

        }
    }

    fun forgotPassword(email:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                AuthService().forgotPassword(email)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }

        }
    }
    fun getSettings()
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val settings = AuthService().settings()
                Settings.setSettings(settings)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError =e
            }

        }
    }

    companion object{
         var token:String? = null
         var userId:String? = null
         var userData:LoginDB? = null

    }
}