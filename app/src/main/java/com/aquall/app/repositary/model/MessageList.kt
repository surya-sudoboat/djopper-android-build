package com.aquall.app.repositary.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

open class MessageList() {
    var _id:String? = null
    var messages:List<Message>? = null
    var unread_message_count:Int=0

    constructor(id:String,messages:List<Message>):this()
    {
        this._id = id
        this.messages = messages
    }

    constructor(messageList: MessageListDB):this()
    {
        this._id = messageList._id
        var temp = ArrayList<Message>()
        if (messageList.messages!=null) {
            temp.addAll(messageList.messages!!)
        }
        this.messages = temp
    }
}