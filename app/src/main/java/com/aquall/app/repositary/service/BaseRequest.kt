package com.aquall.app.repositary.service

import com.android.volley.*
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.StringRequest
import com.aquall.app.Localizer
import com.aquall.app.MyApplication
import com.aquall.app.utils.AppInitializer
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.Logger
import com.example.networkmanager.NetworkCallback
import org.json.JSONArray
import org.json.JSONObject

class BaseRequest<T> constructor(method:Int,path:String,body:T?,listener:Response.Listener<String>,err:Response.ErrorListener):StringRequest(method,path,listener,err)
{
    constructor(method: Int,path: String,body:T?,future:RequestFuture<String>) : this(method,path,body,future,future)
    constructor(method: Int,path: String,body:T?,callback: NetworkCallback<String>):this(method,path,body,
        Response.Listener { callback.onSuccess(it) },
        Response.ErrorListener {  callback.onFailure(it)} )

    var data = body;
    override fun getBody(): ByteArray? {
        if (data != null) {
            if (data is String) {
                val data = this.data as String
                Logger.e(data)
                return data.toByteArray()
            } else if (data is JSONObject) {
                val data = (this.data as JSONObject).toString()
                Logger.e(data)
                return data.toByteArray()
            } else if (data is JSONArray) {
                val data = (this.data as JSONArray).toString()
                Logger.e(data)
                return data.toByteArray()
            }
            Logger.e(data.toString())
            return data.toString().toByteArray()
        }
        return null
    }
     var mHeader:HashMap<String,String> = HashMap();
    fun setHeaders(header:HashMap<String,String>)
    {

        mHeader.putAll(header)
    }

    override fun getHeaders(): MutableMap<String, String> {
        mHeader.put("Content-Type","application/json")
        Logger.e("headers in api application/json ${Localizer.locale}")
        mHeader.put("locale",Localizer.locale)

        return mHeader
    }

    @Throws(AuthFailureError::class)
    override fun getParams(): Map<String?, String?>? {
        var params = super.getParams()
        if (data is java.util.HashMap<*, *>) {
            params = java.util.HashMap()
            params.putAll((data as java.util.HashMap<String?, String?>))
        }
        return params
    }


    override fun deliverResponse(response: String?) {
        if (response!=null) {
            Logger.e(response)
        }
        super.deliverResponse(response)
    }

    override fun deliverError(error: VolleyError?) {
        super.deliverError(error)
        if (error!=null) {
            val statusCode = error.networkResponse?.statusCode
            if (statusCode==440)
            {
                MyApplication.globalApiErrorObserver.postValue(error)
            }
        }
    }



}