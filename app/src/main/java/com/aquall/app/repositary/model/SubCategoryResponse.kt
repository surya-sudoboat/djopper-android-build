package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.SubCategory
class SubCategoryResponse {
    lateinit var data:List<SubCategory>
    lateinit var meta:Map<String,String>
    override fun toString(): String {
        return JsonUtil.toJson(this,SubCategoryResponse::class.java)
    }

    companion object{
        fun fromString(value:String):SubCategoryResponse
        {
            return JsonUtil.fromJson(value,SubCategoryResponse::class.java)
        }
    }
}