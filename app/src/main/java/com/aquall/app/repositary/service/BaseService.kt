package com.aquall.app.repositary.service

import com.android.volley.Request
import com.android.volley.toolbox.RequestFuture
import com.aquall.app.utils.Logger
import com.example.networkmanager.NetworkCallback

open class BaseService<T> constructor(baseUrl:String,token:String?=null) {
  val mBaseUrl = baseUrl
  var mToken = token
  fun post(path:String,body:T,future: RequestFuture<String>):BaseRequest<T>{
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.POST, path, body, future)
    if (mToken!=null) {
      baseRequest.mHeader.put("token", mToken!!)
    }
    return baseRequest
  }

  fun put(path:String,body:T,future: RequestFuture<String>):BaseRequest<T>{
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.PUT, path, body, future)
    if (mToken!=null) {
      baseRequest.mHeader.put("token", mToken!!)
    }
    return baseRequest
  }
  fun delete(path:String,body:T?,future: RequestFuture<String>):BaseRequest<T>{
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.DELETE, path, body, future)
    if (mToken!=null) {
      baseRequest.mHeader.put("token",mToken!!)
    }
    return baseRequest
  }

  fun post(path:String,body:T,callback:NetworkCallback<String>):BaseRequest<T>{
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.POST, path, body, callback)
    if (mToken!=null) {
      baseRequest.mHeader.put("token",mToken!!)
    }
    return baseRequest
  }

  fun put(path:String,body:T,callback:NetworkCallback<String>):BaseRequest<T>{
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.PUT, path, body, callback)
    if (mToken!=null) {
      baseRequest.mHeader.put("token",mToken!!)
    }
    return baseRequest
  }

  fun get(path: String,callback: NetworkCallback<String>):BaseRequest<T>
  {
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.GET, path, null, callback)
    if (mToken!=null) {
      baseRequest.mHeader.put("token",mToken!!)
    }
    return baseRequest
  }
  fun get(path: String,future: RequestFuture<String>):BaseRequest<T>
  {
    var path = if (path.startsWith("http")) path else mBaseUrl+path
    Logger.e("ServiceUrl:${path}")
    val baseRequest = BaseRequest<T>(Request.Method.GET, path, null, future)
    if (mToken!=null) {
      baseRequest.mHeader.put("token",mToken!!)
    }
    return baseRequest
  }





}