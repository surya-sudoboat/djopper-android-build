package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class LabelResponse {
    lateinit var data:LabelTranslation

    companion object{
        fun fromString(value:String):LabelResponse
        {
            return JsonUtil.fromJson(value,LabelResponse::class.java)
        }
    }
}