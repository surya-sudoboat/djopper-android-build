package com.aquall.app.repositary.model

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class UserModel constructor():RealmModel {
    @PrimaryKey
    lateinit var _id:String
     var firstName:String?=null
     var lastName:String?=null
     var email:String?=null
     var image:String?=null
}