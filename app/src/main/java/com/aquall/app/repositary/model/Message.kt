package com.aquall.app.repositary.model

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class Message() :RealmModel{
    @PrimaryKey
    var _id:String?=null
    var message:String? = null
    var sender_id:String? = null
    var timestamp:String?=null
}