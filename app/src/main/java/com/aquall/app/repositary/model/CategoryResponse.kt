package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil


class CategoryResponse {
    lateinit var data:List<CategoryDB>

    companion object{
        fun fromString(value:String):CategoryResponse
        {
            return JsonUtil.fromJson(value,CategoryResponse::class.java)
        }
    }
}