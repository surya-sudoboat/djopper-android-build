package com.aquall.app.repositary.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class MessageListDB() :RealmObject(){
    @PrimaryKey
    var _id:String? = null
    var messages:RealmList<Message>? = null


    constructor(messageList: MessageList):this()
    {
        this._id = messageList._id
        this.messages = RealmList()
        if (messageList.messages!=null) {

            val elements = messageList.messages!!

            this.messages?.addAll(elements)
        }
    }
}