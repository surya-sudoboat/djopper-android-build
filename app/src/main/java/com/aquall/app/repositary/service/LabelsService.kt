package com.aquall.app.repositary.service

import com.android.volley.toolbox.RequestFuture
import com.aquall.app.Localizer
import com.aquall.app.repositary.model.LabelResponse
import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.utils.AppInitializer
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.Logger
import com.example.networkmanager.NetworkCallback
import com.example.networkmanager.ServiceManager

class LabelsService: BaseService<String>(
    ServiceUrl.BASE_URL),NetworkCallback<String> {

    suspend fun getLabels(language:String):LabelsDB
    {
        val newFuture = RequestFuture.newFuture<String>()
        val get = get("/api/locales/${language}", newFuture)
        ServiceManager.processor.process(get)
        val get1 = newFuture.get()
        val fromString = LabelResponse.fromString(get1)
        fromString.data.translations?._id = fromString.data._id!!
        return fromString.data.translations!!
    }
    fun getLocales(): Localizer.LocaleData
    {
        val newFuture = RequestFuture.newFuture<String>()
        val get = get("/api/locales", newFuture)
        ServiceManager.processor.process(get)
        val get1 = newFuture.get()
        val fromString = JsonUtil.fromJson(get1,Localizer.LocaleData::class.java)
        return fromString
    }

    override fun onSuccess(response: String) {

    }

    override fun onFailure(error: Throwable) {

    }
}