package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Job
class JobResponse {

    lateinit var data:List<Job>
     var meta:Map<String,Int>?=null

    companion object{
        fun fromString(value:String): JobResponse
        {
            return JsonUtil.fromJson(value,JobResponse::class.java)
        }
    }
}