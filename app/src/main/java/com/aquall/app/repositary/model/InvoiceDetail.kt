package com.aquall.app.repositary.model

data class InvoiceDetail (
     var year:Long=0,
     var month:String,
     var date:String
)