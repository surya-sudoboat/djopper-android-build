package com.example.networkmanager

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.aquall.app.repositary.service.BaseRequest
import com.aquall.app.utils.OwnHttpStack
import java.io.File


class ServiceManager(context: Context){
    private var  requestQueue:RequestQueue
    init {
        val cache = DiskBasedCache(File(context.getCacheDir(), "volley"), 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(OwnHttpStack())
        // Instantiate the RequestQueue with the cache and network. Start the queue.
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
    }

    fun <T> process(request:Request<T>)
    {
        requestQueue.add(request)
    }


    companion object{
        lateinit var processor:ServiceManager;
        fun init(context: Context)
        {
            processor = ServiceManager(context)
        }
    }


}