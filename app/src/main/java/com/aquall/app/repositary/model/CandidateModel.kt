package com.aquall.app.repositary.model

class CandidateModel {
    var _id:String?=null
    var is_unread_message:Boolean=false
    var applicant:UserModel?=null
    var chat_room:ChatRoom?=null
}