package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class Payments {
class Amount(val value: String,val currency:String)
    lateinit var _id:String
    lateinit var amount:Amount
    lateinit var status:String
    lateinit var payment_id:String
    lateinit var payment_created_at:String
    lateinit var description:String
    lateinit var method:String
    lateinit var url_expired_at:String
    lateinit var user_id:String
    var payment_expired_at:String?=null
    var is_cancelable:Boolean=true


    override fun toString(): String {
        return JsonUtil.toJson(this,Payments::class.java)
    }

    fun getSample():Payments{
        val payments = this
        payments._id = "1"
        payments.amount = Amount("4000","inr")
        payments.description = "JOB Description"
        payments.payment_created_at = "2022-06-09T17:15:05.000Z"
        payments.payment_expired_at = "2022-06-09T19:15:05.000Z"
        payments.url_expired_at = "2022-06-09T19:15:05.000Z"
        payments.method ="credit card"
        payments.is_cancelable  = true
        payments.status = "PAID"
        return payments

    }

    companion object {
        fun fromString(value: String): Payments {
            return JsonUtil.fromJson(value,Payments::class.java)
        }
    }
}