package com.aquall.app.repositary.model

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class JobApplication:RealmModel {
    @PrimaryKey
    lateinit var _id :String
}