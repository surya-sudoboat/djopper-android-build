package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class CandidateResponse {

    var data:List<CandidateModel>? = null



    companion object{
        fun fromString(json:String):CandidateResponse
        {
            return JsonUtil.fromJson(json,CandidateResponse::class.java)
        }
    }
}