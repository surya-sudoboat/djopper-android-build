package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Favorite
import com.google.gson.annotations.SerializedName

class FavoriteResponse {
    @SerializedName("data", alternate = ["jobs"])
    lateinit var jobs:List<Favorite>
    var unread_fav_job_count:Int = 0

    companion object{
        fun fromString(value:String):FavoriteResponse
        {
            return JsonUtil.fromJson(value,FavoriteResponse::class.java)
        }
    }
}