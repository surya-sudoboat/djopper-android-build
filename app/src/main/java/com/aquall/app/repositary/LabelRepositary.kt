package com.aquall.app.repositary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aquall.app.Localizer
import com.aquall.app.repositary.dao.LabelsDAO
import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.repositary.service.LabelsService
import com.aquall.app.utils.AppInitializer
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.Logger
import com.aquall.app.utils.SharedPrefUtil
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Labels
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDate
import kotlin.coroutines.CoroutineContext

class LabelRepositary: BaseRepositary() {


    fun getLabels(language:String,shouldLoadFromStorage:Boolean=true):MutableLiveData<LabelsDB>{
       val labels = MutableLiveData<LabelsDB>()
        networkStatus.postValue(DataStatus.LOADING)
        CoroutineScope(Dispatchers.IO).launch {
            if (shouldLoadFromStorage) {
                val first = LabelsDAO().getFirst()
                if (first!=null)
                {
                    Localizer.labels = first
                    labels.postValue(first)
                }
            }
            try {
                val labelsNetwork = LabelsService().getLabels(language)
                Localizer.labels = labelsNetwork
                labels.postValue(labelsNetwork)
                LabelsDAO().clearAll()
                LabelsDAO().save(labelsNetwork)
                Localizer.locale = language
                SharedPrefUtil.getInstance().locale = language
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)

            }
        }

        return labels

    }


    fun getLocale():MutableLiveData<Localizer.LocaleData>{
        val labels = MutableLiveData<Localizer.LocaleData>()
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val locales = SharedPrefUtil.getInstance().locales
                if (locales!=null)
                {
                    val fromJson = JsonUtil.fromJson(locales, Localizer.LocaleData::class.java)
                    Localizer.locales = fromJson
                    labels.postValue(fromJson)
                }
                val labelsNetwork = LabelsService().getLocales()
                if (labelsNetwork!=null) {
                    Localizer.locales = labelsNetwork
                    labels.postValue(labelsNetwork)
                    SharedPrefUtil.getInstance().locales = JsonUtil.toJson(labelsNetwork,Localizer.LocaleData::class.java)
                }

            } catch (e: Exception) {
                Logger.e(e)
            }
        }

        return labels

    }




}