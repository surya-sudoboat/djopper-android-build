package com.aquall.app.repositary.service

import com.android.volley.toolbox.RequestFuture
import com.aquall.app.Localizer
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.*
import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.Logger
import com.aquall.app.utils.model.SearchKeys
import com.example.networkmanager.ServiceManager
import com.google.gson.Gson
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class JobService:BaseService<String>(
    ServiceUrl.BASE_URL, AuthRepositary.token) {

    fun getJobs(searchKeys: SearchKeys,page:Int):JobResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                post("/api/job_offers/browse?page=${page}&limit=30", searchKeys.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = JobResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }


    fun getFavorite():FavoriteCountResponse2
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/favourites", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = FavoriteCountResponse2.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }

    fun getAppliedJobs():FavoriteResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_applications", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = FavoriteResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }

    fun getAppliedChatJobs():MessageJobResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_applications/applied_jobs_applications", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = MessageJobResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }

    fun getPostedJobsChat():MessageJobResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_applications/posted_jobs_applications", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = MessageJobResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }
    fun getExpiredJobs():JobResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_offers/list-expired-jobs", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = JobResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }
    fun getPostedJobs():JobResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_offers/list-posted-jobs", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = JobResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }

    fun getCandidates(jobId:String):CandidateResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_applications/${jobId}", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            val fromString = CandidateResponse.fromString(get)
            return fromString
        } catch (e: Exception) {
            throw e
        }

    }

    fun setFavorite(jobId:String)
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject=JSONObject()
            jsonObject.put("job_offer_id",jobId)
            val post =
                post("/api/favourites/add_favourite",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)

        } catch (e: Exception) {
            throw e
        }
    }
    fun createJob(job:CreateJobRequest):JobDescriptionResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post = post("/api/job_offers",job.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
           return JsonUtil.fromJson(get,JobDescriptionResponse::class.java)
        } catch (e: Exception) {
            throw e
        }
    }

    fun createJob(job:CreateJobRequest,jobId:String):JobDescriptionResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post = put("/api/job_offers/${jobId}",job.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return JsonUtil.fromJson(get,JobDescriptionResponse::class.java)
        } catch (e: Exception) {
            throw e
        }
    }
    fun applyJob(jobId:String):JobApplyResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject=JSONObject()
            jsonObject.put("job_offer_id",jobId)
            val post =
                post("/api/job_applications/apply",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return JsonUtil.fromJson(get,JobApplyResponse::class.java)
        } catch (e: Exception) {
            throw e
        }
    }
    fun updateProfilePicture(jobId:String,fileName:String,file:ByteArray):String
    {
        try{
            val newFuture = RequestFuture.newFuture<String>()
            val multiPartRequest = MultiPartRequest("/api/job_offers/${jobId}/upload",fileName, "file", file, newFuture)
            ServiceManager.processor.process(multiPartRequest)
            val get1 = newFuture.get(30,TimeUnit.SECONDS)
            return get1
        }catch (e:java.lang.Exception)
        {
            throw e
        }
    }
    fun getJobData(jobId:String):JobDescriptionResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/job_offers/${jobId}", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return JsonUtil.fromJson(get,JobDescriptionResponse::class.java)
        } catch (e: Exception) {
            throw e
        }
    }

    fun paymentDetail(jobId:String):String
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject=JSONObject()
            jsonObject.put("job_offer_id",jobId)
            val post =
                post("/api/payment",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return get
        } catch (e: Exception) {
            throw e
        }
    }



    fun invoiceDetails():List<InvoiceDetail>
    {
        try {
            val future = RequestFuture.newFuture<String>()

            val post =
                get("/api/invoice/invoice-details", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return Gson().fromJson(get,Array<InvoiceDetail>::class.java).asList()
        } catch (e: Exception) {
            throw e
        }
    }

    fun getRecentSearch():List<SearchKeys>?
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/recent_searches/recent-searches", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            Logger.e("recent search -> ${get}")
            val recentSearchResponse = Gson().fromJson(get, RecentSearchResponse::class.java)
            val searchResults = recentSearchResponse.data?.get(0)?.searchResults
            searchResults?.let {
                val locale = Localizer.locale
                for (searchKey in searchResults)
                {
                    val categories = searchKey.category
                    for (category in categories){
                        category.category_name?.let { categoryList->
                            for (categoryName in categoryList){
                                if (categoryName.language.equals(locale))
                                {
                                    category.name = categoryName.value
                                }
                            }
                        }
                    }
                    val subCategories  = searchKey.subCategory
                    for (subCategory in subCategories){
                        subCategory.sub_category_name?.let { subCategoryList->
                            for (subCategoryName in subCategoryList){
                                if (subCategoryName.language.equals(locale))
                                {
                                    subCategory.name = subCategoryName.value
                                }
                            }
                        }
                    }

                }
            }
                return searchResults
        } catch (e: Exception) {
            throw e
        }

    }

    fun getPaymentHistory(startDate:String,endDate:String):List<Payments>
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/payment/payment-details?start_date=$startDate&end_date=$endDate", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return PaymentsResponse.fromString(get).data
        } catch (e: Exception) {
            throw e
        }
    }
    fun getPDF(startDate:String,endDate:String):String
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/pdf_generator?start_date=$startDate&end_date=$endDate", future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
            return get
        } catch (e: Exception) {
            throw e
        }
    }
    fun removeFavorite(jobId:String)
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                delete("/api/favourites/${jobId}",null, future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)

        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteJob(jobId:String)
    {
        try {
            val future = RequestFuture.newFuture<String>()

            val post =
                delete("/api/job_offers/${jobId}",null, future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteJobApplication(jobId:String)
    {
        try {
            val future = RequestFuture.newFuture<String>()

            val post =
                delete("/api/job_applications/${jobId}",null, future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
        } catch (e: Exception) {
            throw e
        }
    }

    fun holdJobOffer(job_offer_id:String,is_on_hold:Boolean = false)
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject=JSONObject()
            jsonObject.put("is_on_hold",is_on_hold)
            val post =
                put("/api/job_offers/hold/${job_offer_id}",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30,TimeUnit.SECONDS)
        } catch (e: Exception) {
            throw e
        }
    }
}