package com.aquall.app.repositary.service

import com.android.volley.toolbox.RequestFuture
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.ChatRoomResponse
import com.aquall.app.repositary.model.MessageResponse
import com.aquall.app.utils.JsonUtil
import com.example.networkmanager.ServiceManager
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class ChatService:BaseService<String>(
    ServiceUrl.BASE_URL, AuthRepositary.token) {
    fun createChat(jobId:String):ChatRoom
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject= JSONObject()
            jsonObject.put("job_application_id",jobId)
            val post =
                post("/api/chat_rooms",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30, TimeUnit.SECONDS)
            return JsonUtil.fromJson(get,ChatRoomResponse::class.java).data!!
        } catch (e: Exception) {
            throw e
        }
    }

    fun deleteChat(jobId:String):String
    {
        try {
            val future = RequestFuture.newFuture<String>()

            val post =
                delete("/api/chat_rooms/delete/"+jobId,null, future)
            ServiceManager.processor.process(post)
            val get = future.get(30, TimeUnit.SECONDS)
            return get
        } catch (e: Exception) {
            throw e
        }
    }


    fun listMessages(jobId:String):MessageResponse
    {
        try {
            val future = RequestFuture.newFuture<String>()
            val post =
                get("/api/chat_rooms/${jobId}/messages", future)
            ServiceManager.processor.process(post)
            val get = future.get(30, TimeUnit.SECONDS)
            return JsonUtil.fromJson(get, MessageResponse::class.java)
        } catch (e: Exception) {
            throw e
        }
    }



    fun sendChat(jobId:String,message:String)
    {
        try {
            val future = RequestFuture.newFuture<String>()
            var jsonObject= JSONObject()
            jsonObject.put("chat_room_id",jobId)
            jsonObject.put("message",message)
            val post =
                put("/api/chat_rooms/messages",jsonObject.toString(), future)
            ServiceManager.processor.process(post)
            val get = future.get(30, TimeUnit.SECONDS)

        } catch (e: Exception) {
            throw e
        }
    }
}