package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class FavoriteCountResponse2 {
    lateinit var data:FavoriteResponse2


    companion object{
        fun fromString(value:String):FavoriteCountResponse2
        {
            return JsonUtil.fromJson(value,FavoriteCountResponse2::class.java)
        }
    }
}