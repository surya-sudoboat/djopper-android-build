package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class FavoriteCountResponse {
    lateinit var data:FavoriteResponse


    companion object{
        fun fromString(value:String):FavoriteCountResponse
        {
            return JsonUtil.fromJson(value,FavoriteCountResponse::class.java)
        }
    }
}