package com.aquall.app.repositary.model

import com.aquall.app.utils.model.Job
import com.aquall.app.utils.model.Profile
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class JobDB constructor(): RealmModel{
    @PrimaryKey
    lateinit var _id:String
    lateinit var name:String
    lateinit var start_date:String
    lateinit var end_date:String
    lateinit var description:String
    lateinit var email:String
    lateinit var mobile:String
    var image:String? = null
    lateinit var locationDB:LocationDB
    var is_on_hold=false
    var is_mobile_contactable = false
    var is_email_contactable  =false
    var is_active = false
    var is_favourite = false
    var unread_count:Int =0
    var payment:Payment?=null
    lateinit var job_owner_id:String
    lateinit var category:CategoryDB
    lateinit var subcategory:SubCategoryDB
    var applied_count:Int = 0
    lateinit var job_owner:UserModel
    lateinit var job_application:JobApplication
    var job_application_id:String? = null
    var updatedAt:String?=null

}