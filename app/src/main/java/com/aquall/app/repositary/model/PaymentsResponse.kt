package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class PaymentsResponse {
    lateinit var data :List<Payments>

    companion object {
        fun fromString(value: String): PaymentsResponse {
            return JsonUtil.fromJson(value,PaymentsResponse::class.java)
        }
    }
}