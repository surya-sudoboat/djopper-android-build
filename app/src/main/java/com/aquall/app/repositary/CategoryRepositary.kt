package com.aquall.app.repositary

import androidx.lifecycle.MutableLiveData
import com.aquall.app.repositary.dao.CategoryDAO
import com.aquall.app.repositary.model.CategoryDB
import com.aquall.app.repositary.service.CategoryService
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SubCategory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.logging.Logger

class CategoryRepositary : BaseRepositary() {
    lateinit var mutableCategoryLiveData:MutableLiveData<List<Category>>
    fun getCategories(liveData: MutableLiveData<List<Category>>,forceNetworkData:Boolean = false)
    {
        mutableCategoryLiveData =liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                var categoriesDB:List<CategoryDB>? = null
                if (!forceNetworkData) {
                    CategoryDAO().getAll()
                }
                if (categoriesDB==null||forceNetworkData)
                {
                    categoriesDB = getCategoryNetwork()
                }
                val categoryList = ArrayList<Category>()
                if (categoriesDB!=null) {
                    for (category in categoriesDB)
                    {
                        categoryList.add(Category(category))
                    }
                }
                mutableCategoryLiveData.postValue(categoryList)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }

    lateinit var mutableSubCategoryLiveData:MutableLiveData<List<SubCategory>>
    fun getSubCategories(categoryId:String,liveData: MutableLiveData<List<SubCategory>>)
    {
        com.aquall.app.utils.Logger.e("CategoryRespositary subcategory service started")
        mutableSubCategoryLiveData =liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val subCategories = CategoryService().getSubCategories(categoryId)
                mutableSubCategoryLiveData.postValue(subCategories)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }
    fun getSubCategories(categoryId:List<String>,liveData: MutableLiveData<List<SubCategory>>)
    {
        mutableSubCategoryLiveData =liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val subCategories = CategoryService().getSubCategories(categoryId)
                mutableSubCategoryLiveData.postValue(subCategories)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                com.aquall.app.utils.Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }
    public fun getCategoryNetwork(): List<CategoryDB> {
        val categoriesDB = CategoryService().getCategories()
        CategoryDAO().save(categoriesDB)
        return categoriesDB
    }

}