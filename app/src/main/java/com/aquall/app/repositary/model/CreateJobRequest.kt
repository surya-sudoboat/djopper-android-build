package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.Location
import com.aquall.app.utils.model.SubCategory

class CreateJobRequest {
    lateinit var name:String
    lateinit var start_date:String
    lateinit var end_date:String
    lateinit var description:String
    lateinit var email:String
    lateinit var mobile:String
    var location: Location? =null
    var is_on_hold=false
    var is_mobile_contactable = true
    var is_email_contactable  =true
    var is_active = true
    var job_owner_id:String?=null
    var category_id: String?=null
    var subcategory_id: String?=null


    fun setLocationName(name:String)
    {
        val location1 = Location()
        location1.name = name
        location1.type = "Point"
        location1.coordinates = arrayListOf(77.729398, 11.329653)
        location = location1
    }

    override fun toString(): String {
        return JsonUtil.toJson(this,CreateJobRequest::class.java)
    }
}