package com.aquall.app.repositary.service

import com.android.volley.toolbox.RequestFuture
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.CategoryDB
import com.aquall.app.repositary.model.CategoryResponse
import com.aquall.app.repositary.model.SubCategoryResponse
import com.aquall.app.utils.Logger
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SubCategory
import com.example.networkmanager.ServiceManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

class CategoryService:BaseService<String>(
    ServiceUrl.BASE_URL,AuthRepositary.token) {

     fun getCategories():ArrayList<CategoryDB>{
         try {
             Logger.e("Token: "+AuthRepositary.token)
             val future = RequestFuture.newFuture<String>()
             val get = get("/api/categories", future)
             ServiceManager.processor.process(get)
             val get1 = future.get(30,TimeUnit.SECONDS)
             val fromString = CategoryResponse.fromString(get1)
             return ArrayList(fromString.data)
         } catch (e: Exception) {
        throw e
         }
     }


    fun getSubCategories(categoryId:String):List<SubCategory>{
        try {
            val future = RequestFuture.newFuture<String>()
            val get = get("/api/sub_categories?category_id=${categoryId}", future)
            ServiceManager.processor.process(get)
            val get1 = future.get(30,TimeUnit.SECONDS)
            val fromString = SubCategoryResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }

    fun getSubCategories(categoryIdList:List<String>):List<SubCategory>{
        try {
            val future = RequestFuture.newFuture<String>()
            var query = ""
            categoryIdList.forEach {  query +="category_id=$it&"}
            query.removeSuffix("&")
            val get = get("/api/sub_categories?$query", future)
            ServiceManager.processor.process(get)
            val get1 = future.get(30,TimeUnit.SECONDS)
            val fromString = SubCategoryResponse.fromString(get1)
            return fromString.data
        } catch (e: Exception) {
            throw e
        }
    }
}