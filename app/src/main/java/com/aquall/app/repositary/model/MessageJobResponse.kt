package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil

class MessageJobResponse {
    lateinit var data:MessageJobList


    companion object{
        fun fromString(value:String):MessageJobResponse
        {
            return JsonUtil.fromJson(value,MessageJobResponse::class.java)
        }
    }
}