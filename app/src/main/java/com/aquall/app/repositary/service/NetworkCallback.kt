package com.example.networkmanager

interface NetworkCallback<T>{
    fun onSuccess(response:T)
    fun onFailure(error:Throwable)
}