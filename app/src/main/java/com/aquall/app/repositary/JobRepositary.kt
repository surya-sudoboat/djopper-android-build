package com.aquall.app.repositary

import androidx.lifecycle.MutableLiveData
import com.aquall.app.repositary.model.*
import com.aquall.app.repositary.service.ChatService
import com.aquall.app.repositary.service.JobService
import com.aquall.app.utils.Logger
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.Job
import com.aquall.app.utils.model.JobsList
import com.aquall.app.utils.model.SearchKeys
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.util.ArrayList

class JobRepositary : BaseRepositary() {
    private lateinit var mLiveData:MutableLiveData<JobsList>
fun fetchJobs(searchKeys: SearchKeys,liveData:MutableLiveData<JobsList>)
{
    mLiveData = liveData
    CoroutineScope(Dispatchers.IO).launch {
        try {
            networkStatus.postValue(DataStatus.LOADING)
            val jobs = JobService().getJobs(searchKeys, 1)
            val total_pages = jobs.meta?.get("total_pages")?:0
            val page = jobs.meta?.get("page")?:0
            mLiveData.postValue(JobsList(jobs =ArrayList(jobs.data), moreData = page<total_pages,page=page))
            networkStatus.postValue(DataStatus.DATA_AVAILABLE)
        } catch (e: Exception) {
            networkStatus.postValue(DataStatus.FAILED)
            Logger.e(e)
            latestError = e

        }

    }
}


    fun fetchFavorites(liveData:MutableLiveData<JobsList>)
    {
        mLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getFavorite()
                SocketClient.getSocketClient().unReadFavLiveData.postValue(jobs.data.unread_fav_job_count)
                val arrayList = ArrayList<Job>()
                val chatArrayList = HashMap<String,List<ChatRoom>>()
                for (job in jobs.data.jobs)
                {
                    if (job.job_offer!=null) {
                        job.job_offer?.isRead = job.seen
                        arrayList.add(Job(job.job_offer!!))
                        if (job.chat_rooms!=null) {
                            chatArrayList.put(job.job_offer?._id!!,job.chat_rooms!!)
                        }
                    }
                }
                mLiveData.postValue(JobsList(jobs =arrayList, moreData = false,page=0,0,chatArrayList))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }



    fun fetchAppliedJob(liveData: MutableLiveData<List<Job>>){
        CoroutineScope(Dispatchers.IO).launch{
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getAppliedJobs()

                liveData.postValue(jobs.jobs.map { it.job_offer!!.apply {
                    job_application_id = it._id
                    it.job_owner?.let { jobOwner-> this.job_owner = jobOwner }
                } })
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e
            }
        }
    }
    fun fetchAppliedJobChat(liveData:MutableLiveData<JobsList>)
    {
        mLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getAppliedChatJobs()
                val arrayList = ArrayList<Job>()
                val chatArrayList = HashMap<String,List<ChatRoom>>()
                for (job in jobs.data.jobs!!)
                {
                    if (job.job_offer!=null) {
                        job.job_offer?.isRead = ! job.isRead
                        job.job_offer?.unread_count =job.unread_count
                        arrayList.add(job.job_offer!!)
                        if (job.chat_rooms!=null) {
                            chatArrayList.put(job.job_offer?._id!!,job.chat_rooms!!)
                        }
                    }
                }
                mLiveData.postValue(JobsList(jobs =arrayList, moreData = false,page=0,jobs.data.unread_count,chatArrayList))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e
            }
        }
    }


    fun fetchPostedJobChat(liveData:MutableLiveData<JobsList>)
    {
        mLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getPostedJobsChat()
                val arrayList = LinkedHashSet<Job>()
                val chatArrayList = HashMap<String,List<ChatRoom>>()
                for (job in jobs.data?.jobs!!)
                {
                    if (job.job_offer!=null) {
                        job.job_offer?.isRead = job.isRead
                        job.job_offer?.unread_count=job.unread_count
                        arrayList.add(job.job_offer!!)
                        if (job.chat_rooms!=null&&!chatArrayList.containsKey(job.job_offer?._id)) {
                            chatArrayList.put(job.job_offer?._id!!,job.chat_rooms!!)
                        }
                    }
                }
                mLiveData.postValue(JobsList(jobs =ArrayList(arrayList), moreData = false,page=0,jobs.data.unread_count,chatArrayList))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }


    fun fetchExpiredJob(liveData:MutableLiveData<JobsList>)
    {
        mLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getExpiredJobs()
                val arrayList = ArrayList<Job>()
                val chatArrayList = HashMap<String,List<ChatRoom>>()
                for (job in jobs.data)
                {
                    arrayList.add(job)

                }
                mLiveData.postValue(JobsList(jobs =arrayList, moreData = false,page=0,0,chatArrayList))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }
    fun fetchPostedJob(liveData:MutableLiveData<JobsList>)
    {
        mLiveData = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val jobs = JobService().getPostedJobs()
                val arrayList = ArrayList<Job>()
                val chatArrayList = HashMap<String,List<ChatRoom>>()
                for (job in jobs.data)
                {
                    arrayList.add(job)

                }
                mLiveData.postValue(JobsList(jobs =arrayList, moreData = false,page=0,0,chatArrayList))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }
    var mLiveDataUserModel:MutableLiveData<List<CandidateModel>>? = null
    fun fetchCandidates(jobId:String,liveData:MutableLiveData<List<CandidateModel>>)
    {
        mLiveDataUserModel = liveData
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val candidates = JobService().getCandidates(jobId)

                mLiveDataUserModel?.postValue(candidates.data)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }

    fun addAsFavorite(jobId:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                JobService().setFavorite(jobId)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e

            }
        }
    }

    fun applyJob(jobId:String,liveData:MutableLiveData<JobApply>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val applyJob = JobService().applyJob(jobId)
                val createChat = ChatService().createChat(applyJob.data._id)
                val hashMap = JobApply()
                hashMap.jobApplication = applyJob.data
                hashMap.chatRoom = createChat
                liveData.postValue(hashMap)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }



    fun createChat(jobId: String,chatroomLiveData:MutableLiveData<ChatRoom>){
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val createChat = ChatService().createChat(jobId)
                chatroomLiveData.postValue(createChat)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }


    fun createPayment(jobId:String,liveData: MutableLiveData<String>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val applyJob = JobService().paymentDetail(jobId)
                val jsonObject = JSONObject(applyJob)
                val jsonObject1 = jsonObject.getJSONObject("data")
                val string = jsonObject1.getString("gatewayUrl")
                liveData.postValue(string)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }


    fun getPaymentList(startDate:String,endDate:String,liveData: MutableLiveData<List<Payments>>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val history = JobService().getPaymentHistory(startDate,endDate)
                liveData.postValue(history)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }

    fun getRecentSearch(liveData: MutableLiveData<List<SearchKeys>>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val searchKeys = JobService().getRecentSearch()
                liveData.postValue(searchKeys)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }

    fun getPdf(startDate:String,endDate:String,liveData: MutableLiveData<InvoicePdfResponse>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                liveData.postValue(InvoicePdfResponse(null,DataStatus.LOADING))
                val history = JobService().getPDF(startDate,endDate)
                liveData.postValue(InvoicePdfResponse(history,DataStatus.DATA_AVAILABLE))
            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                liveData.postValue(InvoicePdfResponse(status = DataStatus.FAILED))
                latestError = e
            }
        }
    }

    fun getInvoiceList(liveData: MutableLiveData<List<InvoiceDetail>>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val history = JobService().invoiceDetails()
                liveData.postValue(history)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }
    fun getJobData(jobId:String,liveData: MutableLiveData<Job>)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                val applyJob = JobService().getJobData(jobId)
                liveData.postValue(Job(applyJob.data!!))
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }






    fun createJob(job:CreateJobRequest,liveData: MutableLiveData<Job>,fileName:String?=null,fileData:ByteArray?=null,isEdit:Boolean=false,jobId:String?=null)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                var createJob:Job?= null
                if (isEdit)
                {
                    createJob = JobService().createJob(job,jobId!!).data?.let { Job(it) }
                }else{
                 createJob = JobService().createJob(job).data?.let { Job(it) }

                }
                if (fileName!=null&&fileData!=null) {
                    val updateProfilePicture =
                        JobService().updateProfilePicture(createJob?._id!!, fileName, fileData)
                    Logger.e(updateProfilePicture)
                    val jsonObject = JSONObject(updateProfilePicture)
                    val data = jsonObject.getJSONObject("data")
                    val string = data.getString("url")
                    createJob?.image = string
                }
                liveData.postValue(createJob)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e
            }
        }
    }


    fun removeFavorite(jobId:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                JobService().removeFavorite(jobId)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e

            }
        }
    }

    fun deleteJob(jobId:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                JobService().deleteJob(jobId)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e

            }
        }
    }
    fun deleteJobApplication(jobId:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                JobService().deleteJobApplication(jobId)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e

            }
        }
    }
  fun holdJobOffer(jobId:String,is_on_hold:Boolean=false)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                JobService().holdJobOffer(jobId,is_on_hold)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                Logger.e(e)
                networkStatus.postValue(DataStatus.FAILED)
                latestError = e

            }
        }
    }


}