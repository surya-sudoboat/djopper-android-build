package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Profile
import io.realm.RealmList

class UpdateProfileRequest() {
    var _id:String? = null
    var firstName:String? = null
    var lastName:String? = null
    var email:String? = null
    var mobile:String? = null
    var token:String? = null
    var DOB:String? = null
    var location: LocationDB? = null
    var gender: String? = null
    var password:String? = null
    var image:String? =null
    var unread_message_count:Int = 0
    var locale:String? = null
    var countryCode:String?=null
    var dialCode:String? = null
    var profession:String? = null
    var is_notification_enabled:Boolean = true
    var is_notification_sound_enabled:Boolean = true
    var preferred_category: List<String>?=null
    var preferred_subcategory: List<String>?=null
    var range:String? = null

    constructor(profile: Profile):this()
    {
        this._id=profile._id
        this.firstName=profile.firstName
        this.lastName=profile.lastName
        this.DOB = profile.DOB
        this.email = profile.email
        this.mobile = profile.mobile
        this.range = profile.range
        this.location= profile.location?.let { LocationDB(it) }
        this.gender = profile.gender
        this.password = profile.password
        this.image = profile.image
        this.dialCode = profile.dialCode
        this.countryCode = profile.countryCode
        this.is_notification_enabled = profile.is_notification_enabled
        this.is_notification_sound_enabled = profile.is_notification_sound_enabled
        val categoryRealmList = ArrayList<String>()
        if (profile.preferred_category!=null) {
            categoryRealmList.addAll(profile.preferred_category!!.map { it._id }!!)
        }
        this.preferred_category = categoryRealmList
        val subCategoryRealmList = ArrayList<String>()
        if (profile.preferred_subcategory!=null) {
            subCategoryRealmList.addAll(profile.preferred_subcategory!!.map { it._id })
        }
        this.preferred_subcategory = subCategoryRealmList
        this.profession = profile.profession
        this.is_notification_enabled = profile.is_notification_enabled
        this.is_notification_sound_enabled = profile.is_notification_sound_enabled


    }
    override fun toString(): String {
        return  JsonUtil.toJson(this,UpdateProfileRequest::class.java)
    }
}