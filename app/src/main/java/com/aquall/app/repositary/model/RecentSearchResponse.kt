package com.aquall.app.repositary.model

import com.aquall.app.utils.model.SearchKeys

class RecentSearchResponse() {
    class SearchResults{
        var searchResults : List<SearchKeys>? = null
    }

    var data:List<SearchResults>? = null


}