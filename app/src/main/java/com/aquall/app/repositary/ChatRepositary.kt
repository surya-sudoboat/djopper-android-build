package com.aquall.app.repositary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.aquall.app.repositary.dao.MessageDAO
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.Message
import com.aquall.app.repositary.model.MessageList
import com.aquall.app.repositary.model.MessageListDB
import com.aquall.app.repositary.service.ChatService
import com.aquall.app.repositary.service.JobService
import com.aquall.app.utils.Logger
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.enums.DataStatus
import com.aquall.app.utils.model.ChatRoomMessage
import com.aquall.app.utils.model.Job
import com.aquall.app.utils.model.JobsList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.ArrayList

class ChatRepositary: BaseRepositary() {
    private  var mLiveData:MediatorLiveData<MessageList> = MediatorLiveData()
    fun fetchChat(jobId:String):LiveData<MessageList>
    {

        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                val byPrimaryKey = MessageDAO().getByPrimaryKey(jobId)
                if (byPrimaryKey!=null&&byPrimaryKey?.messages?.size!! >0) {
                    mLiveData?.postValue(MessageList(byPrimaryKey))
                }
                val messageResponse = ChatService().listMessages(jobId)
                messageResponse.data?.let {
                    MessageDAO().save(MessageListDB( it))
                    mLiveData?.postValue(it)
                }
                SocketClient.getSocketClient().unReadLiveData.postValue(messageResponse.data?.unread_message_count)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }

        return mLiveData
    }

    fun sendchat(chat_room_id:String,message:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                networkStatus.postValue(DataStatus.LOADING)
                ChatService().sendChat(chat_room_id,message)
                val messageResponse = ChatService().listMessages(chat_room_id)
                messageResponse.data?.let {
                    MessageDAO().save(MessageListDB( it))
                    if (mLiveData!= null) {
                        mLiveData?.postValue(it)
                    }
                }
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)
            } catch (e: Exception) {
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }

        }
    }

    fun deleteChat(chat_room_id: String){
        CoroutineScope(Dispatchers.IO).launch {
            try{
                networkStatus.postValue(DataStatus.LOADING)
                ChatService().deleteChat(chat_room_id)
                MessageDAO().deleteById(chat_room_id)
                networkStatus.postValue(DataStatus.DATA_AVAILABLE)

            }catch (e:Exception){
                networkStatus.postValue(DataStatus.FAILED)
                Logger.e(e)
                latestError = e

            }
        }
    }
}