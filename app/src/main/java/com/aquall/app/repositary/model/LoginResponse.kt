package com.aquall.app.repositary.model

import com.aquall.app.utils.JsonUtil
import com.aquall.app.utils.model.Profile


class LoginResponse {
    lateinit var data:LoginDB

    companion object {
        fun fromString(value: String): LoginResponse {
            return JsonUtil.fromJson(value,LoginResponse::class.java)
        }
    }
}