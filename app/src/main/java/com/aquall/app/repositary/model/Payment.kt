package com.aquall.app.repositary.model

import com.aquall.app.Localizer
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class Payment:RealmModel {
    @PrimaryKey
    var id:String? = null
    var status:String? = null

    companion object {
        fun getStatusFromString(string: String):STATUS{
           return STATUS.getSTATUS(string)
        }
    }

    abstract class STATUS{
        companion object{
            fun getSTATUS(value:String):STATUS
            {
               return when (value)
                {
                    NEW.getString()->NEW
                    CONTINUE.getString()->CONTINUE
                    RETRY.getString()->RETRY
                    EXPIRED.getString()->EXPIRED
                    PAID.getString()->PAID
                   else -> {
                       NEW
                   }
               }


            }
        }
        abstract fun getButtonText():String
        abstract fun getString():String
    }
    object NEW:STATUS(){
        override fun getString():String{
            return "new"
        }
        override fun getButtonText():String{
           return Localizer.labels.pay_now!!
        }
    }

    object CONTINUE:STATUS(){
        override fun getString():String{
            return "continue"
        }
        override fun getButtonText():String{
            return Localizer.labels.continue_payment!!
        }
    }

    object RETRY:STATUS(){
        override fun getString():String{
            return "failure"
        }
        override fun getButtonText():String{
            return Localizer.labels.retry_payment!!
        }
    }

    object EXPIRED:STATUS(){
        override fun getString():String{
            return "expired"
        }
        override fun getButtonText():String{
            return Localizer.labels.renew_payment!!
        }
    }

    object PAID:STATUS(){
        override fun getString():String{
            return "paid"
        }
        override fun getButtonText():String{
            return Localizer.labels.paid!!
        }
    }
}