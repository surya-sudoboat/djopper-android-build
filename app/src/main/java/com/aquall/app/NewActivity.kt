package com.aquall.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.aquall.app.fragments.DashboardFragment
import com.aquall.app.fragments.SearchJobFragment
import com.aquall.app.utils.ScreenName

class NewActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)
        val intExtra = intent.getIntExtra("clazzValue", -1)
        val data = intent.getStringExtra("data")
        if (intExtra>0) {
            val screen = ScreenName.Name.getClassByValue(intExtra)
            replaceFragment(screen,data)
        }
    }


    override fun onBackPressed() {
        finish()
    }


}