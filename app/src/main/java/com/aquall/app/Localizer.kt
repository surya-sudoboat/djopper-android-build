package com.aquall.app

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.utils.AppInitializer

object Localizer {

    var locale = "en"
    var locales = LocaleData()
    var labels:LabelsDB = LabelsDB()


    fun getLabel():String
    {
        if (locales.data!=null) {
            for (lang in locales.data!!)
            {
                if (lang.code!=null&&lang.code.equals(locale)&&lang.label!=null)
                {
                    return lang.label!!
                }
            }
        }

        return "English"
    }




    class Locale{
        var _id:String? = null
        var code:String? = null
        var label:String? = null
    }
    class LocaleData{
        var data:List<Locale>? = null


    }

}