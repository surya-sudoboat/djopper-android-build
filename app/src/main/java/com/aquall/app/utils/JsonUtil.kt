package com.aquall.app.utils

import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SubCategory
import com.google.gson.Gson
object JsonUtil {


    class SubCategoryAdapter{
        lateinit var _id:String
         var name:String?=null
         var value:String?=null

    fun SubCategoryFromIntermediate(value: SubCategoryAdapter):SubCategory
    {

        val subCategory = SubCategory()
        subCategory._id = value._id
        if (value.name!=null)
            subCategory.name = value.name!!
        else if (value.value!=null)
            subCategory.name = value.value!!
        return subCategory
    }
    }

    class ArrayListCategoryAdapter{

        fun  listToArrayList(list:List<Category>):ArrayList<Category>
        {
            return ArrayList(list)
        }


        fun  arrayListToList(arrayList:ArrayList<Category>):List<Category>
        {
            return arrayList
        }

    }

    class ArrayListSubCategoryAdapter{

        fun  listToArrayList(list:List<SubCategory>):ArrayList<SubCategory>
        {
            return ArrayList(list)
        }


        fun  arrayListToList(arrayList:ArrayList<SubCategory>):List<SubCategory>
        {
            return arrayList
        }

    }


    fun <T> fromJson(value:String,clazz:Class<T>): T
    {
        return Gson().fromJson(value,clazz)!!
    }

    fun <T> toJson(value:T,clazz:Class<T>):String
    {
        return Gson().toJson(value,clazz)
    }
}