package com.aquall.app.utils.model

import com.aquall.app.repositary.model.Message


class ChatRoomMessage() {
    constructor(chatID:String,messageList:List<Message>):this()
    {
        this.chat_room_id = chatID
        this.message = messageList
    }
    var chat_room_id:String? = null
    var message:List<Message>? = null
}