package com.aquall.app.utils.enums

class NavScreen {
    open class Screen{
        fun isDashboard():Boolean
        {
            return this is Dashboard
        }

        fun isSearch():Boolean
        {
            return this is Search
        }

        fun isFavorite():Boolean
        {
            return this is Favorite
        }

        fun isProfile():Boolean
        {
            return this is Profile
        }

        fun isMessage():Boolean
        {
            return this is Messages
        }
    }
    class Dashboard: Screen()
    class Search:Screen()
    class Favorite:Screen()
    class Profile:Screen()
    class Messages:Screen()


}