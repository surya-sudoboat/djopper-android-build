package com.aquall.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static io.realm.Realm.getApplicationContext;

import com.aquall.app.utils.model.SearchKeys;

public class SharedPrefUtil{
    public static  SharedPrefUtil sharedPrefUtil;
    SharedPreferences pref;

    public static void init(Context context){
        sharedPrefUtil = new SharedPrefUtil();
        sharedPrefUtil.pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
    }
    public static SharedPrefUtil getInstance()
    {
      return sharedPrefUtil;

    }

    public void setUserId(String userId)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("userId",userId);
        edit.apply();
    }


    public String getUserId()
    {
        return pref.getString("userId", null);
    }


    public void setSettings(String settings)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("settings",settings);
        edit.apply();
    }

    public void setLocales(String locales)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("locales",locales);
        edit.apply();
    }
    public String getLocale()
    {
        return pref.getString("locale", "en");
    }
    public void setLocale(String locale)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("locale",locale);
        edit.apply();
    }


    public void setFirebaseToken(String fcmToken)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("fcmToken",fcmToken);
        edit.apply();
    }

    public String getFirebaseToken()
    {
        return pref.getString("fcmToken", null);
    }

    public void setUnreadCount(Integer count)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putInt("unReadCount",count);
        edit.apply();
    }

    public Integer getFavUnreadCount()
    {
        return pref.getInt("favUnReadCount", 0);
    }

    public void setFavUnreadCount(Integer count)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putInt("favUnReadCount",count);
        edit.apply();
    }

    public Integer getUnreadCount()
    {
        return pref.getInt("unReadCount", 0);
    }
    public String getSettings()
    {
        return pref.getString("settings", null);
    }
    public String getLocales()
    {
        return pref.getString("locales", null);
    }

    public List<SearchKeys> getSearchKeys(){
        String searchKeys = pref.getString("searchKeys", null);
        if (searchKeys==null)
            return null;
        return Arrays.asList(JsonUtil.INSTANCE.fromJson(searchKeys,SearchKeys[].class));
    }

    public void  setSearchKeys(List<SearchKeys> searchKeys){
        String searchKeysSerial = JsonUtil.INSTANCE.toJson(searchKeys.toArray(new SearchKeys[0]), SearchKeys[].class);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("searchKeys",searchKeysSerial);
        edit.apply();
    }

    public void deleteAll()
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.clear();
        edit.apply();
    }
}