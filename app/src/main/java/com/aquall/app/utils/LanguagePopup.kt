package com.aquall.app.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import androidx.core.app.DialogCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.adapters.LanguageListAdapter
import com.aquall.app.repositary.LabelRepositary
import com.aquall.app.viewholders.ClickListener

class LanguagePopup(context: Context,clickListener: ClickListener?=null): Dialog(context) {
    var mClickListener = clickListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.alert_language);
        val rvLanguage = findViewById<RecyclerView>(R.id.rvLanguage)
        val locales = Localizer.locales
        rvLanguage.layoutManager = LinearLayoutManager(rvLanguage.context)
        rvLanguage.adapter = locales.data?.let { LanguageListAdapter(it,object :ClickListener{
            override fun onClick(language: Localizer.Locale) {
                mClickListener?.onClick(language)
                dismiss()
            }
        }) }
    }


}