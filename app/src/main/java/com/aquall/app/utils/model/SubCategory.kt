package com.aquall.app.utils.model

import com.aquall.app.repositary.model.SubCategoryDB
import com.aquall.app.utils.JsonUtil

class SubCategory() {
    lateinit var _id:String
    var name:String? = null
    var sub_category_name:List<SubCategoryName>? = null

    data class SubCategoryName(val _id:String,val language:String,val value:String)

    public constructor(subCategory: SubCategoryDB):this()
    {
        this._id=subCategory._id
        this.name=subCategory.value
    }
    override fun toString(): String {
        return JsonUtil.toJson(this,SubCategory::class.java)
    }


    override fun equals(other: Any?): Boolean {
        if (other is SubCategory)
        {return _id.equals(other._id)}
        else{
            return false
        }
    }

    override fun hashCode(): Int {
        return _id.hashCode()
    }


}