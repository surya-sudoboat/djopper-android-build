package com.aquall.app.utils.model

import com.aquall.app.repositary.model.JobApplication
import com.aquall.app.repositary.model.Payment
import com.aquall.app.repositary.model.UserModel

class Job2 {
    var _id:String?=null
    var name:String?=null
    var start_date:String? = null
    var end_date:String? = null
    var description:String? = null
    var image:String?= null
    var email:String? = null
    var mobile:String? = null
    var location: Location? =null
    var is_on_hold=false
    var is_mobile_contactable = false
    var is_email_contactable  =false
    var is_active = false
    var is_favourite = false
    var job_owner_id:UserModel?=null
    var payment: Payment?= null
    var category: Category?=null
    var subcategory: SubCategory?=null
    var applied_count:Int =0
    var job_owner: UserModel?=null
    var job_application: JobApplication?=null
    var updatedAt:String?=null
    var key:String?=null
    var unread_count:Int =0
    var isRead:Boolean = true
    var job_application_id:String? = null
}