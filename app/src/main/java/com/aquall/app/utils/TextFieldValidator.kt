package com.aquall.app.utils

import android.text.TextUtils
import android.util.Patterns


class TextFieldValidator {
    companion object{
        fun isValidEmail(target: CharSequence?): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target!!).matches()
        }

        fun isValidPassword(target:CharSequence):Boolean
        {
            return !TextUtils.isEmpty(target)&&target.length>5
        }

        fun isValidMobile(target:CharSequence):Boolean
        {
            return !TextUtils.isEmpty(target)&&target.length>9
        }
    }
}