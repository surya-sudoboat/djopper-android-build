package com.aquall.app.utils

import androidx.lifecycle.MutableLiveData
import com.aquall.app.Localizer
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.LabelRepositary
import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.utils.model.Labels
import kotlinx.coroutines.flow.MutableStateFlow

object AppInitializer {
    var labels:MutableLiveData<LabelsDB> = MutableLiveData()



    fun getLabelClasses():MutableLiveData<LabelsDB>{
        return labels
    }


    interface  Callback{
        fun success()
    }

    fun init()
    {
        Localizer.locale = SharedPrefUtil.getInstance().locale
        if (Settings.getgooglePlaceAPIkey()==null) {
            AuthRepositary().getSettings()
        }
        labels = LabelRepositary().getLabels(Localizer.locale)
        LabelRepositary().getLocale()


    }

}