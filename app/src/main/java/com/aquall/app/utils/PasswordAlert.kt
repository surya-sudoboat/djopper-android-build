package com.aquall.app.utils

import android.content.Context
import android.text.InputType
import android.widget.EditText
import android.widget.Toast
import com.aquall.app.Localizer
import com.aquall.app.utils.enums.DataStatus

abstract class PasswordAlert {
    abstract fun success(password:String)
    abstract fun confirmPasswordWrong()
    abstract fun userCancelled()
    fun showPasswordAlert(context:Context)
    {
        val alert = android.app.AlertDialog.Builder(context)
        val edittext = EditText(context)
        edittext.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        alert.setMessage(null)
        alert.setTitle(Localizer.labels.password)
        alert.setView(edittext)
        alert.setPositiveButton(
            Localizer.labels.ok
        ) { dialog, whichButton -> //What ever you want to do with the value
            val YouEditTextValue = edittext.text.toString()
            dialog.cancel()
            showConfirmPassword(context,YouEditTextValue)

        }

        alert.setNegativeButton(
            Localizer.labels.cancel
        ) { dialog, whichButton ->
            // what ever you want to do with No option.
            dialog.cancel()
            userCancelled()
        }

        alert.show()
    }

    fun showConfirmPassword(context: Context,password:String)
    {
        val alert = android.app.AlertDialog.Builder(context)
        val edittext = EditText(context)
        edittext.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        alert.setMessage(null)
        alert.setTitle(Localizer.labels.confirm_your_password)
        alert.setView(edittext)
        alert.setPositiveButton(
            Localizer.labels.ok
        ) { dialog, whichButton -> //What ever you want to do with the value
            val YouEditTextValue = edittext.text.toString()
            dialog.cancel()
            if (password.equals(YouEditTextValue))
            {
                success(password)
            }else{
                confirmPasswordWrong()
            }

        }

        alert.setNegativeButton(
            Localizer.labels.cancel
        ) { dialog, whichButton ->
            dialog.cancel()
            // what ever you want to do with No option.
            userCancelled()
        }

        alert.show()


    }
}