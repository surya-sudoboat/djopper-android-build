package com.aquall.app.utils.model

import com.aquall.app.utils.JsonUtil
import com.google.gson.annotations.SerializedName


class SearchKeys {
     @SerializedName("name", alternate = ["jobTitle"])
     var name:String = ""
     var location:Location? = Location()
     var category:ArrayList<Category> = ArrayList()
     var subCategory:ArrayList<SubCategory> = ArrayList()
     var range:String?=null
override fun toString():String{
    return JsonUtil.toJson(this,SearchKeys::class.java)
}

    companion object{
        fun fromString(value:String):SearchKeys
        {
            return JsonUtil.fromJson(value,SearchKeys::class.java)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other is SearchKeys)
         return name.equals(other.name)
        else
            return false
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}