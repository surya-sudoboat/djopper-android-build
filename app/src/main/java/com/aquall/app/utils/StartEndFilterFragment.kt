package com.aquall.app.utils

import android.app.Dialog
import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.aquall.app.Localizer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.Duration.Companion.hours
import kotlin.time.DurationUnit

class StartEndFilterFragment(val onDateSet:(startDate:Date,endDate:Date)->Unit):BottomSheetDialogFragment() {
    lateinit var mfragmentManager:FragmentManager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mfragmentManager = requireActivity().supportFragmentManager
        return ComposeView(requireContext()).apply {
            setContent {
                    FilterScreen()
            }
        }
    }


    @Composable
    fun FilterScreen(){
        val DATE_FORMAT = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        var startDate by remember {
            mutableStateOf(Date())
        }
        var endDate by remember {
            mutableStateOf(Date())
        }


        val datePickerStart = rememberDatePickerDialog(
            select = startDate,
            title = "Start Date",
        ) { startDate = it
        }

        val datePickerEnd = rememberDatePickerDialog(
            select = endDate,
            bounds = CalendarConstraints.Builder().setValidator(
                DateValidatorPointForward.from(startDate.time)).build(),
            title = "End Date",
        ) {
            endDate = it
        }

        Column(Modifier.fillMaxSize().padding(10.dp,0.dp,10.dp,0.dp)) {
            Spacer(modifier = Modifier.height(10.dp))
            TextField(
                modifier = Modifier.fillMaxWidth(),
                readOnly = true,
                value = DATE_FORMAT.format(startDate),
                placeholder = {
                              Text(text = Localizer.labels.start_date_label!!)
                },
                onValueChange = { startDate = DATE_FORMAT.parse(it)!! },
                trailingIcon = {
                    IconButton({ datePickerStart.show(mfragmentManager, "Start Date") }) {
                        Icon(Icons.Default.DateRange,contentDescription = null)
                    }
                },
            )
            Spacer(modifier = Modifier.height(15.dp))
            TextField(
                modifier = Modifier.fillMaxWidth(),
                readOnly = true,
                value = DATE_FORMAT.format(endDate),
                placeholder = {
                    Text(text = Localizer.labels.end_date_label!!)
                },
                onValueChange = { endDate = DATE_FORMAT.parse(it)!! },
                trailingIcon = {
                    IconButton({ datePickerEnd.show(mfragmentManager, "End Date") }) {
                        Icon(Icons.Default.DateRange,contentDescription = null)
                    }
                },
            )
            Spacer(modifier = Modifier.height(10.dp))

            Button(modifier=Modifier.fillMaxWidth(), enabled = startDate<=endDate, onClick = {
                onDateSet(startDate,endDate)
                dismiss()
            }) {
                Text(text = Localizer.labels.set_filter!!)
            }
        }
    }


    @Composable
    fun rememberDatePickerDialog(
        title: String,
        select: Date? = null,
        bounds: CalendarConstraints? = null,
        onDateSelected: (Date) -> Unit = {},
    ): MaterialDatePicker<Long> {
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setSelection((select?.time
                    ?: Date().time) + 24.hours.toLong(DurationUnit.MILLISECONDS))
                .setCalendarConstraints(bounds)
                .setTitleText(title)
                .build()


        DisposableEffect(datePicker) {
            val listener = MaterialPickerOnPositiveButtonClickListener<Long> {
                if (it != null) onDateSelected(Date(it))
            }
            datePicker.addOnPositiveButtonClickListener(listener)
            onDispose {
                datePicker.removeOnPositiveButtonClickListener(listener)
            }
        }

        return datePicker
    }
}