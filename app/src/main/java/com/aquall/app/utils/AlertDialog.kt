package com.aquall.app.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.view.Window
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.aquall.app.Localizer
import com.aquall.app.R

class AlertDialog(context: Context) :androidx.appcompat.app.AlertDialog(context),LifecycleEventObserver{
     interface Listener{
        public fun success()
        public fun cancel()
    }


    companion object{
        private var mAlertDialog:AlertDialog? = null
        private var mListener:Listener? = null
        fun  show(context: Context?,text:String,showNegativeButton:Boolean=false,listener: Listener?=null,lifecycleOwner: LifecycleOwner?=null){
            if (context!=null) {
                mAlertDialog= AlertDialog(context)
                mListener = listener
                mAlertDialog?.setTitle(null)
                mAlertDialog?.setMessage(text)
                mAlertDialog?.setCancelable(false)
                mAlertDialog?.setButton(BUTTON_POSITIVE,Localizer.labels.ok, DialogInterface.OnClickListener
                { dialogInterface, i ->
                    mListener?.success()
                    dialogInterface.dismiss()
                })
                if (showNegativeButton) {
                    mAlertDialog?.setButton(
                        BUTTON_NEGATIVE,Localizer.labels.cancel, DialogInterface.OnClickListener
                    { dialogInterface, i ->
                        mListener?.cancel()
                        dialogInterface.dismiss()
                    })
                }
                lifecycleOwner?.lifecycle?.addObserver(mAlertDialog!!)
                mAlertDialog?.show()
            }

        }


        fun dismiss(){
            mAlertDialog?.dismiss()
        }
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        if (event == Lifecycle.Event.ON_PAUSE)
        {
           dismiss()
           source.lifecycle.removeObserver(this)
        }
    }
}