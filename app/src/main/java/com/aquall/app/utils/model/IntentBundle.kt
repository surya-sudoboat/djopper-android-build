package com.aquall.app.utils.model

import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.utils.JsonUtil

class IntentBundle {
    var screenType = 0
    var job:Job? = null
    var jobOfferId:String? = null
    var candidate:UserModel?= null
    var chatRoom:ChatRoom?=null
    var isCandidateClickable = false
    var takeToPayment = false;
    var paymentUrl:String?= null

    override fun toString():String{
        return JsonUtil.toJson(this,IntentBundle::class.java)
    }

    companion object{
        fun fromString(json:String):IntentBundle
        {
            return JsonUtil.fromJson(json,IntentBundle::class.java)
        }
    }
}