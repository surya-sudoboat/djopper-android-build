package com.aquall.app.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import androidx.lifecycle.*
import com.aquall.app.R


class ProgressDialog(context: Context): Dialog(context),LifecycleEventObserver{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_progress_dialog);
        setCancelable(false)
    }


    companion object{
        var progressDialog:ProgressDialog? = null

        fun show(lifecycleOwner: LifecycleOwner,context: Context?)
        {

            if (context!=null) {
                if (progressDialog==null)
                {
                    progressDialog = ProgressDialog(context!!)
                }
                lifecycleOwner.lifecycle.addObserver(progressDialog!!)
                try {
                        progressDialog?.show()
                } catch (e: Exception) {
                }
            }
        }

        fun hide()
        {

                progressDialog?.dismiss()

        }
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        if (event == Lifecycle.Event.ON_PAUSE)
        {
            this.dismiss()
            source.lifecycle.removeObserver(this)
        }
    }
}