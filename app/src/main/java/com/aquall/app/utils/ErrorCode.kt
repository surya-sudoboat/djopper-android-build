package com.aquall.app.utils

import android.content.Context
import android.widget.Toast
import com.android.volley.ClientError
import org.json.JSONObject

object ErrorCode {

    fun getMessage(ex:Exception,context: Context,defaultText:String?=null)
    {
        if (ex.cause is ClientError) {
            try {
                val localizedMessage =
                    String((ex.cause as ClientError).networkResponse.data)
                val jsonObject = JSONObject(localizedMessage)
                val errorJson = jsonObject.getJSONObject("error")
                val string = errorJson.getString("message")
                Toast.makeText(context,string, Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        else if (ex is MessageException)
        {
            Toast.makeText(context,ex.mCausingText, Toast.LENGTH_SHORT).show()

        }
        else{
            Toast.makeText(context,defaultText?:"Network Error", Toast.LENGTH_SHORT).show()

        }
    }
}