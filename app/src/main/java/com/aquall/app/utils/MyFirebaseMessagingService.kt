package com.aquall.app.utils

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.aquall.app.MainActivity
import com.aquall.app.R
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.repositary.service.JobService
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Job
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject
import kotlin.random.Random


class MyFirebaseMessagingService:FirebaseMessagingService() {

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Logger.e("Token Firebase: $p0")
        SharedPrefUtil.getInstance().firebaseToken = p0
        AuthRepositary().updateFCMToken(p0)
    }




    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        val data:Map<String,String> = p0.data
        val notification = p0.notification
        val notif_body = notification?.body
        val notif_channnel_id = notification?.channelId
        var mDataString:String =""
        for (key in data.keys)
        {
            mDataString = mDataString + key + "--->" + data[key] + ";"
        }
        Logger.e("Firebase Message: $mDataString")
        Logger.e("Firebase Notification text: $notif_body")
        val dataString = data["data"]
        try {
            val jsonObject = dataString?.let { JSONObject(it) }
            val jobOfferId = jsonObject?.getString("jobOfferId")
            if (jsonObject!=null&&jobOfferId!=null&&jsonObject.has("roomId")) {
                val senderFirstName = jsonObject.getString("senderFirstName")
                val roomId = jsonObject.getString("roomId")
                val senderLastName = if(jsonObject.has("senderLastName"))jsonObject.getString("senderLastName") else ""
                val senderImage = if(jsonObject.has("senderImage"))jsonObject.getString("senderImage") else ""
                val fullName = senderFirstName + senderLastName
                val jobData = JobService().getJobData(jobOfferId)
                val intentBundle = IntentBundle()
                intentBundle.job = if (jobData.data!=null)Job(jobData.data!!)else null
                val chatRoom = ChatRoom()
                chatRoom._id = roomId
                intentBundle.chatRoom = chatRoom
                val userModel = UserModel()
                userModel._id =""
                userModel.firstName = senderFirstName
                userModel.lastName = senderLastName
                userModel.image = senderImage
                intentBundle.candidate = userModel
                val userId = AuthRepositary.userId
                if (userId!=null&&jobData.data?.job_owner_id!=null&&userId.equals(jobData.data?.job_owner_id))
                {
                    intentBundle.screenType =0
                }else{
                    intentBundle.screenType =1
                }
                notif_channnel_id?.let { setNotificationn(notif_body, channel_id = it,ScreenName.MESSAGEDETAIL,intentBundle) }
            }
            
            else if(jsonObject!=null&&jobOfferId!=null){
//                val jobData = JobService().getJobData(jobOfferId)
                val intentBundle = IntentBundle()
                intentBundle.jobOfferId = jobOfferId
                notif_channnel_id?.let { setNotificationn(notif_body, channel_id = it,ScreenName.JOBDESCRIPTION,intentBundle) }
            }

        }catch (e:Exception)
        {
            e.printStackTrace()
        }

    }

    private fun setNotificationn(notif_body: String?,channel_id:String,screenName: ScreenName.Name,intentBundle: IntentBundle) {
        NotificationChannels.createNotificationChannel(this)
        NotificationChannels.createNotificationChannelSilent(this)
        var intent = Intent(this, MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent = intent.putExtra("screenName", screenName.getValue())
        intent = intent.putExtra("data",intentBundle.toString())
        val nextInt = Random.nextInt()
        val pendingIntent: PendingIntent = PendingIntent.getActivity(
            this,
            nextInt,
            intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        var builder = NotificationCompat.Builder(this, channel_id)
            .setSmallIcon(R.drawable.ic_splash_logo)
            .setContentTitle("Djopper")
            .setContentText(notif_body)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            if (ActivityCompat.checkSelfPermission(
                    this@MyFirebaseMessagingService,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            notify(100021, builder.build())
        }
    }




}