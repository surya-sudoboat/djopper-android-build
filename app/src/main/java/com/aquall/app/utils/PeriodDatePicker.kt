package com.aquall.app.utils

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import java.text.SimpleDateFormat
import java.util.*

class PeriodDatePicker(context: Context):DatePickerDialog.OnDateSetListener {
    var fromDatePicker:DatePickerDialog = DatePickerDialog(context)
    var fromDateString:String?=null
    var toDateString:String?=null
    var toDatePickerDialog = DatePickerDialog(context)
    interface OnDateSelected{
        fun onDate(from:String,to:String)
    }

    var mOnDateSelected:OnDateSelected?=null

    fun show()
    {
        fromDatePicker.setOnDateSetListener(this)
        fromDatePicker.setTitle("Job starting date")
        toDatePickerDialog.setOnDateSetListener(this)
        toDatePickerDialog.setTitle("Job ending date")
        val instance = Calendar.getInstance()
        instance.set(Calendar.HOUR,0)
        instance.set(Calendar.MINUTE,0)
        instance.set(Calendar.SECOND,0)
        instance.set(Calendar.MILLISECOND,0)
        fromDatePicker.datePicker.minDate = instance.timeInMillis
        fromDatePicker.show()
    }

    fun setOnDateSelected(onDateSelected: OnDateSelected)
    {
        this.mOnDateSelected = onDateSelected
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        if (p0==fromDatePicker.datePicker)
        {
            Logger.e("${p1}:${p2}:${p3}")
            fromDateString = getDate("${p1}/${p2+1}/${p3}")
            val instance = Calendar.getInstance()
            instance.set(p1,p2,p3)
            toDatePickerDialog.datePicker.minDate = instance.timeInMillis
            toDatePickerDialog.show()
        }

        if (p0==toDatePickerDialog.datePicker)
        {
            Logger.e("${p1}:${p2}:${p3}")
            toDateString = getDate("${p1}/${p2+1}/${p3}")
            if (mOnDateSelected!=null)
            {
                mOnDateSelected!!.onDate(fromDateString!!,toDateString!!)
            }
        }
    }

    fun getDate(dateStr: String):String {
        val formatter = SimpleDateFormat("yyyy/M/d", Locale.ENGLISH)
        val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val mDate = formatter.parse(dateStr) // this never ends while debugging
        return formatter2.format(mDate)
    }

}