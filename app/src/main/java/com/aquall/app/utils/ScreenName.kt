package com.aquall.app.utils

import android.util.SparseArray
import androidx.fragment.app.Fragment
import com.aquall.app.fragments.*

class ScreenName {
    companion object{
        var fragmentlist:HashMap<Int,Name> = HashMap()
    }
    abstract class Name() {
        init {
            fragmentlist.put(getValue(),this)
        }
        abstract fun getValue():Int
        abstract fun getFragment():Fragment
        var data:String? = null
        companion object {
            fun getFragmentFromValue(value: Int): Fragment? {
                return fragmentlist.get(value)?.getFragment()
            }
            fun getClassByValue(value:Int):Name?{
                return fragmentlist.get(value)
            }
        }

    }
    object LOGIN :Name(){
        override fun getValue():Int {
            return 1
        }
        override fun getFragment():Fragment
        {
            return LoginFragment()
        }
    }

    object REGISTER :Name(){
        override fun getValue():Int {
            return 2
        }
        override fun getFragment():Fragment
        {
            return RegisterFragment()
        }
    }
    object CATEGORYSELECTION:Name(){
        override fun getValue():Int {
            return 3
        }
        override fun getFragment():Fragment
        {
            return CategorySelectionFragment()
        }
    }
    object FORGOTPASSWORD:Name(){
        override fun getValue():Int {
            return 4
        }
        override fun getFragment():Fragment
        {
            return ForgotPasswordFragment()
        }
    }
    object JOBDESCRIPTION:Name(){
        override fun getValue():Int {
            return 5
        }
        override fun getFragment():Fragment
        {
            return JobDescriptionFragment()
        }
    }
    object JOBSEARCHRESULT:Name(){
        override fun getValue():Int {
            return 6
        }
        override fun getFragment():Fragment
        {
            return JobSearchResultFragment()
        }
    }
    object SEARCHJOB:Name(){
        override fun getValue():Int {
            return 7
        }
        override fun getFragment():Fragment
        {
            return SearchJobFragment()
        }
    }

    object DASHBOARD:Name(){
        override fun getValue():Int {
            return 8
        }
        override fun getFragment():Fragment
        {
            return DashboardFragment()
        }
    }

    object FAVORITELIST:Name(){
        override fun getValue():Int {
            return 9
        }
        override fun getFragment():Fragment
        {
            return FavoriteJobListFragment()
        }
    }
    object PROFILE:Name(){
        override fun getValue():Int {
            return 10
        }
        override fun getFragment():Fragment
        {
            return ProfileFragment()
        }
    }
    object MESSAGE:Name(){
        override fun getValue():Int {
            return 11
        }
        override fun getFragment():Fragment
        {
            return MessagesFragment()
        }
    }

    object APPLIEDJOBS:Name(){
        override fun getValue():Int {
            return 12
        }
        override fun getFragment():Fragment
        {
            return AppliedJobFragment()
        }
    }


    object EDITPROFILE:Name(){
        override fun getValue():Int {
            return 13
        }
        override fun getFragment():Fragment
        {
            return EditProfileFragment()
        }
    }

    object JOBCREATION:Name(){
        override fun getValue():Int {
            return 14
        }
        override fun getFragment():Fragment
        {
            return JobCreationFragment()
        }
    }

    object POSTEDJOBS:Name(){
        override fun getValue():Int {
            return 15
        }
        override fun getFragment():Fragment
        {
            return PostedJobFragment()
        }
    }


    object MESSAGEDETAIL:Name(){
        override fun getValue():Int {
            return 16
        }
        override fun getFragment():Fragment
        {
            return MessageDetailFragment()
        }
    }




    object CANDIDATELIST:Name(){
        override fun getValue():Int {
            return 17
        }
        override fun getFragment():Fragment
        {
            return CandidatesListFragment()
        }
    }


    object PAYMENTDETAIL:Name(){
        override fun getValue():Int {
            return 18
        }
        override fun getFragment():Fragment
        {
            return PaymentFragment()
        }
    }


    object EXPIREDJOBS:Name(){
        override fun getValue():Int {
            return 19
        }
        override fun getFragment():Fragment
        {
            return ExpiredJobFragment()
        }
    }

    object PAYMENTHISTORY:Name(){
        override fun getValue():Int {
            return 20
        }
        override fun getFragment():Fragment
        {
            return PaymentListFragment()
        }
    }

    object INVOICELIST:Name(){
        override fun getValue():Int {
            return 21
        }
        override fun getFragment():Fragment
        {
            return InvoiceFragment()
        }
    }
    object SUBCATEGORYFRAGMENTLIST:Name(){
        override fun getValue():Int {
            return 22
        }
        override fun getFragment():Fragment
        {
            return SubCategorySelectionFragment()
        }
    }
}