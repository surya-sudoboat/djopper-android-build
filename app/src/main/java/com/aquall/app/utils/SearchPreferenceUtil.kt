package com.aquall.app.utils

import com.aquall.app.utils.model.SearchKeys

class SearchPreferenceUtil {
    companion object{
        private var searchList:List<SearchKeys>
         get() {
            return SharedPrefUtil.getInstance().searchKeys?:ArrayList()
         }
        set(value){
            SharedPrefUtil.getInstance().searchKeys = value
        }
        fun addNewSearchKey(searchKeys: SearchKeys){
            val temp:HashSet<SearchKeys> = LinkedHashSet()
            temp.add(searchKeys)
            temp.addAll(searchList)
            searchList = ArrayList(temp)
        }

        fun getSearchKey(name:String):SearchKeys?{
            val foundKey = searchList.find {
                it.name.startsWith(name)
            }

            return foundKey
        }

        fun getAllKeyNames():List<String>{
            return searchList.map { it.name }
        }

    }

}