package com.aquall.app.utils

import android.util.Log
import java.lang.Exception

class Logger {

    companion object{
        private  val TAG = "Auquall"

        fun e(value:String){
            Log.e(TAG,value)
        }
        fun e(value:Exception){

            if (value.message!=null) {
                Log.e(TAG,value.message!!)
            }
            value.printStackTrace()

        }

        fun d(value: String)
        {
            Log.e(TAG,value)
        }

        fun i(value: String)
        {
            Log.i(TAG,value)
        }
    }


}