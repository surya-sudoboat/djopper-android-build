package com.aquall.app.utils.model

import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.UserModel
import com.google.gson.annotations.SerializedName

class Favorite {
    lateinit var _id:String
    var job_offer:Job?=null
    var chat_rooms:List<ChatRoom>?=null
    @SerializedName("is_unread_message", alternate = ["isRead"])
    var isRead:Boolean = true
    var unread_count=0
    var job_owner: UserModel?=null
}