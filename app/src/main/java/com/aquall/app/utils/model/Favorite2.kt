package com.aquall.app.utils.model

import com.aquall.app.repositary.model.ChatRoom
import com.google.gson.annotations.SerializedName

class Favorite2 {
    lateinit var _id:String
    var job_offer:Job2?=null
    var chat_rooms:List<ChatRoom>?=null
    @SerializedName("is_unread_message", alternate = ["isRead"])
    var isRead:Boolean = true
    var seen:Boolean = true
    var unread_count=0
}