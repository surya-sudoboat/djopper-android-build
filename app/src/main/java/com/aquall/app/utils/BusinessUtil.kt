package com.aquall.app.utils

import com.aquall.app.utils.model.RegistrationType
import java.util.Arrays
import java.util.Date
import java.util.concurrent.TimeUnit


class BusinessUtil {
    public fun isCompany(registrationType: RegistrationType.TYPE):Boolean{
        return registrationType == RegistrationType.COMPANY
    }
    val times: List<Long> = Arrays.asList(
        TimeUnit.DAYS.toMillis(365),
        TimeUnit.DAYS.toMillis(30),
        TimeUnit.DAYS.toMillis(1),
        TimeUnit.HOURS.toMillis(1),
        TimeUnit.MINUTES.toMillis(1),
        TimeUnit.SECONDS.toMillis(1)
    )
    val timesString = Arrays.asList("year", "month", "day", "hour", "minute", "second")

    fun toDuration(date: Date): String? {
        val duration =  Date().time-date.time
        val res = StringBuffer()
        for (i in 0 until times.size) {
            val current: Long = times.get(i)
            val temp = duration / current
            if (temp > 0) {
                res.append(temp).append(" ").append(timesString.get(i))
                    .append(if (temp != 1L) "s" else "").append(" ago")
                break
            }
        }
        return if ("" == res.toString()) "0 seconds ago" else res.toString()
    }
}