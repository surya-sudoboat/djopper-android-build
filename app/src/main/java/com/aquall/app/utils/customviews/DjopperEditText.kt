package com.aquall.app.utils.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

import android.content.res.TypedArray
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import com.aquall.app.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView


class DjopperEditText: LinearLayout,TextWatcher{

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet): super(context, attrs){
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    lateinit var ediText: EditText

    private fun init(attrs: AttributeSet?)
    {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.layout_djopper_edittext, this, true);
        val childAt = getChildAt(0) as ViewGroup
        val textTitle:TextView = childAt.getChildAt(0) as TextView
         ediText = childAt.getChildAt(1) as EditText


        if (attrs!=null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.DjopperEditText)
            val text = a.getText(R.styleable.DjopperEditText_android_text)
            if (text!=null) {
                ediText.setText(text)
            }
            val hint = a.getText(R.styleable.DjopperEditText_android_hint)
            if (hint!=null) {
                ediText.hint = hint
            }
            val title = a.getText(R.styleable.DjopperEditText_android_title)
            if (title!=null) {
                textTitle.text = title
            }

            val drawableLeft = a.getDrawable(R.styleable.DjopperEditText_android_drawableLeft)
            if (drawableLeft!=null) {
                ediText.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableLeft,null,null,null)
            }

            val drawableStart = a.getDrawable(R.styleable.DjopperEditText_android_drawableStart)
            if (drawableStart!=null) {
                ediText.setCompoundDrawablesRelativeWithIntrinsicBounds(drawableStart,null,null,null)
            }

            val drawablEnd = a.getDrawable(R.styleable.DjopperEditText_android_drawableEnd)
            if (drawablEnd!=null) {
                ediText.setCompoundDrawablesRelativeWithIntrinsicBounds(null,null,drawablEnd,null)
            }
            val drawablePadding = a.getDimension(R.styleable.DjopperEditText_android_drawablePadding,0f)
            if (drawablePadding>0) {
                ediText.compoundDrawablePadding=drawablePadding.toInt()
            }

            a.recycle()
            ediText.addTextChangedListener(this)
        }

    }


    public fun getText():CharSequence{
        return ediText.text
    }


    fun setError(err:String)
    {
        ediText.error = err
    }

    fun setPassword(){
        ediText.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
    }

    fun setSingleLine()
    {
        ediText.maxLines=1
        ediText.isSingleLine = true
    }

    fun setNumerical(){
        ediText.inputType = InputType.TYPE_CLASS_NUMBER
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun afterTextChanged(p0: Editable?) {
      ediText.setError(null)
    }


}