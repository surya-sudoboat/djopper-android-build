package com.aquall.app.utils;

import com.aquall.app.repositary.AuthRepositary;
import com.aquall.app.repositary.dao.MessageDAO;
import com.aquall.app.repositary.model.Message;
import com.aquall.app.repositary.model.MessageList;
import com.aquall.app.repositary.model.MessageListDB;
import com.aquall.app.repositary.service.ServiceUrl;
import com.aquall.app.utils.model.ChatRoomMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;

public class SocketClient {
    public static SocketClient socketClient;
    private Socket mSocket;
    private final MutableLiveData<MessageList> messageLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> unReadLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> unReadFavLiveData = new MutableLiveData<>();

    public MutableLiveData<MessageList> getMessageLiveData() {
        return messageLiveData;
    }
    public MutableLiveData<Integer> getUnReadLiveData() {
        return unReadLiveData;
    }
    public MutableLiveData<Integer> getUnReadFavLiveData() {
        return unReadFavLiveData;
    }

    public boolean isRunning(){
       return socketClient.mSocket!=null&&socketClient.mSocket.connected();
    }

    public static  SocketClient getSocketClient() {
        if (socketClient==null)
        {
            socketClient = new SocketClient();
        }
        try {
            if (socketClient.mSocket==null) {
                IO.Options opts = new IO.Options();
                opts.path = "/socket.io";
//                opts.transports = new String[]{WebSocket.NAME};
                String socket_url = ServiceUrl.INSTANCE.getSocket_URL();
                socketClient.mSocket = IO.socket(socket_url,opts);
                socketClient.listen();
            }


        } catch (Exception e) {
            e.printStackTrace();
            Logger.Companion.e(e.toString());
        }

        return socketClient;
    }


    public void connect()
    {
        try {
            if (!socketClient.mSocket.connected()) {
                socketClient.mSocket.connect();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendUserid()
    {
        try {
            JSONObject jsonObject = new JSONObject();
            String userId = AuthRepositary.Companion.getUserId();
            String token = AuthRepositary.Companion.getToken();
            jsonObject.put("_id",userId);
            jsonObject.put("jwt_token",token);
            socketClient.mSocket.emit("user",jsonObject);
            Logger.Companion.e("emitting socket event-> user : "+jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void listen()
    {

            socketClient.mSocket.on("error", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.Companion.e("error " + args[0].toString());

                }
            });

            socketClient.mSocket.on("connect", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.Companion.e("connected");
                    sendUserid();
                }
            });

            socketClient.mSocket.on("notification", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.Companion.e("notification socket event");
                    try{
                        JSONObject jsonObject = new  JSONObject(args[0].toString());
                        Integer unread_message_count = jsonObject.getInt("unread_message_count");
                        Integer unread_fav_job_count = jsonObject.getInt("unread_fav_job_count");
                        Logger.Companion.e("Unread message from notification : "+unread_message_count);
                        String receiver_id = jsonObject.getString("receiver_id");
                        unReadLiveData.postValue(unread_message_count);
                        unReadFavLiveData.postValue(unread_fav_job_count);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();

                    }
                }
            });



        socketClient.mSocket.on("jobAlert", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Logger.Companion.e("jobAlert socket event");
                try{
                    JSONObject jsonObject = new  JSONObject(args[0].toString());

                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }
            }
        });

            socketClient.mSocket.on("message", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    if (args.length>0) {
                        Logger.Companion.e("message "+args[0].toString());
                        try{
                            JSONObject jsonObject = new  JSONObject(args[0].toString());
                            String chat_room_id = jsonObject.getString("chat_room_id");
                            String message = jsonObject.getString("message");
                            Message message1 = JsonUtil.INSTANCE.fromJson(message, Message.class);
                            MessageListDB byPrimaryKey= new MessageDAO().getByPrimaryKey(chat_room_id);
                            if (byPrimaryKey!=null&&byPrimaryKey.getMessages()!=null) {
                                byPrimaryKey.getMessages().add(message1);
                                new MessageDAO().save(byPrimaryKey);
                                messageLiveData.postValue(new MessageList(byPrimaryKey));

                            }else{
                                MessageList chatRoomMessage = new MessageList();
                                chatRoomMessage.set_id(chat_room_id);
                                chatRoomMessage.setMessages(Collections.singletonList(message1));
                                new MessageDAO().save(new MessageListDB(chatRoomMessage));
                                messageLiveData.postValue(chatRoomMessage);
                            }

                            Integer unread_message_count = jsonObject.has("unread_message_count")?jsonObject.getInt("unread_message_count"):0;
                            unReadLiveData.postValue(unread_message_count);
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            });

    }
}
