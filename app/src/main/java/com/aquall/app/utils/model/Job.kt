package com.aquall.app.utils.model

import com.aquall.app.repositary.model.JobApplication
import com.aquall.app.repositary.model.JobDB
import com.aquall.app.repositary.model.Payment
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.utils.JsonUtil
import com.google.gson.annotations.SerializedName

open class Job(){
     var _id:String?=null
     var name:String?=null
     var start_date:String? = null
     var end_date:String? = null
     var description:String? = null
    var image:String?= null
     var email:String? = null
     var mobile:String? = null
    var location: Location? =null
    var is_on_hold=false
    var is_mobile_contactable = false
    var is_email_contactable  =false
    var is_active = false
    var is_favourite = false
     var job_owner_id:String?=null
    var payment:Payment?= null
     var category: Category?=null
     var subcategory: SubCategory?=null
    var applied_count:Int =0
    var job_owner: UserModel?=null
     var job_application: JobApplication?=null
    var updatedAt:String?=null
    var key:String?=null
    var unread_count:Int =0
    var isRead:Boolean = true
    var job_application_id:String? = null
    constructor(jobDB: JobDB):this()
    {
        this._id=jobDB._id
        this.name=jobDB.name
        this.start_date=jobDB.start_date
        this.end_date=jobDB.end_date
        this.description=jobDB.description
        this.email=jobDB.email
        this.mobile=jobDB.mobile
        this.location=Location(jobDB.locationDB)
        this.is_on_hold=jobDB.is_on_hold
        this.unread_count = jobDB.unread_count
        this.is_mobile_contactable=jobDB.is_mobile_contactable
        this.is_email_contactable=jobDB.is_email_contactable
        this.is_active=jobDB.is_active
        this.is_favourite=jobDB.is_favourite
        this.job_owner_id=jobDB.job_owner_id
        this.category=Category(jobDB.category)
        this.subcategory= SubCategory(jobDB.subcategory)
        this.applied_count = jobDB.applied_count
        this.job_owner = jobDB.job_owner
        this.job_application = jobDB.job_application
        this.image = jobDB.image
        this.job_application_id = jobDB.job_application_id

    }


    constructor(jobDB: Job2):this()
    {
        this._id=jobDB._id
        this.name=jobDB.name
        this.start_date=jobDB.start_date
        this.isRead = jobDB.isRead
        this.end_date=jobDB.end_date
        this.description=jobDB.description
        this.email=jobDB.email
        this.mobile=jobDB.mobile
        this.location=jobDB.location
        this.is_on_hold=jobDB.is_on_hold
        this.unread_count = jobDB.unread_count
        this.is_mobile_contactable=jobDB.is_mobile_contactable
        this.is_email_contactable=jobDB.is_email_contactable
        this.is_active=jobDB.is_active
        this.is_favourite=jobDB.is_favourite
        this.job_owner_id=jobDB.job_owner_id?._id
        this.category=jobDB.category
        this.subcategory= jobDB.subcategory
        this.applied_count = jobDB.applied_count
        this.job_owner = jobDB.job_owner_id
        this.job_application = jobDB.job_application
        this.image = jobDB.image
        this.job_application_id = jobDB.job_application_id
        this.updatedAt=jobDB.updatedAt
        this.payment = jobDB.payment
    }

    companion object{
        fun fromString(value:String):Job
        {
            return JsonUtil.fromJson(value,Job::class.java)
        }
    }

    override fun toString(): String {
        return JsonUtil.toJson(this,Job::class.java)
    }


    override fun hashCode(): Int {
        return _id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Job) {
            return _id?.equals(other._id)?:false
        }

        return false
    }
}