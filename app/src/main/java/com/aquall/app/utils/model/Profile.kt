package com.aquall.app.utils.model

import android.text.TextUtils
import com.aquall.app.repositary.model.CompanyProfile
import com.aquall.app.repositary.model.LoginDB
import com.aquall.app.utils.JsonUtil
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Profile() {
     var _id:String?=null
     var firstName:String?=null
     var lastName:String?=null
     var email:String?=null
     var mobile:String?=null
     var DOB:String?=null
     var location:Location?=null
     var gender:String?=null
     @Transient
     var password:String?=null
     var image:String? = null
     var dialCode:String? = null
     var countryCode:String?=null
     var profession:String? = null
     var preferred_category:ArrayList<Category>?=null
     var preferred_subcategory:ArrayList<SubCategory>?=null
     var is_notification_enabled:Boolean = true
     var is_notification_sound_enabled:Boolean = true
     var range:String?= null;
     var appliedJobsCount:Int=0
     var postedJobsCount:Int=0
     var favouriteCount:Int=0
     var isCompany:Boolean = false
    @Transient var companyProfile: CompanyProfile?=null

    constructor(loginDB: LoginDB) : this() {
        this._id=loginDB._id
        this.firstName=loginDB.firstName
        this.lastName=loginDB.lastName
        this.DOB = loginDB.DOB
        this.email = loginDB.email
        this.range = loginDB.range
        this.mobile = loginDB.mobile
        this.location= loginDB.location?.let { Location(it) }
        this.gender = loginDB.gender
        this.countryCode = loginDB.countryCode
        this.password = loginDB.password
        this.dialCode = loginDB.dialCode
        this.image = loginDB.image
        this.profession = loginDB.profession
        this.is_notification_enabled = loginDB.is_notification_enabled
        this.is_notification_sound_enabled = loginDB.is_notification_sound_enabled
        if (loginDB.preferred_category!=null) {
            this.preferred_category = ArrayList(loginDB.preferred_category!!.map { Category(it) })
        }
        if (loginDB.preferred_subcategory!=null) {
            this.preferred_subcategory = ArrayList(loginDB.preferred_subcategory!!.map{SubCategory(it)})
        }
        this.appliedJobsCount = loginDB.appliedJobsCount
        this.postedJobsCount = loginDB.postedJobsCount
        this.favouriteCount = loginDB.favouriteCount
        this.isCompany = loginDB.isCompany
        this.companyProfile = loginDB.companyProfile

    }


    public fun getFullName():String{
        val fname = if (!TextUtils.isEmpty(firstName)) firstName!! else ""
        val lname =  if (!TextUtils.isEmpty(lastName)) " $lastName" else ""
        val s = fname + lname
        return s
    }

    companion object {
        fun fromString(value: String): Profile {
            return JsonUtil.fromJson(value,Profile::class.java)
        }
    }


    fun getGenderString():String?{
        return gender?.replaceFirstChar {it-> it.uppercaseChar() }
    }

    fun getDateOfBirth():String? {
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val mDate = formatter.parse(DOB)
            return formatter2.format(mDate!!)
        } catch (e: Exception){
        }
        return DOB
    }


    fun getProfilePercent():Int
    {
        var percent = 0.0
        if (!TextUtils.isEmpty(getFullName())) percent +=10
        if (!TextUtils.isEmpty(DOB)) percent+=10
        if (!TextUtils.isEmpty(email)) percent+=10
        if (!TextUtils.isEmpty(mobile)) percent+=10
        if (location!=null) percent+=10
        if (!TextUtils.isEmpty(gender)) percent+=10
        if (!TextUtils.isEmpty(password)) percent+=10


        return Math.round((percent/70)*100).toInt()
    }

    override fun toString(): String {
        return  JsonUtil.toJson(this,Profile::class.java)
    }
}