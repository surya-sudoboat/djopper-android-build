package com.aquall.app.utils.model

import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.utils.JsonUtil

class Labels() {
     var _id : String?=null
     var banner_header : String?=null
     var banner_description : String?=null
     var job_search_placeholder : String?=null
     var select_location_placeholder : String?=null
     var search_button_text : String?=null
     var jobs_listed_count_text : String?=null
     var view_all_jobs : String?=null
     var top_categories : String?=null
     var sub_categories : String?=null
     var view_details_button : String?=null
     var banner_2_title : String?=null
     var banner_2_description : String?=null
     var banner_2_benefit_1 : String?=null
     var banner_2_benefit_2 : String?=null
     var banner_2_benefit_3 : String?=null
     var clients_text : String?=null
     var projects_text : String?=null
     var employees_text : String?=null
     var login_text : String?=null
     var jobs_text : String?=null
     var register_text : String?=null
     var all_job_text : String?=null
     var create_account_Text : String?=null
     var contact_us_text : String?=null
     var help_text : String?=null
     var privacy_text : String?=null
     var terms_text : String?=null
     var copy_rights_text : String?=null
     var category_text : String?=null
     var location_text : String?=null
     var search_job_text : String?=null
     var result_text : String?=null
     var load_more_text : String?=null
     var category_placeholder_text : String?=null
     var location_placeholder_text : String?=null
     var search_job_placeholder_text : String?=null
     var apply_job_btn_text : String?=null
     var similar_job_text : String?=null




    companion object{
        fun fromJson(json:String):Labels
        {
            return JsonUtil.fromJson(json, Labels::class.java)!!
        }
    }
}