package com.aquall.app.utils

import com.aquall.app.Localizer

class Settings {
    class DATA{
        var _id:String?=null
        var key:String?=null
        var value:String?=null
    }

    class SettingsData{
        var data:List<DATA>? = null
    }


    companion object{
        var mSettingsData:SettingsData? = null
        var mLocales:Localizer.LocaleData? = null

        fun getgooglePlaceAPIkey():String?{
            val settings = SharedPrefUtil.getInstance().settings
            if (settings!=null) {
                val fromJson = JsonUtil.fromJson(settings, SettingsData::class.java)
                if (fromJson!= null) {
                    mSettingsData = fromJson
                }
            }

            if (mSettingsData!=null&& mSettingsData?.data!=null) {
                for (data in mSettingsData!!.data!!)
                {
                    if (data.key.equals("googlePlaceAPIkey"))
                    {
                        return data.value
                    }
                }
            }

            return null

        }


        fun setSettings(settings:SettingsData)
        {
            mSettingsData = settings
            SharedPrefUtil.getInstance().settings  = JsonUtil.toJson(settings,SettingsData::class.java)
        }

        fun setLocales(locales:Localizer.LocaleData)
        {
            mLocales = locales
            SharedPrefUtil.getInstance().locales  = JsonUtil.toJson(locales,Localizer.LocaleData::class.java)

        }

    }


}