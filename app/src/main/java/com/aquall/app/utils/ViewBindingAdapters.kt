package com.aquall.app.utils

import android.view.View
import androidx.databinding.BindingAdapter
import com.aquall.app.utils.model.RegistrationType

class ViewBindingAdapters {

    @BindingAdapter("shouldBeVisibleCompany")
    fun setCustomText(view: View, item: RegistrationType.TYPE) {
        if (item is RegistrationType.COMPANY)
            view.visibility = View.VISIBLE
        else
            view.visibility = View.GONE

    }
}