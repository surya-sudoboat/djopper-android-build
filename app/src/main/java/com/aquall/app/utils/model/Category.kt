package com.aquall.app.utils.model

import com.aquall.app.repositary.model.CategoryDB
import com.aquall.app.utils.JsonUtil
import com.google.gson.annotations.SerializedName

class Category constructor() {
    data class CategoryName(val _id:String,val language:String,val value:String)
    lateinit var _id:String
    lateinit var name:String
    var sub_category_count:Int=0
    var category_name:List<CategoryName>? = null


    constructor(categoryDB: CategoryDB) : this() {
        _id=categoryDB._id
        name = categoryDB.name
        sub_category_count = categoryDB.sub_category_count

    }

    override fun toString(): String {
        return JsonUtil.toJson(this,Category::class.java)
    }

    override fun hashCode(): Int {
        return _id.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Category)
            return false
        return _id.equals(other._id)
    }
}