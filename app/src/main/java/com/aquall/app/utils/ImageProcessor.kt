package com.aquall.app.utils

import android.content.Context
import android.os.Build.VERSION.SDK_INT
import android.widget.ImageView
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.imageLoader
import coil.load
import coil.request.ImageRequest
import coil.size.Scale
import coil.transform.CircleCropTransformation
import coil.transform.RoundedCornersTransformation
import coil.transform.Transformation
import coil.util.CoilUtils
import okhttp3.OkHttpClient

class ImageProcessor {
    private lateinit var imageView: ImageView
    private  var transformation:Transformation?= null
    companion object{
        fun into(imageView: ImageView):ImageProcessor{
            val imageProcessor = ImageProcessor()
            imageProcessor.imageView=imageView

            return imageProcessor
        }



    }

    fun setRoundedCorners(leftTop:Float,leftBottom:Float,rightTop:Float,rightBottom:Float):ImageProcessor
    {

        transformation=RoundedCornersTransformation(leftTop,rightTop,leftBottom,rightBottom)
        return this
    }
    fun setCircleCrop():ImageProcessor
    {

        transformation=CircleCropTransformation()
        return this
    }

    fun setRoundedCorners(left:Float,right:Float):ImageProcessor{
        return this.setRoundedCorners(left,left,right,right)
    }

    fun loadResource(resource:Int)
    {
        this.imageView.load(resource, imageView.context.imageLoader, builder = {
            if (transformation!=null)
                this.transformations(transformation!!)
            this.scale(Scale.FIT)
        })
    }


    fun loadResource(resource:String,placeholder:Int?=null)
    {
        this.imageView.load(resource, imageView.context.imageLoader, builder = {
            if (transformation!=null)
                this.transformations(transformation!!)
            if (placeholder!=null) {
                this.placeholder(placeholder)
                this.error(placeholder)
            }
            this.scale(Scale.FIT)
        })
    }
}