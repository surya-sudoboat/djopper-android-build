package com.aquall.app.utils.model

class RegistrationType {
    open class TYPE
    object COMPANY:TYPE()
    object INDIVIDUAL:TYPE()
}