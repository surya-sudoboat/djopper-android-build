package com.aquall.app.utils.model

import com.aquall.app.repositary.model.LocationDB
import io.realm.RealmList
class Location() {
   var type:String? =null
   var name:String? = null
   var coordinates: List<Double>? = null

    constructor(locationDB: LocationDB):this()
    {

        if (locationDB.coordinates!=null&& locationDB.coordinates!!.size>0) {
            this.coordinates = locationDB.coordinates
        }
        this.name = locationDB.name
        this.type = locationDB.type
    }



}