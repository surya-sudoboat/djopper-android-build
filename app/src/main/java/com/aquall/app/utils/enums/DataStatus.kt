package com.aquall.app.utils.enums

object DataStatus {

       public val LOADING = 0
        public val FAILED = 1
        public val DATA_AVAILABLE = 2

}