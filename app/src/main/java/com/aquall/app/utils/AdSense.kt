package com.aquall.app.utils

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.google.android.gms.ads.*
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions

object AdSense {
    val mMobileAds  = ArrayList<NativeAd>()
    fun init(context: Context,lifeCycleAware:LifecycleOwner){
        MobileAds.initialize(context) {
            Logger.e(it.toString())
        }
//        val adLoader = AdLoader.Builder(context, "ca-app-pub-3940256099942544/2247696110")
//            .forNativeAd { ad : NativeAd ->
//                // Show the ad.
//                if (lifeCycleAware.lifecycle.currentState==Lifecycle.State.DESTROYED)
//                {
//                    ad.destroy()
//                    return@forNativeAd
//                }
//                mMobileAds.add(ad)
//
//            }
//            .withAdListener(object : AdListener() {
//                override fun onAdFailedToLoad(adError: LoadAdError) {
//                    // Handle the failure by logging, altering the UI, and so on.
//                }
//            })
//            .withNativeAdOptions(
//                NativeAdOptions.Builder()
//                // Methods in the NativeAdOptions.Builder class can be
//                // used here to specify individual options settings.
//                .build())
//            .build()
//        adLoader.loadAds(AdRequest.Builder().build(), 5)
    }

}