package com.aquall.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aquall.app.repositary.ChatRepositary
import com.aquall.app.repositary.model.Message
import com.aquall.app.repositary.model.MessageList
import com.aquall.app.utils.model.Profile

class ChatViewModel() : ViewModel() {

    lateinit var chatRepositary:ChatRepositary
    fun getChat(jobId:String):LiveData<MessageList>
    {
         chatRepositary = ChatRepositary()
       return chatRepositary.fetchChat(jobId)
    }

    fun sendChat(chatId:String,message:String)
    {
        chatRepositary?.sendchat(chatId,message)
    }




}