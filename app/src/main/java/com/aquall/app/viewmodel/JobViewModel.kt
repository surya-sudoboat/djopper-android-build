package com.aquall.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aquall.app.repositary.JobRepositary
import com.aquall.app.repositary.model.*
import com.aquall.app.utils.model.Job
import com.aquall.app.utils.model.JobsList
import com.aquall.app.utils.model.SearchKeys
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class JobViewModel:ViewModel() {
    private var mLiveData:MutableLiveData<JobsList> = MutableLiveData()
    private var mLiveDataAppliedJob:MutableLiveData<List<Job>> = MutableLiveData()
    private var mLiveDataUserModel:MutableLiveData<List<CandidateModel>> = MutableLiveData()
    private var jobRepositary:JobRepositary = JobRepositary()
    private var mJobLiveData:MutableLiveData<Job>?=null
    private var mPaymentLiveData:MutableLiveData<String>?=null

    fun getJobList():LiveData<JobsList>
    {
        return mLiveData
    }
    fun search(searchKeys: SearchKeys)
    {
       jobRepositary.fetchJobs(searchKeys, mLiveData)
    }

    fun getFavorite()
    {
        jobRepositary.fetchFavorites(mLiveData)
    }

    fun getAppliedJobsChat()
    {
        jobRepositary.fetchAppliedJobChat(mLiveData)
    }

    fun getAppliedJobs():LiveData<List<Job>>
    {
        jobRepositary.fetchAppliedJob(mLiveDataAppliedJob)
        return mLiveDataAppliedJob
    }

    fun createChat(jobApplicationId:String):LiveData<ChatRoom>
    {
        val chatroomLiveData = MutableLiveData<ChatRoom>()
        jobRepositary.createChat(jobApplicationId, chatroomLiveData)
        return chatroomLiveData
    }


    fun getPostedJobsChat()
    {
        jobRepositary.fetchPostedJobChat(mLiveData)
    }

    fun getPostedJobs()
    {
        jobRepositary.fetchPostedJob(mLiveData)
    }

    fun getExpiredJobs()
    {
        jobRepositary.fetchExpiredJob(mLiveData)
    }
    fun getNetworkData():LiveData<Int>
    {
        return jobRepositary.networkStatus
    }

    fun markAsFavorite(jobId:String)
    {
        JobRepositary().addAsFavorite(jobId)
    }

    fun removeFavorite(jobId: String)
    {
        JobRepositary().removeFavorite(jobId)
    }

    fun deleteJob(jobId: String):LiveData<Int>
    {
        val jobRepositary1 = JobRepositary()
        jobRepositary1.deleteJob(jobId)
        return jobRepositary1.networkStatus
    }
    fun deleteJobApplication(jobId: String):LiveData<Int>
    {

        val jobRepositary1 = JobRepositary()
        jobRepositary1.deleteJobApplication(jobId)
        return jobRepositary1.networkStatus
    }

    fun holjobApplication(jobId: String,is_on_hold:Boolean =false):LiveData<Int>
    {

        val jobRepositary1 = JobRepositary()
        jobRepositary1.holdJobOffer(jobId,is_on_hold)
        return jobRepositary1.networkStatus
    }

    fun applyJob(jobId: String):LiveData<JobApply>
    {
        val liveData = MutableLiveData<JobApply>()
        jobRepositary.applyJob(jobId, liveData)
        return liveData
    }

    fun createJob(jobreq: CreateJobRequest,fileName:String?=null,fileData:ByteArray?=null,isEdit:Boolean=false,jobId:String?=null):LiveData<Job>
    {
        mJobLiveData  = MutableLiveData()
        jobRepositary.createJob(jobreq , mJobLiveData!!,fileName,fileData,isEdit,jobId)
        return mJobLiveData!!
    }

    fun getJobDescription(jobId: String):LiveData<Job>
    {
        mJobLiveData = MutableLiveData()
        jobRepositary.getJobData(jobId,mJobLiveData!!)
        return mJobLiveData!!
    }
    fun paymentDetail(jobId: String):LiveData<String>
    {
        mPaymentLiveData = MutableLiveData()
        JobRepositary().createPayment(jobId,mPaymentLiveData!!)
        return mPaymentLiveData!!
    }

    fun getCandidates(jobId: String):LiveData<List<CandidateModel>>
    {
        jobRepositary.fetchCandidates(jobId,mLiveDataUserModel)
        return mLiveDataUserModel
    }

    fun getLatestError():Exception
    {
        return jobRepositary.latestError
    }

    val paymentLiveData = MutableLiveData<List<Payments>>()
    fun getPaymentHistory(
        startDate:Date = Calendar.getInstance().apply {add(Calendar.DAY_OF_MONTH,-20)}.time,
        endDate:Date=Date()):LiveData<List<Payments>>{
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        jobRepositary.getPaymentList(simpleDateFormat.format(startDate),simpleDateFormat.format(endDate), paymentLiveData)
        return paymentLiveData
    }

    val invoiceListLiveData = MutableLiveData<List<InvoiceDetail>>()
    fun getInvocieList():LiveData<List<InvoiceDetail>>
    {
        jobRepositary.getInvoiceList(invoiceListLiveData)
        return invoiceListLiveData
    }

    val pdfStringBase64 = MutableLiveData<InvoicePdfResponse>()
    fun getInvoiceList(startDate:String):LiveData<InvoicePdfResponse>
    {
        jobRepositary.getPdf(startDate,startDate,pdfStringBase64)
        return pdfStringBase64
    }
    fun getInvoiceList(startDate:String,endDate:String):LiveData<InvoicePdfResponse>
    {
        jobRepositary.getPdf(startDate,endDate,pdfStringBase64)
        return pdfStringBase64
    }




}