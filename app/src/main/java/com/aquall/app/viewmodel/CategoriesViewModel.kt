package com.aquall.app.viewmodel

import androidx.lifecycle.*
import com.aquall.app.repositary.CategoryRepositary
import com.aquall.app.repositary.JobRepositary
import com.aquall.app.utils.Logger
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SearchKeys
import com.aquall.app.utils.model.SubCategory

class CategoriesViewModel: ViewModel() {
    private var categoryList = CategoryLiveData()
    private var subCategoryList= SubCategoryLiveData()


    fun getCategories():CategoryLiveData{
        CategoryRepositary().getCategories(categoryList)
        return categoryList
    }

    fun getSubCategories(categoryId:String):LiveData<Int>
    {
        Logger.Companion.e("CATEGORYVIEWMODEL getsubcategories")
        val categoryRepositary = CategoryRepositary()
        categoryRepositary.getSubCategories(categoryId,subCategoryList)
        return categoryRepositary.networkStatus
    }



    fun getSubCategories(categoryId:List<String>)
    {
        CategoryRepositary().getSubCategories(categoryId,subCategoryList)
    }

    fun getSubCategories():SubCategoryLiveData
    {
        return subCategoryList
    }
 interface CategoryObserver{
    fun onCategoriesChanged(value:List<Category>)
 }
class CategoryLiveData:MutableLiveData<List<Category>>(),Observer<List<Category>>{
    lateinit var mCategoryObserver:CategoriesViewModel.CategoryObserver
    fun observe(lifecycleOwner: LifecycleOwner,categoryObserver: CategoryObserver)
    {
        this.observe(lifecycleOwner, this)
        mCategoryObserver = categoryObserver
    }

    override fun onChanged(t: List<Category>?) {
        if (t!=null) {
            mCategoryObserver.onCategoriesChanged(t)
        }
    }
}

    interface SubCategoryObserver{
        fun onSubCategoriesChanged(value:List<SubCategory>)
    }
    class SubCategoryLiveData:MutableLiveData<List<SubCategory>>(),Observer<List<SubCategory>>{
        lateinit var mCategoryObserver:CategoriesViewModel.SubCategoryObserver
        fun observe(lifecycleOwner: LifecycleOwner,categoryObserver: SubCategoryObserver)
        {
            this.observe(lifecycleOwner, this)
            mCategoryObserver = categoryObserver
        }

        override fun onChanged(t: List<SubCategory>?) {
            if (t!=null) {
                mCategoryObserver.onSubCategoriesChanged(t)
            }
        }

    }


    fun getRecentSearches():LiveData<List<SearchKeys>>{
        val mutableLiveData = MutableLiveData<List<SearchKeys>>()
        JobRepositary().getRecentSearch(mutableLiveData)
        return mutableLiveData
    }

}

