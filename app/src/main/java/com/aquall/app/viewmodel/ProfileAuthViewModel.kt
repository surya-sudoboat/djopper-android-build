package com.aquall.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.model.LoginDB
import com.aquall.app.utils.model.Profile

class ProfileAuthViewModel() : ViewModel() {
    private var profile: MutableLiveData<Profile> = MutableLiveData()


    fun getProfile():LiveData<Profile>
    {
        AuthRepositary().getProfile(profile)
        return profile
    }




}