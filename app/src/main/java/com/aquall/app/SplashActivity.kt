package com.aquall.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.ChatRepositary
import com.aquall.app.repositary.JobRepositary
import com.aquall.app.repositary.model.ChatRoom
import com.aquall.app.repositary.model.LabelsDB
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.repositary.service.JobService
import com.aquall.app.utils.AppInitializer
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.SharedPrefUtil
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.model.IntentBundle
import com.aquall.app.utils.model.Labels
import kotlinx.coroutines.*
import org.json.JSONObject

class SplashActivity : AppCompatActivity() ,Observer<LabelsDB>{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        CoroutineScope(Dispatchers.Main).launch { gotoNextPage() }
    }
    var labels:LiveData<LabelsDB>? = null;
    suspend fun gotoNextPage(){
        delay(1000)
        AppInitializer.init()
        labels = AppInitializer.labels
        labels?.observe(this,this)
    }

    override fun onChanged(t: LabelsDB?) {
        if (t!=null)
        {
            labels?.removeObserver(this)
            val notificationData = getIntent().getStringExtra("data")
            if (notificationData!=null)
            {
                getDataAndSendIntent(notificationData)
            }else{
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }

        }
    }

    fun  getDataAndSendIntent(data:String)
    {
        CoroutineScope(Dispatchers.IO).launch {
            var intentBundle:IntentBundle?=null
            var ischatNotification = false

            try {
                val jsonObject = JSONObject(data)
                val jobOfferId = jsonObject.getString("jobOfferId")
                if (jsonObject.has("roomId")) {
                    ischatNotification =true
                    val senderFirstName = jsonObject.getString("senderFirstName")
                    val roomId = jsonObject.getString("roomId")
                    val senderLastName =
                        if (jsonObject.has("senderLastName")) jsonObject.getString("senderLastName") else ""
                    val senderImage =
                        if (jsonObject.has("senderImage")) jsonObject.getString("senderImage") else ""
                    val fullName = senderFirstName + senderLastName
                    val jobData = JobService().getJobData(jobOfferId)
                    intentBundle = IntentBundle()
                    intentBundle.job = com.aquall.app.utils.model.Job( jobData.data!!)
                    val chatRoom = ChatRoom()
                    chatRoom._id = roomId
                    intentBundle.chatRoom = chatRoom
                    val userModel = UserModel()
                    userModel._id = ""
                    userModel.firstName = senderFirstName
                    userModel.lastName = senderLastName
                    userModel.image = senderImage
                    intentBundle.candidate = userModel
                    val userId = AuthRepositary.userId
                    if (userId != null && jobData.data?.job_owner_id != null && userId.equals(jobData.data?.job_owner_id)) {
                        intentBundle.screenType = 0
                    } else {
                        intentBundle.screenType = 1
                    }
                    if (roomId!=null) {
                        ChatRepositary().fetchChat(roomId)
                    }
                }else if(jobOfferId!=null){
                    val jobData = JobService().getJobData(jobOfferId)
                    intentBundle = IntentBundle()
                    intentBundle.job = com.aquall.app.utils.model.Job(jobData.data!!)


                }
            } catch (e: Exception) {
            }
            CoroutineScope(Dispatchers.Main).launch {
                var intent = Intent(this@SplashActivity, MainActivity::class.java)
                if (intentBundle!=null) {
                    intent.putExtra("screenName", if(!ischatNotification)ScreenName.JOBDESCRIPTION.getValue() else ScreenName.MESSAGEDETAIL.getValue())
                    intent = intent.putExtra("data", intentBundle.toString())
                }
                startActivity(intent)
                finish()
            }
        }
    }

}