package com.aquall.app.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.repositary.model.Message
import com.aquall.app.viewholders.MessageIncomingViewHolder
import com.aquall.app.viewholders.MessageOutgoingViewHolder

class MessageListAdapter(messages:List<Message>,profileId:String,screenType:Int): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var mMessageList = messages
    var mProfileId = profileId
    var mScreenType = screenType

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       if (viewType ==1)
       {
           return MessageOutgoingViewHolder(parent)
       }else{
           return MessageIncomingViewHolder(parent)
       }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       if (holder is MessageIncomingViewHolder)
       {
           (holder as MessageIncomingViewHolder).bind(mMessageList[holder.layoutPosition])
       }

        if (holder is MessageOutgoingViewHolder)
        {
            (holder as MessageOutgoingViewHolder).bind(mMessageList[holder.layoutPosition])
        }
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mMessageList.get(position).sender_id.equals(mProfileId)) {
            0
        }
        else{
            1
        }
    }
}