package com.aquall.app.adapters

import android.content.Context
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.R
import com.aquall.app.viewholders.JobSearchHorizontalViewHolder

class JobSearchKeyListAdapter(value:List<String>):RecyclerView.Adapter<JobSearchHorizontalViewHolder> (){
      var mValue = value
   companion object {
       fun setAdapter(recyclerView: RecyclerView,value: List<String>){
           recyclerView.layoutManager = LinearLayoutManager(recyclerView.context,RecyclerView.HORIZONTAL,false)
           recyclerView.adapter = JobSearchKeyListAdapter(value)
       }
   }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): JobSearchHorizontalViewHolder {
       return JobSearchHorizontalViewHolder(parent);
    }

    override fun onBindViewHolder(holder: JobSearchHorizontalViewHolder, position: Int) {
         holder.bind(mValue.get(holder.adapterPosition))
    }

    override fun getItemCount(): Int {
       return mValue.size
    }

}