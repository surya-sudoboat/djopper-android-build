package com.aquall.app.adapters

import android.content.Context
import android.text.TextUtils
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.utils.model.Job
import com.aquall.app.viewholders.*

class JobListAdapter(needLastEmptyCell: Boolean,isMessageAdapter:Boolean,isBigviewHolder:Boolean=false) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    var mIteractionListener:JobListViewHolder.IteractionListener?=null
    var mIteractionListenerBig:JobListViewHolderBig.IteractionListener?=null
    var mNeedLastEmptyCell = needLastEmptyCell
    var mIsBigViewHolder:Boolean =isBigviewHolder
    var mIsMessageAdapter:Boolean = isMessageAdapter
     var jobs:ArrayList<Job> = ArrayList<Job>()
    var tempJobs:ArrayList<Job> = ArrayList<Job>()
    fun filter(searchKey:String?):JobListAdapter
    {
        if (TextUtils.isEmpty(searchKey))
        {
            tempJobs = jobs
            return this
        }
        else{
            tempJobs = ArrayList(jobs.filter {
               it.name?.lowercase()?.contains(searchKey!!.lowercase())?:false
            })
            return this
        }

    }

    fun remove(job:Job){
        jobs.remove(job)
        tempJobs.remove(job)
        notifyDataSetChanged()
    }

    fun getItem(position: Int):Job
    {
        return tempJobs.get(position)
    }

    fun updateAdapter(job:List<Job>)
    {
        this.jobs = ArrayList(job)
        this.tempJobs = ArrayList(job)
        notifyDataSetChanged()
    }
    companion object{
        fun setAdapter(recyclerView: RecyclerView,jobs:List<Job>,
                       iteractionListener: JobListViewHolder.IteractionListener?=null,
                       needLastEmptyCell:Boolean=false,isMessageAdapter:Boolean=false):JobListAdapter
        {
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            val jobListAdapter = JobListAdapter(needLastEmptyCell,isMessageAdapter,false)
            jobListAdapter.jobs = ArrayList(jobs)
            jobListAdapter.tempJobs = ArrayList(jobs)
            recyclerView.adapter= jobListAdapter
            jobListAdapter.mIteractionListener = iteractionListener
            return jobListAdapter
        }
        fun setAdapter(recyclerView: RecyclerView,jobs:List<Job>,
                       iteractionListenerBig:JobListViewHolderBig.IteractionListener?=null,
                       needLastEmptyCell:Boolean=false,isMessageAdapter:Boolean=false):JobListAdapter
        {
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            val jobListAdapter = JobListAdapter(needLastEmptyCell,isMessageAdapter,true)
            jobListAdapter.jobs = ArrayList(jobs)
            jobListAdapter.tempJobs = ArrayList(jobs)
            recyclerView.adapter= jobListAdapter
            jobListAdapter.mIteractionListenerBig = iteractionListenerBig
            return jobListAdapter
        }


        fun getAdapter(jobs:List<Job>,iteractionListener: JobListViewHolder.IteractionListener?=null, needLastEmptyCell:Boolean=false,isMessageAdapter:Boolean=false):JobListAdapter
        {
            val jobListAdapter = JobListAdapter(needLastEmptyCell,isMessageAdapter)
            jobListAdapter.mIteractionListener = iteractionListener
            jobListAdapter.jobs = ArrayList(jobs)
            jobListAdapter.tempJobs = ArrayList(jobs)
            return jobListAdapter
        }


        var EMPTYVIEWHOLDER = 0
        var JOBVIEWHOLDER =1
        var BIGJOBVIEWHOLDER =2
        var ADSENSEVIEWHOLDER=3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType==0)
            return EmptyVerticalViewHolder(parent,70)

        if (viewType==2)
        {return JobListViewHolderBig.getInstance(parent)}

        if (viewType== ADSENSEVIEWHOLDER)
        {return AdSenseViewHolder.getInstance(parent)}

        return if (mIsMessageAdapter) JobMessageListViewHolder.getInstance(parent) else JobListViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is JobListViewHolder)
        {
            holder.bind(job = tempJobs.get(holder.layoutPosition), iteractionListener =  mIteractionListener)
        }
        if (holder is JobListViewHolderBig)
        {
            holder.bind(job = tempJobs.get(holder.layoutPosition), iteractionListener =  mIteractionListenerBig)
        }
        else if (holder is JobMessageListViewHolder)
        {
            holder.bind(tempJobs.get(holder.layoutPosition),mIteractionListener)
        }
    }

    override fun getItemViewType(position:Int):Int
    {
        if (position==tempJobs.size&&mNeedLastEmptyCell)
        {
            return EMPTYVIEWHOLDER
        }
        else{

            return if(tempJobs.get(position)._id==null)ADSENSEVIEWHOLDER else if(mIsBigViewHolder) BIGJOBVIEWHOLDER else JOBVIEWHOLDER
        }
    }

    fun removeFavorites(position:Int):JobListAdapter
    {
        jobs.removeAt(position)
        tempJobs.removeAt(position)
        return this
    }

    override fun getItemCount(): Int {
        return if(mNeedLastEmptyCell)tempJobs.size+1 else tempJobs.size
    }


    fun search(searchValue:String)
    {
        if (searchValue.length>0) {
           tempJobs = ArrayList(  tempJobs.filter { it.name?.lowercase()?.contains(searchValue.lowercase())?:false })
        }
        else{
            tempJobs = jobs
        }
        notifyDataSetChanged()

    }

}