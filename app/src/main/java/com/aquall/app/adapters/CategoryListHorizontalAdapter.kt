package com.aquall.app.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.utils.model.Category
import com.aquall.app.viewholders.CategoryListViewHolder

class CategoryListHorizontalAdapter : RecyclerView.Adapter<CategoryListViewHolder>() {
    val mCategoryList = ArrayList<Category>()
    var mItemClicked:CategoryListViewHolder.ItemClicked?=null;
    companion object{

        fun setAdapter(
            recyclerView: RecyclerView,
            categoryList:List<Category>,
            itemClicked: CategoryListViewHolder.ItemClicked?):CategoryListHorizontalAdapter{
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context,RecyclerView.HORIZONTAL,false)
            val categoryListHorizontalAdapter = CategoryListHorizontalAdapter()
            categoryListHorizontalAdapter.mCategoryList.addAll(categoryList)
            if (itemClicked!=null) {
                categoryListHorizontalAdapter.setItemClickedListener(itemClicked)
            }
            recyclerView.adapter = categoryListHorizontalAdapter
            return categoryListHorizontalAdapter
        }
    }

    fun setItemClickedListener(itemClicked: CategoryListViewHolder.ItemClicked)
    {
        mItemClicked = itemClicked
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryListViewHolder {
        val categoryListViewHolder = CategoryListViewHolder(parent)
        mItemClicked?.let { categoryListViewHolder.setItemClickedListener(it) }
        return categoryListViewHolder
    }



    override fun onBindViewHolder(holder: CategoryListViewHolder, position: Int) {
        if (holder.layoutPosition>=0) {
            holder.bind(mCategoryList[holder.layoutPosition])
        }
    }


    override fun getItemCount(): Int {
        return mCategoryList.size;
    }
}