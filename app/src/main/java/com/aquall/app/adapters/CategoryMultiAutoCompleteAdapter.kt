package com.aquall.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import com.aquall.app.R
import com.aquall.app.utils.model.Category
import com.aquall.app.utils.model.SubCategory

fun getSample():List<String>
{
    val list = ArrayList<String>()
    list.add("Sub Category 1")
    list.add("Sub Category 2")
    list.add("Sub Category 3")
    list.add("Sub Category 4")
    return list
}
class CategoryMultiAutoCompleteAdapter constructor(context: Context,subArray:List<Category>?=null): ArrayAdapter<String>(context,
    R.layout.item_subcategory_multiselect, subArray?.map { it.name }?: getSample()) {
    var selected:ArrayList<String> = ArrayList();
    var mSubArray = subArray
    var selectedPositions:ArrayList<Category> = ArrayList()
    var changeListener:onChangeListener?=null;

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflate = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_subcategory_multiselect, parent, false)
        val findViewById = inflate.findViewById<CheckBox>(R.id.checkbox)
        val item = getItem(position)
        findViewById.setText(item)
        findViewById.isFocusableInTouchMode = false
        for (itemString in selected)
        {
            if (findViewById.text.equals(itemString))
                findViewById.isChecked = true
        }

        findViewById.setOnClickListener { compoundButton ->
            val checkBox = compoundButton as CheckBox
            val position1 = getPosition(checkBox.text.toString())
            if (!selected.contains(checkBox.text.toString()))
            {
                selected.add(checkBox.text.toString())
                val get = mSubArray?.get(position1)
                selectedPositions.add(get!!)
            }
            else{
                selected.remove(checkBox.text.toString())
                val get = mSubArray?.get(position1)
                selectedPositions.remove(get!!)

            }
            var concatenatedString:String = ""
            var i=0
            while ( i<selected.size){
                concatenatedString+=selected.get(i)
                if (i!=selected.size-1)
                    concatenatedString+=","
                i++
             }
            changeListener?.onAutocompleteSelect(concatenatedString,selectedPositions)
        }
        return inflate
    }

    fun setOnChangeListener(onChangeListener: onChangeListener)
    {
        this.changeListener=onChangeListener
    }

    interface onChangeListener{
        fun onAutocompleteSelect(value:String,positions:ArrayList<Category>);
    }




}