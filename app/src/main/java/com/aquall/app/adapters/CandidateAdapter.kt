package com.aquall.app.adapters

import android.text.TextUtils
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.repositary.model.CandidateModel
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.viewholders.CandidateListViewHolder

class CandidateAdapter(userList:List<CandidateModel>, iteractor:CandidateListViewHolder.Iteractor): RecyclerView.Adapter<CandidateListViewHolder>() {
    var mUserList = ArrayList(userList)
    var filteredList = mUserList
    var mIteractor:CandidateListViewHolder.Iteractor = iteractor
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CandidateListViewHolder {
        return CandidateListViewHolder(parent)
    }

    override fun onBindViewHolder(holder: CandidateListViewHolder, position: Int) {
        holder.bind(filteredList.get(holder.layoutPosition),mIteractor)
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    fun getItem(position: Int):CandidateModel{
        return filteredList.get(position)
    }

    fun remove(item:CandidateModel){
        filteredList.remove(item)
        mUserList.remove(item)
        notifyDataSetChanged()
    }

    public fun filter(text:String){
        if (TextUtils.isEmpty(text)){
            filteredList = ArrayList(mUserList)
        }
        else{
            filteredList =  ArrayList(filteredList.filter {
                 val firstNameLowerCase = it.applicant?.firstName?.lowercase()
                 val secondNameLowerCase = it.applicant?.lastName?.lowercase()
                 val result =  firstNameLowerCase?.contains(text)?:false
                     || secondNameLowerCase?.contains(text)?:false
                 result
             })
        }
        notifyDataSetChanged()
    }
}