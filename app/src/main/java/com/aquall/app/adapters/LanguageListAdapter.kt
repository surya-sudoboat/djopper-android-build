package com.aquall.app.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.viewholders.ClickListener
import com.aquall.app.viewholders.LanguageViewHolder

class LanguageListAdapter(languageList:List<Localizer.Locale>,clickListener: ClickListener?=null) :RecyclerView.Adapter<LanguageViewHolder>(){

    var mLanguageList:List<Localizer.Locale> = languageList
    var mClickListener:ClickListener? = clickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        return LanguageViewHolder(parent)
    }

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        holder.bind(mLanguageList[position], clickListener = mClickListener)
    }

    override fun getItemCount(): Int {
        return mLanguageList.size
    }
}