package com.aquall.app.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aquall.app.R
import com.aquall.app.utils.CustomItemTouchHelper

class JobsViewPagerAdapter(parentFragment:Fragment,adapter1:JobListAdapter,adapter2: JobListAdapter,val customItemTouchHelper: CustomItemTouchHelper?):FragmentStateAdapter(parentFragment) {
    var mAdapter1=adapter1
    var mAdapter2=adapter2
    lateinit var currentViewPagerFragment:ViewPagerRecyclerFragment

    fun getRecyclerView():RecyclerView{
        return currentViewPagerFragment.recyclerView
    }


    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        currentViewPagerFragment =
            ViewPagerRecyclerFragment(if (position == 0) mAdapter1 else mAdapter2,if (position==0) customItemTouchHelper else null)
        return currentViewPagerFragment
    }


}

class ViewPagerRecyclerFragment(adapter:JobListAdapter,val customItemTouchHelper: CustomItemTouchHelper?):Fragment(){
    var mAdapter = adapter
    lateinit var recyclerView:RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inflate = inflater.inflate(R.layout.item_viewpager_job_rv, container, false)
        recyclerView = inflate.findViewById<RecyclerView>(R.id.rvJob)
        val textEmpty = inflate.findViewById<TextView>(R.id.textEmpty)
        recyclerView.layoutManager = LinearLayoutManager(inflate.rootView.context)
        recyclerView.adapter = mAdapter
        if (mAdapter.tempJobs.size<=0)
        {
            textEmpty.visibility = View.VISIBLE
        }else{
            textEmpty.visibility = View.GONE

        }
        customItemTouchHelper?.attachToRecyclerView(recyclerView)
        return inflate
    }
}