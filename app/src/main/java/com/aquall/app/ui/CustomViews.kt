package com.aquall.app.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.googlefonts.Font
import androidx.compose.ui.text.googlefonts.GoogleFont
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aquall.app.Localizer
import com.aquall.app.R

class CustomViews {
}

@Preview
@Composable
fun AquallTopBar(titleText:String?="djopper",menuIcons:@Composable RowScope.()->Unit={},homeClick:()->Unit={}){
    TopAppBar(
        backgroundColor = Color.White,
        elevation = 0.dp,

    ){
        Spacer(modifier = Modifier.width(10.dp))
        Icon(
            painter = painterResource(id = R.drawable.ic_back_arrow),
            contentDescription = "back",
            modifier = Modifier.clickable(true, onClick = homeClick)
        )
        Spacer(modifier = Modifier.width(15.dp))
        titleText?.let { Text(text = it, fontFamily = openSans, fontSize = 18.sp, fontWeight = FontWeight.Bold) }
        CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl){
        Row(Modifier.fillMaxWidth(),content = menuIcons)
        }
    }
}



@Preview
@Composable
fun JobViewHolder(){

}

