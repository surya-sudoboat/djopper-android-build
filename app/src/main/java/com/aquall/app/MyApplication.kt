package com.aquall.app

import android.app.Application
import android.os.Build.VERSION.SDK_INT
import coil.Coil
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.imageLoader
import coil.util.CoilUtils
import com.aquall.app.repositary.AppDatabase
import com.aquall.app.utils.Settings
import com.aquall.app.utils.SharedPrefUtil
import com.aquall.app.utils.model.Profile
import com.example.networkmanager.ServiceManager
import com.google.android.libraries.places.api.Places
import okhttp3.OkHttpClient
import android.widget.Toast

import android.R

import androidx.annotation.NonNull
import androidx.lifecycle.MutableLiveData
import com.aquall.app.utils.Logger
import com.aquall.app.utils.NotificationChannels

import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.messaging.FirebaseMessaging




class MyApplication:Application() {
    companion object{
        var globalApiErrorObserver = MutableLiveData<Exception>()
    }
    override fun onCreate() {
        super.onCreate()
        AppDatabase.init(this)
        ServiceManager.init(this)
        SharedPrefUtil.init(this)
        val getgooglePlaceAPIkey = Settings.getgooglePlaceAPIkey()
        try {
            Places.initialize(applicationContext, getgooglePlaceAPIkey?:"AIzaSyDRJkJkCVRt1b-_FMrwoPKJ_anlAkzZwSU")
        } catch (e: Exception) {
        }
        val imageLoader = ImageLoader.Builder(applicationContext)
            .crossfade(true)
            .okHttpClient {
                OkHttpClient.Builder()
                    .cache(CoilUtils.createDefaultCache(applicationContext))
                    .build()
            }.componentRegistry {
                if (SDK_INT >= 28) {
                    add(ImageDecoderDecoder(applicationContext))
                } else {
                    add(GifDecoder())
                }
            }
            .build()
        Coil.setImageLoader(imageLoader)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    if (task.exception!=null) {
                        Logger.e(task.exception!!)
                    }
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                val token = task.result
                SharedPrefUtil.getInstance().firebaseToken = token
                // Log and toast
                Logger.e("FCM token:$token")
            })

        NotificationChannels.createNotificationChannel(this)
        NotificationChannels.createNotificationChannelSilent(this)

    }


}