package com.aquall.app.viewholders

import android.content.res.Resources
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
fun getView(parent:ViewGroup,size:Int):View{
    val view = View(parent.context)
    val r: Resources = Resources.getSystem()
    val px = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        size.toFloat(),
        r.displayMetrics
    )
    view.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,px.toInt())
    return view
}
class EmptyVerticalViewHolder constructor(parent:ViewGroup, size:Int):RecyclerView.ViewHolder(
    getView(parent,size)
) {
}