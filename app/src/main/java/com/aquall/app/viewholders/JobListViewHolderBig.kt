package com.aquall.app.viewholders

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.databinding.ItemJobListHomescreenBigBinding
import com.aquall.app.databinding.ItemJobListHomescreenBinding
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.ImageProcessor
import com.aquall.app.utils.model.Job
import java.text.SimpleDateFormat
import java.util.*

class JobListViewHolderBig private constructor(view: View) :
    RecyclerView.ViewHolder(view),AlertDialog.Listener,View.OnClickListener{
    lateinit var binding:ItemJobListHomescreenBigBinding
    interface IteractionListener{
        fun onDeleteClicked(position: Int)
        fun onCandidateClicked(position: Int)
        fun onEditClicked(position: Int)
        fun onItemClicked(position: Int)
        fun deleteAlertText():String
        fun getIsPostedJob():Boolean
    }



    var mIteractionListener: IteractionListener? = null

    companion object{
        fun getInstance(parent:ViewGroup):JobListViewHolderBig{
            val binding = ItemJobListHomescreenBigBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            val jobListViewHolder = JobListViewHolderBig(binding.root)
            jobListViewHolder.setBinder(binding)
            return jobListViewHolder
        }
    }

    fun setBinder(itemJobListHomescreenBinding: ItemJobListHomescreenBigBinding)
    {
        binding = itemJobListHomescreenBinding
    }

    fun bind(job: Job, iteractionListener: IteractionListener?=null)
    {
        ImageProcessor.into(binding.imageJobPicture).setRoundedCorners(10f,0f).loadResource(R.drawable.ic_camera)
        val i  = layoutPosition+1
        binding.textPeriodLabel.text = Localizer.labels.period
        binding.textJobTitle.text = job.name
        if (job.location!=null) {
            binding.textLocationName.visibility = View.VISIBLE
            binding.textLocationName.text = job.location?.name
        }else{
            binding.textLocationName.visibility = View.GONE
        }
        if (job.start_date!=null&&job.end_date!=null) {
            binding.textPeriod.setText("${getDate(job.start_date!!)} - ${getDate(job.end_date!!)}")
        }
        if (job.applied_count>0) {
            binding.textCandidatesCount.visibility = View.VISIBLE
            binding.textCandidatesCount.setText("${job.applied_count} ${Localizer.labels.candidates_applied}")
            binding.textCandidatesCount.setOnClickListener {
                mIteractionListener?.onCandidateClicked(layoutPosition)
            }
        }else{
            binding.textCandidatesCount.visibility = View.GONE
        }
        if (iteractionListener?.getIsPostedJob() == true)
        {
            binding.buttonEditProfile.visibility = View.VISIBLE
            binding.buttonEditProfile.setOnClickListener {
                iteractionListener?.onEditClicked(layoutPosition)
            }
        }else{
            binding.buttonEditProfile.visibility = View.GONE

        }
        binding.buttonDeleteJob.setOnClickListener(this)
        binding.root.setOnClickListener(this)
        mIteractionListener = iteractionListener
        if (!TextUtils.isEmpty(job.image)) {
            ImageProcessor.into(binding.imageJobPicture).setRoundedCorners(10f,0f).loadResource(
                ServiceUrl.BASE_URL+job.image,R.drawable.ic_camera)

        }
    }

    fun getDate(dateStr: String):String {
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val mDate = formatter.parse(dateStr)
            return formatter2.format(mDate!!)
        } catch (e: Exception){
        }
        return dateStr
    }

    override fun success() {
        if (mIteractionListener!=null) {
            mIteractionListener!!.onDeleteClicked(layoutPosition)
        }
    }

    override fun cancel() {}

    override fun onClick(p0: View?) {

         if (p0?.id == binding.root.id) {
            mIteractionListener?.onItemClicked(layoutPosition)
        }else if(p0?.id == binding.buttonDeleteJob.id)
         {
             if (mIteractionListener!=null) {
                 AlertDialog.show(p0.context,mIteractionListener!!.deleteAlertText(),true,this)
             }

         }
    }


}