package com.aquall.app.viewholders

import android.view.ViewGroup
import android.widget.TextView
import com.aquall.app.R

class JobSearchHorizontalViewHolder(parent:ViewGroup):BaseViewHolder(parent, R.layout.item_horizontalscroll_searchkey) {
fun bind(value:String)
{
    val findViewById = itemView.findViewById<TextView>(R.id.tvSearchKeyScrollItem)
    findViewById.setText(value)
}

}