package com.aquall.app.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.databinding.ItemJobMessageListBinding
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.ImageProcessor
import com.aquall.app.utils.model.Job
import android.graphics.Color
import android.text.TextUtils
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.repositary.service.ServiceUrl
import java.text.SimpleDateFormat
import java.util.*


//donot create new object use getInstance
class JobMessageListViewHolder private constructor(view: View) :
    RecyclerView.ViewHolder(view),AlertDialog.Listener,View.OnClickListener{
    lateinit var binding:ItemJobMessageListBinding
  var  mListener:JobListViewHolder.IteractionListener? = null


    companion object{
        fun getInstance(parent:ViewGroup):JobMessageListViewHolder{
            val binding = ItemJobMessageListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            val jobListViewHolder = JobMessageListViewHolder(binding.root)
            jobListViewHolder.setBinder(binding)
            return jobListViewHolder
        }
    }


    fun bind(job:Job,listener:JobListViewHolder.IteractionListener?)
    {
        mListener = listener
        ImageProcessor.into(binding.ivJobBanner).loadResource(com.aquall.app.R.drawable.ic_camera)
        val i  = layoutPosition+1
        binding.tvValueJobTitle.text = job.name
        if (job.location!=null) {
            binding.tvValueLocation.visibility = View.VISIBLE
            binding.tvValueLocation.text = job.location?.name
        }else{
            binding.tvValueLocation.visibility = View.GONE
        }
        if (job.category!=null) {
            binding.textValueCategory.visibility = View.VISIBLE
            binding.textValueCategory.text = job.category?.name
        }else{
            binding.textValueCategory.visibility = View.GONE
        }
        if (job.start_date!=null)
        {
            binding.textValuePostedDate.setText(getDateString(job.updatedAt!!))
        }

        if (mListener!=null&& job.unread_count>0)
        {
            binding.tvValueMessages.visibility = View.VISIBLE
            binding.tvValueMessages.setText("${job.unread_count}")
        }
        else{
            binding.tvValueMessages.visibility = View.GONE

        }
        if (!TextUtils.isEmpty(job.image)) {
            ImageProcessor.into(binding.ivJobBanner).setRoundedCorners(10f,0f).loadResource(
                ServiceUrl.BASE_URL+job.image,
                R.drawable.ic_camera)

        }
        if (job.isRead)
        {
            binding.layoutJobItem.setBackgroundColor(Color.WHITE)
            binding.tvValueJobTitle.setTextAppearance(R.style.textNormal)
        }else{
            binding.layoutJobItem.setBackgroundColor(Color.parseColor("#F8F8F8"))
            binding.tvValueJobTitle.setTextAppearance(R.style.textNormal_bold)
        }

        binding.root.setOnClickListener(this)
    }

    fun getDateString(createdDate: String): String? {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        var dateCreated = formatter.parse(createdDate)
        val diff: Long = Date().getTime() - dateCreated.getTime()
        val diffDays = diff / (24 * 60 * 60 * 1000)
        if(diffDays!=0L){
            return SimpleDateFormat("dd-MM-YYYY").format(dateCreated!!)
        }else{
            return SimpleDateFormat("HH:mm").format(dateCreated!!)

        }
        return ""
    }
    fun setBinder(itemJobListHomescreenBinding: ItemJobMessageListBinding)
    {
        binding = itemJobListHomescreenBinding
    }





    override fun success() {
       }

    override fun cancel() {

    }

    override fun onClick(p0: View?) {
        mListener?.onItemClicked(layoutPosition)
    }


}