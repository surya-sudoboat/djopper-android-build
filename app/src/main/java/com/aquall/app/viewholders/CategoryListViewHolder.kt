package com.aquall.app.viewholders

import android.view.ViewGroup
import android.widget.TextView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.utils.model.Category
import java.text.FieldPosition

class CategoryListViewHolder(parent: ViewGroup) :
    BaseViewHolder(parent, R.layout.item_category_selection_small) {
        interface ItemClicked{
            fun onItemClicked(position:Int)
        }
        var  textCategoryTitle:TextView;
        var  textSubCategoryCount:TextView;
        var mItemClicked:CategoryListViewHolder.ItemClicked?=null;

    fun setItemClickedListener(itemClicked: ItemClicked)
    {
        mItemClicked = itemClicked
    }


    init {
        textCategoryTitle=itemView.findViewById(R.id.textCategoryTitle)
        textSubCategoryCount=itemView.findViewById(R.id.textSubCategoryCount)
    }


        fun bind(category:Category)
        {
            textCategoryTitle.setText(category.name)

            textSubCategoryCount.setText("${category.sub_category_count} ${Localizer.labels.sub_categories}")
            itemView.setOnClickListener {
                mItemClicked?.onItemClicked(layoutPosition)
            }
        }





}