package com.aquall.app.viewholders

import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.aquall.app.R
import com.aquall.app.repositary.model.CandidateModel
import com.aquall.app.repositary.model.Message
import com.aquall.app.repositary.model.UserModel
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.ImageProcessor
import com.aquall.app.utils.model.Job
import java.text.SimpleDateFormat
import java.util.*

class CandidateListViewHolder(parent: ViewGroup):
BaseViewHolder(parent, R.layout.item_candidate_list) {
    var tvValueCandidate:TextView?=null
    var tvValueLocation:TextView?=null
    var textValueTime:TextView?=null
    var ivProfilePicture: ImageView? = null
    var unreadIV: ImageView? = null
    var mIteractor:Iteractor?=null

    interface Iteractor{
        fun onItemClick(position:Int)
        fun getJob(): Job?
    }

    init {
        tvValueCandidate=itemView.findViewById(R.id.tvValueCandidate)
        tvValueLocation=itemView.findViewById(R.id.tvValueLocation)
        textValueTime=itemView.findViewById(R.id.textValueTime)
        ivProfilePicture=itemView.findViewById(R.id.ivProfilePicture)
        unreadIV=itemView.findViewById(R.id.unreadIV)
    }

    fun bind(userModel: CandidateModel, iteraction:Iteractor)
    {
        mIteractor = iteraction
        tvValueCandidate?.text  = userModel.applicant?.firstName + " "+ (userModel.applicant?.lastName?:"")
        tvValueLocation?.visibility = View.GONE
        if(mIteractor!=null&& mIteractor!!.getJob()!=null){
            tvValueLocation?.visibility = View.VISIBLE
            val job = mIteractor!!.getJob()
            tvValueLocation?.text = job?.location?.name

            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val formatter1 = SimpleDateFormat("dd-MM-YYYY, hh:mm a", Locale.ENGLISH)
            val parse = job?.updatedAt?.let { formatter.parse(it) }
            textValueTime?.text = parse?.let { formatter1.format(it) }
        }
        itemView.setOnClickListener {
            mIteractor?.onItemClick(layoutPosition)
        }
        if (!TextUtils.isEmpty(userModel.applicant?.image)) {
            ivProfilePicture?.let {
                ImageProcessor.into(it).setCircleCrop().loadResource(
                    ServiceUrl.BASE_URL+userModel.applicant?.image,R.drawable.ic_abstract_user)
            }
        }
        unreadIV?.visibility = if(userModel.is_unread_message) View.VISIBLE else View.GONE
        if (userModel.is_unread_message)
        {
            itemView.setBackgroundColor(Color.parseColor("#F8F8F8"))
        }else{
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.white))
        }
    }

}