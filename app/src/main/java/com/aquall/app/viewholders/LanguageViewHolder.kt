package com.aquall.app.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.repositary.LabelRepositary

fun getView(parent: ViewGroup): View
{
   return LayoutInflater.from(parent.context).inflate(R.layout.item_language_text,parent,false)

}
interface ClickListener{
    fun onClick(language: Localizer.Locale)
}
class LanguageViewHolder(parent: ViewGroup):RecyclerView.ViewHolder(getView(parent)) {
    lateinit var mLanguage:Localizer.Locale
    var mClickListener:ClickListener?=null

    fun bind(language:Localizer.Locale,clickListener: ClickListener?=null)
    {
        mLanguage = language
        mClickListener = clickListener
        val languageTextView = itemView.findViewById<TextView>(R.id.textLanguage)
        languageTextView.setText(language.label)

        languageTextView.setOnClickListener {
            mClickListener?.onClick(language)
        }
    }

}