package com.aquall.app.viewholders

import android.view.ViewGroup
import android.view.ViewParent
import android.widget.ImageView
import android.widget.TextView
import com.aquall.app.R
import com.aquall.app.repositary.model.Message
import java.text.SimpleDateFormat
import java.util.*

class MessageOutgoingViewHolder(parent: ViewGroup):
BaseViewHolder(parent, R.layout.item_outgoing_text_message) {
    var tvValueMessage:TextView?=null
    var textValueTime:TextView?=null
    var ivProfilePicture: ImageView? = null


    init {
        tvValueMessage=itemView.findViewById(R.id.tvValueMessage)
        textValueTime=itemView.findViewById(R.id.textValueTime)
        ivProfilePicture=itemView.findViewById(R.id.ivProfilePicture)

    }

    fun bind(message:Message)
    {
        tvValueMessage?.text = message.message
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val formatter1 = SimpleDateFormat("dd-MM-yyyy, HH:mm", Locale.ENGLISH)
            val parse = formatter.parse(message.timestamp)

            textValueTime?.text = formatter1.format(parse)
        } catch (e: Exception) {
        }
    }

}