package com.aquall.app.viewholders

import android.content.Context
import android.content.res.Resources
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.Localizer
import com.aquall.app.R
import com.aquall.app.databinding.ItemJobListHomescreenBinding
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.repositary.AuthRepositary.Companion.userId
import com.aquall.app.repositary.service.ServiceUrl
import com.aquall.app.utils.AlertDialog
import com.aquall.app.utils.ImageProcessor
import com.aquall.app.utils.model.Job
import java.text.SimpleDateFormat
import java.util.*


class JobListViewHolder private constructor(view: View) :
    RecyclerView.ViewHolder(view),AlertDialog.Listener,View.OnClickListener{
    lateinit var binding:ItemJobListHomescreenBinding
    interface IteractionListener{
        fun onFavoriteChanged(position:Int,value:Boolean)
        fun onItemClicked(position: Int)
        fun isFavoriteList():Boolean
        fun showFavoriteAndKnowMore():Boolean
        fun deleteAlertText():String
        fun onDeleteClicked(position: Int)
        fun onEditClicked(position: Int)

        fun getMessagesCount(position: Int):Int
    }

    var mIteractionListener: IteractionListener? = null

    companion object{
        fun getInstance(parent:ViewGroup?,context: Context? =parent?.context):JobListViewHolder{
            val binding = ItemJobListHomescreenBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
            val jobListViewHolder = JobListViewHolder(binding.root)
            jobListViewHolder.setBinder(binding)
            return jobListViewHolder
        }
    }

    fun setBinder(itemJobListHomescreenBinding: ItemJobListHomescreenBinding)
    {
        binding = itemJobListHomescreenBinding
    }

    fun bind(job: Job, iteractionListener: IteractionListener?=null)
    {
       binding.label = Localizer.labels
        ImageProcessor.into(binding.imageJobPicture).setRoundedCorners(10f,0f).loadResource(R.drawable.ic_camera)
        val i  = layoutPosition+1
        if (iteractionListener!=null)
        {
            binding.buttonFavorite.isSelected= iteractionListener.isFavoriteList()||job.is_favourite
        }
        binding.textJobTitle.text = job.name
        if (job.location!=null) {
            binding.textLocationName.visibility = View.VISIBLE
            binding.textLocationName.text = job.location?.name
        }else{
            binding.textLocationName.visibility = View.GONE
        }
        if (job.start_date!=null&&job.end_date!=null) {
            binding.textPeriod.setText("${getDate(job.start_date!!)} - ${getDate(job.end_date!!)}")
        }
        if (!TextUtils.isEmpty(job.image)) {
            ImageProcessor.into(binding.imageJobPicture).setRoundedCorners(10f,0f).loadResource(ServiceUrl.BASE_URL+job.image,R.drawable.ic_camera)
        }
        if (job.isRead)
        {
            binding.layoutJobItem.setBackgroundResource(R.drawable.ic_background_white_large)
        }else{
            binding.layoutJobItem.setBackgroundResource(R.drawable.ic_background_grey_large)

        }
        if (iteractionListener?.showFavoriteAndKnowMore()==true){
            binding.buttonFavorite.visibility = View.VISIBLE
            binding.buttonViewDetails.visibility = View.VISIBLE
            binding.buttonFavorite.setOnClickListener(this)
            binding.buttonViewDetails.setOnClickListener(this)
            binding.layoutJobItem?.layoutParams?.height = pxToDp(binding.layoutJobItem.context,230f).toInt()

        }else{
            binding.buttonFavorite.visibility = View.GONE
            binding.buttonViewDetails.visibility = View.GONE
            binding.layoutJobItem?.layoutParams?.height = pxToDp(binding.layoutJobItem.context,150f).toInt()
            if (job.job_owner_id?.equals(AuthRepositary.userId)==true || job.job_owner?._id.equals(userId)){
                binding.buttonDeleteJob.visibility = View.VISIBLE
                binding.buttonDeleteJob.setOnClickListener {
                    if (mIteractionListener!=null) {
                        AlertDialog.show(it.context,mIteractionListener!!.deleteAlertText(),true,
                            object : AlertDialog.Listener {
                                override fun success() {
                                    if (mIteractionListener!=null) {
                                        mIteractionListener!!.onDeleteClicked(layoutPosition)
                                    }
                                }

                                override fun cancel() {

                                }

                            } )
                    }
                }
                binding.buttonEditJob.visibility = View.VISIBLE
                binding.buttonEditJob.setOnClickListener {
                    mIteractionListener?.onEditClicked(layoutPosition)
                }
            }else{
                binding.buttonDeleteJob.visibility = View.GONE
                binding.buttonEditJob.visibility = View.GONE
            }
            binding.layoutJobItem.setOnClickListener(this)
        }

        mIteractionListener = iteractionListener
        if (job.updatedAt!=null) {
            if (job.job_owner?._id.equals(userId)) {
                binding.tvPostedOn.setText(Localizer.labels.posted+" " + getDate(job.updatedAt!!))
            } else {
                binding.tvPostedOn.setText(Localizer.labels.posted+" " + getPostedData(job.updatedAt!!))
            }
        }
        binding.tvPostedBy.setText(job.job_owner?.firstName)
    }
    fun getPostedData(updatedAt:String):String
    {
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        val parse = formatter.parse(updatedAt)
        return DateUtils.getRelativeTimeSpanString(parse.time,Date().time,
            DateUtils.SECOND_IN_MILLIS
        ).toString()
    }
    fun getDate(dateStr: String):String {
        try {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
            val formatter2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val mDate = formatter.parse(dateStr)
            return formatter2.format(mDate!!)
        } catch (e: Exception){}
        return dateStr
    }

    override fun success() {
        binding.buttonFavorite.isSelected = !binding.buttonFavorite.isSelected
        mIteractionListener?.onFavoriteChanged(layoutPosition,binding.buttonFavorite.isSelected)
    }

    override fun cancel() {}

    override fun onClick(p0: View?) {
        if (p0?.id == binding.buttonFavorite.id) {
            val selected = binding.buttonFavorite.isSelected
            if (!selected) {
                AlertDialog.show(p0.context,Localizer.labels.do_you_want_to_make_this_job_offer_as_favorite,true,this)
            }else{
                AlertDialog.show(p0.context,Localizer.labels.do_you_want_to_remove_this_job_offer_from_favorites,true,this)
            }
        }
        else if (p0?.id == binding.buttonViewDetails.id ||  p0?.id == binding.layoutJobItem.id) {
            mIteractionListener?.onItemClicked(adapterPosition)
        }
    }

    fun pxToDp(context: Context?,input:Float):Float{
        val r: Resources? = context?.getResources()
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            input,
            r?.displayMetrics
        )
    }


}