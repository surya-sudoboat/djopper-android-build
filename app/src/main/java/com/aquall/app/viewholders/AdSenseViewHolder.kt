package com.aquall.app.viewholders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aquall.app.R
import com.aquall.app.databinding.ItemAdSenseBinding
import com.aquall.app.databinding.ItemJobListHomescreenBinding
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import java.util.*

fun get(parent:ViewGroup):View
{
   return LayoutInflater.from(parent.context).inflate(R.layout.item_ad_sense,parent,false)


}
class AdSenseViewHolder(view:View):RecyclerView.ViewHolder(view) {
   lateinit var binding:ItemAdSenseBinding
   companion object{
      fun getInstance(parent:ViewGroup?,context:Context?=parent?.context):AdSenseViewHolder{
         val binding = ItemAdSenseBinding.inflate(
            LayoutInflater.from(context),
            parent,
            false
         )
         val adSenseViewHolder = AdSenseViewHolder(binding.root)
         adSenseViewHolder.setBinder(binding)
         return adSenseViewHolder
      }
   }
   fun setBinder(itemAdSenseBinding: ItemAdSenseBinding)
   {
      binding = itemAdSenseBinding
      binding.adView.adListener = object : AdListener() {
         override fun onAdLoaded() {
            super.onAdLoaded()
         }

         override fun onAdFailedToLoad(p0: LoadAdError) {
            super.onAdFailedToLoad(p0)
         }
      }
      val adRequest = AdRequest.Builder().build()
      binding.adView.loadAd(adRequest)
   }
   fun bind()
   {

   }

}