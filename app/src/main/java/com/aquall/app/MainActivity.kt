package com.aquall.app

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.aquall.app.databinding.ActivityMainBinding
import com.aquall.app.fragments.*
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.SharedPrefUtil
import com.aquall.app.utils.SocketClient
import com.aquall.app.utils.enums.NavScreen
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity

class MainActivity : BaseActivity(),View.OnClickListener{
    lateinit var  view:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view = DataBindingUtil.setContentView(this@MainActivity,R.layout.activity_main)
        view.label = Localizer.labels
        if (!SocketClient.getSocketClient().isRunning) {
            SocketClient.getSocketClient().unReadLiveData.postValue(SharedPrefUtil.getInstance().unreadCount)
            SocketClient.getSocketClient().unReadFavLiveData.postValue(SharedPrefUtil.getInstance().favUnreadCount)
        }
        if (AuthRepositary().isLoggedIn()) {
            val intExtra = intent.getIntExtra("screenName", -1)
            val data = intent.getStringExtra("data")
            val classByValue = ScreenName.Name.getClassByValue(intExtra)
            if (intExtra !=-1&& classByValue !=null)
            {
                addFragment(classByValue!!,data)
                if(intExtra == ScreenName.DASHBOARD.getValue()||intExtra == ScreenName.PROFILE.getValue())
                {
                    view.isBottomBarVisible = true
                }
            }
           else{
                addFragment(ScreenName.DASHBOARD,data)
                view.isBottomBarVisible = true
           }

        }else{
            addFragment(ScreenName.LOGIN)
        }
        view.layBottomBar.navChat.setOnClickListener{replaceFragment(ScreenName.MESSAGE,"")}
        view.layBottomBar.navFavorites.setOnClickListener{replaceFragment(ScreenName.FAVORITELIST)}
        view.layBottomBar.navHome.setOnClickListener{replaceFragment(ScreenName.DASHBOARD)}
        view.layBottomBar.navProfile.setOnClickListener{replaceFragment(ScreenName.PROFILE)}
        view.layBottomBar.navSearchJobs.setOnClickListener{replaceFragment(ScreenName.SEARCHJOB)}
        view.layBottomBar.layoutBackground.setOnClickListener {  }

        SocketClient.getSocketClient().unReadLiveData.observe(this) {
            SharedPrefUtil.getInstance().unreadCount = it
            if (it > 0) {
                view.layBottomBar.counterText.visibility = View.VISIBLE
                view.layBottomBar.counterText.text = "${it}"
            } else {
                view.layBottomBar.counterText.visibility = View.GONE

            }
        }

        SocketClient.getSocketClient().unReadFavLiveData.observe(this){
            if (it > 0) {
                view.layBottomBar.counterFavText.visibility = View.VISIBLE
                view.layBottomBar.counterFavText.text = "${it}"
            } else {
                view.layBottomBar.counterFavText.visibility = View.GONE

            }
        }
    }

    override fun replaceFragment(screen: ScreenName.Name?,data:String?,addToHistory:Boolean,prevData:String?) {
        super.replaceFragment(screen,data,addToHistory,prevData)
        view.isBottomBarVisible = screen?.getFragment() is DashboardFragment
                ||screen?.getFragment() is SearchJobFragment
                ||screen?.getFragment() is FavoriteJobListFragment
                ||screen?.getFragment() is ProfileFragment
                ||screen?.getFragment() is MessagesFragment

    }

    override fun login(screen: ScreenName.Name?) {
        super.login(screen)
        view.isBottomBarVisible = screen?.getFragment() is DashboardFragment
                ||screen?.getFragment() is SearchJobFragment
                ||screen?.getFragment() is FavoriteJobListFragment
                ||screen?.getFragment() is ProfileFragment
                ||screen?.getFragment() is MessagesFragment

    }


    override fun logOut() {
        super.logOut()
        view.isBottomBarVisible = false
    }
    fun setScreenType(navScreen: NavScreen.Screen){
        view.navScreen = navScreen
    }

    private fun getFragmentTransaction() = supportFragmentManager
        .beginTransaction()

    override fun onClick(p0: View?) {

    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val intExtra = intent?.getIntExtra("screenName", -1)
        val data = intent?.getStringExtra("data")
        if (intExtra!=-1)
        {

            replaceFragment(ScreenName.Name.getClassByValue(intExtra!!),data)
        }


    }



}
