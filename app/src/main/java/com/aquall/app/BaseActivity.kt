package com.aquall.app

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.android.volley.VolleyError
import com.aquall.app.fragments.*
import com.aquall.app.repositary.AuthRepositary
import com.aquall.app.utils.ScreenName
import com.aquall.app.utils.enums.DataStatus

open class BaseActivity:AppCompatActivity(),Observer<Exception> {
    protected var mPreviousFragment: ScreenName.Name? = null
    protected var mCurrentFragment:ScreenName.Name? = null
    var mFragmentInstance: Fragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApplication.globalApiErrorObserver.observe(this,this)
    }

    fun addFragment(screen: ScreenName.Name,data: String?=null)
    {
        mCurrentFragment = screen
        mFragmentInstance = screen.getFragment()
        if(data!=null)
        {
            val bundle = Bundle()
            bundle.putString("data",data)
            mFragmentInstance?.arguments = bundle
        }
        getFragmentTransaction()
            .replace(R.id.frameContainer, mFragmentInstance!!)
            .commit()
    }
    //IF data is null it will get the old data.
    open fun replaceFragment(screen: ScreenName.Name?,data:String?=null,addToHistory:Boolean=true,prevData:String? = null){
        if (screen!=null) {
            if (addToHistory) {
                mPreviousFragment = mCurrentFragment
                if (prevData!=null) {
                    mPreviousFragment?.data = prevData
                }
            }
            mCurrentFragment = screen
            mFragmentInstance = screen.getFragment()
            if (data!=null) {
                screen.data = data
            }
            if(screen.data!=null)
            {
                val bundle = Bundle()
                bundle.putString("data",screen.data)
                mFragmentInstance?.arguments = bundle
            }
            getFragmentTransaction()
                .replace(R.id.frameContainer, mFragmentInstance!!)
                .commit()
        }

    }
    open fun login(screen: ScreenName.Name?){
        if (screen!=null) {
            mPreviousFragment = null
            mCurrentFragment = screen
            mFragmentInstance = screen.getFragment()
            getFragmentTransaction()
                .replace(R.id.frameContainer, mFragmentInstance!!)
                .commit()
        }

    }
    open fun logOut(){

            mPreviousFragment = null
            mCurrentFragment = null
        mFragmentInstance = ScreenName.LOGIN.getFragment()
        getFragmentTransaction()
                .replace(R.id.frameContainer, mFragmentInstance!!)
                .commit()


    }

    open fun updateArguments(data:String)
    {
        if (data!=null)
        {
            mCurrentFragment?.data = data
        }
    }

    fun newScreen(screen: ScreenName.Name,data: String?)
    {
        val intent = Intent(this, NewActivity::class.java)
        intent.putExtra("clazzValue",screen.getValue())
        if (data!=null) {
            intent.putExtra("data",data)
        }
        startActivity(intent)
    }
    private fun getFragmentTransaction() = supportFragmentManager
        .beginTransaction()

    override fun onBackPressed() {
        if (mFragmentInstance is EditProfileFragment)
        {
            val checkIfDataIsEditing = (mFragmentInstance as EditProfileFragment).checkIfDataIsEditing()
            if (checkIfDataIsEditing)
                return
        }
        if (mPreviousFragment!=null)
        {
            replaceFragment(mPreviousFragment!!)
        }else if (mPreviousFragment==null && !(mCurrentFragment?.getFragment() is DashboardFragment) && AuthRepositary().isLoggedIn())
        {
            if (mCurrentFragment is ScreenName.APPLIEDJOBS ||
                mCurrentFragment is ScreenName.POSTEDJOBS ||
                mCurrentFragment is ScreenName.EXPIREDJOBS){
                replaceFragment(ScreenName.PROFILE, addToHistory = false)
                return
            }

            replaceFragment(ScreenName.DASHBOARD)
        }else{
            super.onBackPressed()
        }
        mPreviousFragment = null
    }

    override fun onChanged(t: Exception?) {

        if (t is VolleyError)
        {
            if (t.networkResponse.statusCode==440)
            {
                val authRepositary = AuthRepositary()
                authRepositary.logout(true)
                authRepositary.networkStatus.observe(this){
                    if (it==DataStatus.FAILED||it==DataStatus.DATA_AVAILABLE)
                    {
                        Toast.makeText(this,Localizer.labels.session_ended_this_account,Toast.LENGTH_SHORT).show()
                        logOut()
                    }
                }
            }
        }

    }
}

fun FragmentActivity.replaceFragment(screen: ScreenName.Name,data:String?=null,addToHistory: Boolean=true,prevDat:String? = null)
{
    if (this is BaseActivity)
    {
        (this as BaseActivity).replaceFragment(screen,data,addToHistory,prevDat)
    }
}

fun FragmentActivity.updateArguments(data:String)
{
    if (this is BaseActivity)
    {
        (this as BaseActivity).updateArguments(data)
    }
}

fun FragmentActivity.login(screen: ScreenName.Name)
{
    if (this is BaseActivity)
    {
        (this as BaseActivity).login(screen)
    }
}
fun FragmentActivity.newScreen(screen: ScreenName.Name,data:String?=null)
{
    if (this is BaseActivity)
    {
        (this as BaseActivity).newScreen(screen,data)
    }
}

fun FragmentActivity.logout()
{
    if (this is BaseActivity)
    {
        (this as BaseActivity).logOut()
    }
}