package com.aquall.app

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.aquall.app.fragments.PaymentItem
import com.google.accompanist.appcompattheme.AppCompatTheme
import org.junit.Assert.*

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PaymentListFragmentTest {

    @get: Rule
    val composeTestRule = createComposeRule()

    @Test
    fun checkAmountAvailable() {
        composeTestRule.setContent {
            AppCompatTheme() {
                PaymentItem()
            }
        }
        composeTestRule.onNode(hasText("Amount",true)).assertIsDisplayed()
    }
}