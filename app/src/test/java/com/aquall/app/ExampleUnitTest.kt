package com.aquall.app

import com.aquall.app.fragments.DashboardFragment
import com.aquall.app.fragments.LoginFragment
import com.aquall.app.utils.ScreenName
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun `check if it is the dashboard fragment`() {
        val fragmentFromValue = ScreenName.Name.getFragmentFromValue(ScreenName.DASHBOARD.getValue())
        assert(fragmentFromValue is DashboardFragment)
    }
}